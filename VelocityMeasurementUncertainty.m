% Calculate uncertainty of velocity measurement with pitot tube

p_0=1.013e5; % Ambient pressure
d_p=25e2; % pressure difference
T=273.15; % Temperature in cold chamber in K
R=287.058; % Specific general gas constant
% 
% Maximum Scale Values
max_p0=1.6e5;
max_dp=25e2;

% 
% Uncertainties
Delta_p0=0.25e-2*max_p0;
Delta_dp=0.07e-2*max_dp;
Delta_T=0.3;

%%%%%%%%%%%%% after Nitsche %%%%%%%%%%%%%

U_inf=sqrt(2*(d_p)*R*T/p_0);

Delta_Udp=U_inf^(-1)*(R*T/p_0)*Delta_dp;
Delta_UT=U_inf^(-1)*(R*d_p/p_0)*Delta_T;
Delta_Up0=U_inf^(-1)*(-R*d_p*T/(p_0^2))*Delta_p0;



Delta_U=sqrt(Delta_Udp^2+Delta_UT^2+Delta_Up0^2);




