 %Calculate freezing velocity for equilibrium freezing
lambda_ice=2.2; % thermal conductivity ice at 0�C in W/(K m) (VDI 10th Edition)
delta_h=333.1e3; % Latent heat of water at melting in [J]
cp_ice=2000; % Heat capacity of ice at 0�C (VDI 10th Edition)
rho_ice=900; % Density of ice 0�C (VDI 10th Edition)
Delta_T=10; % Difference in Wall Temp to melting temp in Kelvin
T_wall=-Delta_T; % Wall temp in �C


St=cp_ice.*Delta_T/delta_h;
Lambda_solarr=NaN(1,length(St));

for ii=1:length(St)
    
syms Lambda
eqn = sqrt(pi).*Lambda.*exp(Lambda.^2).*erf(Lambda) == St(ii);

Lambda_sol=vpasolve(eqn,Lambda);
Lambda_solarr(ii)=double(Lambda_sol);
end

% t=3600*24; % timescale for freezing velocity in s
t=0.001; % timescale for freezing velocity in s

v_freez=(Lambda_solarr.*sqrt(lambda_ice/(rho_ice.*cp_ice))).*(t)^(-1/2);
h_freez= 2*Lambda_solarr*sqrt(lambda_ice/(rho_ice.*cp_ice)*t);

z=0:h_freez/100:h_freez;
% T= 1-(erf((z)/(2*t))/erf(Lambda_solarr)); % Solution for melting and temp in liquid phase 

T_ice=T_wall+Delta_T*erf(0.5*z*sqrt(rho_ice*cp_ice/(lambda_ice*t)))/erf(Lambda_solarr);

plot(z,T_ice)
xlabel('z')
ylabel('T_{ice}')
