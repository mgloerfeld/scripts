%% Save data for exchange

% Save Data to named Variables
% D_0=num2str(arr_avdiam,'%.2e');
% U_0=num2str(arr_dropspeed,'%.2f');
% T_0=num2str(arr_droptemp,'%.2f');
% xi_ice=num2str(arr_icefr,'%.3f');
% D_res=num2str(arr_ResSplatDiam,'%.2e');
% H_0=num2str(arr_initheight,'%.2e');
% H_res=num2str(arr_resheight,'%.2e');
% Y_equiv=num2str(arr_Ymod_pl,'%2.f');
% 
% datatable=table(D_0,U_0,T_0,xi_ice,D_res,H_0,H_res,Y_equiv);
% 
% writetable(datatable,[SavingLocation,'CRST_Gloerfeld_Data'],'Delimiter','\t')



% Save Data to named Variables
DeltaT=num2str(values_x,'%.2e');
U_f=num2str(values_y,'%.2f');

datatable=table(DeltaT,U_f);

SavingLocation='D:\Promotion\Repos\';
writetable(datatable,[SavingLocation,'DendriteFrontVelocitySchremb2016'],'Delimiter','\t')