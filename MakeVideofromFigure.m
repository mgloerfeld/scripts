%% Script that creates a video from a figure
Location = 'F:\Experiments\Residualmass_extension\Experiments\03222021\V106_-10�C_15ms_C001H001S0001\';
%% Load Data

% Load image data
ImageFiles = dir([Location '**\*images*']);
VideoFileNames = ImageFiles.name;
load([Location,VideoFileNames])

% Load image processing data
ImageFiles = dir([Location '**\*Data_test2*']);
DataFileNames = ImageFiles.name;
load([Location,DataFileNames])


%% Loop through images

var_imgwidth=size(images(:,:,1),2);
var_imghght=size(images(:,:,1),1);

fig1=figure(1);
fig1.Units='Normalized';
fig1.Position=[0 0 0.9 0.9];

    for ii = 1:130 %length(images)
    imshow(imrotate(images(:,:,ii),180))
    if ~isnan(stc_VID.ImageData(ii).SurfaceTouchPts)
      hold on
      arr_wtpts=stc_VID.ImageData(ii).SurfaceTouchPts;
      arr_wtpts=[var_imgwidth,var_imghght]-arr_wtpts;
      scatter(arr_wtpts(1,1),arr_wtpts(1,2),'wo','filled')
      scatter(arr_wtpts(2,1),arr_wtpts(2,2),'wo','filled')
      hold off
    end
      F(ii) = getframe(gca) ;
      drawnow
    end
  % create the video writer with 1 fps
  writerObj = VideoWriter([Location,'WettingTrackingVideo.avi']);
  writerObj.FrameRate = 30;
  % set the seconds per image
% open the video writer
open(writerObj);
% write the frames to the video
for ii=1:length(F)
    % convert the image to a frame
    frame = F(ii) ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);