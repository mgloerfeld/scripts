Location='F:\Experiments\ImpactPositionVariation\CalibrationExperiments\C001_22�C_0ms_0\';

    % Load all side view images
    img_BG_SideOR=imread([Location,'BG_Side.tif']);
    if length(img_BG_SideOR(1,1,:))>1
        img_BG_SideOR=rgb2gray(img_BG_SideOR);
    end
    img_BG_SideOR=imadjust(img_BG_SideOR,[0 0.88]);
    
    
    % Get Pixelsize for SideView
    var_pixsize_Side=getpixelsize(Location);
    
    img_current=imread([Location,'DI_120D.tif']);
    img_current=imadjust(img_current,[0 0.88]);
    img_original=img_current;
    
        % Get Surface position and inclination in BG image
%         [~,~,var_srfangBG,~,arr_srfedgeptsBG]=getsurfaceposition(img_BG_SideOR,var_pixsize_Side,'Bottom');
    
        % Get Surface Inclination from Drop image
        [~,~,var_srfang,~,arr_srfedgepts]=getsurfaceposition(img_original,var_pixsize_Side,'Bottom');
        
        
        arr_lftptBG=(arr_srfedgeptsBG(1,:)); % Get right point of BG image
        arr_lftptor=(arr_srfedgepts(1,:)); % Get right point of drop image
        
        var_trnsang=(-atan(arr_lftptBG(2)/arr_lftptBG(1))+atan(arr_lftptor(2)/arr_lftptor(1))); % Calculate rotation angle from right point displacement
        arr_gendspl=(arr_srfedgepts(2,:)-arr_srfedgeptsBG(2,:)); % Calculate general displacement
        % Reconstruct lateral displacement due to rotation 
        var_rotdsplx=sum(arr_srfedgepts(1,:).*[(1-cos(var_trnsang)) sin(var_trnsang)]);
        var_rotdsply=sum(arr_srfedgepts(1,:).*[sin(var_trnsang) (cos(var_trnsang)-1)]);

        arr_trnspos=arr_gendspl+[var_rotdsplx var_rotdsply];
        
        arr_trnsT=[1 0 0;0 1 0; arr_trnspos(1) arr_trnspos(2) 1]; % Create translatation matrix
        arr_trnsR=[cos(var_trnsang) sin(var_trnsang) 0;-sin(var_trnsang) cos(var_trnsang) 0;0 0 1];
        tform=affine2d(arr_trnsT*arr_trnsR);

        img_BG_Side=imwarp(img_BG_SideOR,tform,'bilinear','OutputView',imref2d(size(img_current)),'FillValues',NaN); % Adjust BG Image for better BG substraction          
        
        var_surfslp=tan(var_srfang); % Save slope from surface inclination angle for fitting surface line to drop

%         % Mirror boundaries according to row next to erased rows from transformation
%         % Vertical translation
%         var_trpx=sign(tform.T(6))*ceil(abs(tform.T(6))); % Ceil number of tranlated pixels
%         if var_trpx>0 % Choose starting end for transformation
%             var_ctbnd=1;
%         else
%             var_ctbnd=size(img_BG_SideOR,1);
%         end
%         % Replace pixels of imwarp with last pixel row original BG image
%         img_BG_Side(var_ctbnd+var_trpx:-sign(var_trpx):var_ctbnd,1:size(img_BG_SideOR,2))=repmat(img_BG_SideOR(var_ctbnd,1:size(img_BG_SideOR,2)),abs(var_trpx)+1,1);
%              
%        % Horizontal translation
%         var_trpx=sign(tform.T(3))*ceil(abs(tform.T(3))); % Ceil number of tranlated pixels
%         if var_trpx>0 % Choose starting end for transformation
%             var_ctbnd=1;
%         else
%             var_ctbnd=size(img_BG_SideOR,2);
%         end
%         % Replace pixels of imwarp with last pixel row original BG image
%         img_BG_Side(1:size(img_BG_SideOR,1),var_ctbnd+var_trpx:-sign(var_trpx):var_ctbnd)=repmat(img_BG_SideOR(1:size(img_BG_SideOR,1),var_ctbnd),1,abs(var_trpx)+1);

                %% Image Analysis
        figure();
        subplot(2,2,1)
        imshowpair(img_BG_Side,img_original)
        subplot(2,2,2)
        imshow(img_BG_Side-img_current)
        var_bwthr=38;
        subplot(2,2,3)
        img_current=(img_BG_Side-img_current)>var_bwthr; % Convert to BW image
%         img_current=imfill(img_current,'holes'); % Fill holes
        
        imshow(img_current)