% Plot Threshold for Corona Splash
T=(-20:0.1:20);
Oh_crit=0.0044;
D=[2:0.1:3]*10^-3;
fig1=figure(1);
T_crit=nan(length(D),1);


hold on
% plot([min(T) max(T)], [Oh_crit Oh_crit],'k','LineStyle','--','Displayname','Corona splash threshhold');

for ii=1:5
    D_curr=D(ii);
    Oh=water_props(T,'eta')./(sqrt(D_curr.*water_props(T,'rho').*water_props(T,'sigma')));
    
    plot(T,Oh,'DisplayName',['D=',num2str(D(ii)*10^3),'mm'],'LineWidth',2);
    
    Oh_dif2Thr=abs(Oh-Oh_crit);
    T_crit(ii)=T(Oh_dif2Thr==min(Oh_dif2Thr));
    plot([T_crit(ii) T_crit(ii)],[0 Oh_crit],'k','LineStyle',':','LineWidth',1,'HandleVisibility','off')

end

ar=area([T(1) T(end)],[fig1.CurrentAxes.YLim(2) fig1.CurrentAxes.YLim(2)],Oh_crit,'HandleVisibility','off');
ar.FaceAlpha=0.1;
ar.FaceColor=[0.6 0.6 0.6];


hold off;
xlabel('T in �C')
ylabel('Oh')
legend('-DynamicLegend')


fig1=plotprops(fig1);

% print('D:\Promotion\Nebenprojekte\Ice Genesis\CoronaSplashThreshold','-dpng')