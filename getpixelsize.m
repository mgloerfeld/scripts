function [var_pixsize]=getpixelsize(FullFileDirectory,varargin) 

% getpixelsize% Function to get pixel size from Reference Image and save
% pixelsize for future analysis
%
%   Input: 
%             FullFileDirectory = string containing the file directory of
%             the Video file
% 
%   Output: Variabel 'var_pixsize' with pixelsize in m/px
%
%           
%
% AUTHOR: Markus Schremb,Mark Gloerfeld; 22.07.2019
bl_checkspec=false;
bl_skip=false;


    for ii=1:length(varargin)
    
    if bl_skip==true
        bl_skip=false;
        continue
    end
        
        switch varargin{ii}
            
            case 'CheckSpec'
                bl_checkspec=true;
                bl_skip=true;
                str_refdata=varargin{ii+1};
                
            otherwise
                disp(['Unkown input:',varargin{ii},'!'])
            return
        end
    end

    % Check for existing pixsize data (Choose standard name if 'CheckSpec'
    % is not entered
    if bl_checkspec==false
        str_refdata='var_pixsize';
    end
    
    if isfile([FullFileDirectory,str_refdata,'.mat'])
        load([FullFileDirectory,str_refdata,'.mat'],'var_pixsize')
    else 
    % Open calibration-image to set reference pixelsize
            [calibfilename, calibpathname]=uigetfile({[FullFileDirectory, '*CI*']}, 'Select a reference image file');
            imshow(fullfile(calibpathname, calibfilename));
            set(gcf, 'Position', get(0,'Screensize')); % Maximize figure
            [x_coord,y_coord] = ginput(2);

            prompt = {'Physical length in calibration image [m]:'};
            dlg_title = 'Spatial resolution calibration';
            num_lines = 1;
            def = {'0.000'};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            var_pixsize = str2double (answer (1))/sqrt((x_coord(2)-x_coord(1))^2+(y_coord(1)-y_coord(2))^2);
            % Save pixelsize variable
            str_saveas=questdlg('Do you want to give a specific pixsize name?','SaveName','Yes','No','Yes');
            if strcmp(str_saveas,'Yes') % Check if Pixel size needs to be saved with specific name
                SaveNameSpec= inputdlg('Enter specification for var_pixsize_specification:');
                save (fullfile(FullFileDirectory, ['var_pixsize_',SaveNameSpec{1},'.mat']),'var_pixsize');
            else
                save (fullfile(FullFileDirectory, 'var_pixsize.mat'),'var_pixsize');
            end
            
            close(gcf)
    end   
end
