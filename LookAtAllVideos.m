% Investigate all videos in a loop and add values manually

%   Videofolder='F:\Experiments\ResidualMass_Experiments\Experiments\';
 Videofolder='F:\Experiments\ResidualMass_extension\Experiments\';

Imagefiles = dir([Videofolder '**\images*']);
% CIHfiles = dir([Videofolder '**\*.cih']);

% log_unusables=(contains({CIHfiles.folder},'NOTUSABLE') | contains({CIHfiles.name},'NOTUSABLE') | contains({CIHfiles.name},'iced') | contains({CIHfiles.name},'Kopie')); % Search for marker 'NOTUSABLE' in Filename

% CIHfiles(log_unusables)=[];

breakupfrm=NaN(length(Imagefiles),1);

for ii=1:length(Imagefiles)
%     if strcmp(Imagefiles(ii).folder,CIHfiles(ii).folder)
    
        ImageLocation=[Imagefiles(ii).folder,'\',Imagefiles(ii).name];
        % Load images
        load(ImageLocation)
        m1=implay(images);
        m1.Parent.Position=[300 1200 1800 900];
        disp(['Current Video: ',Imagefiles(ii).name])
        pause

        ind_BrUP=inputdlg('Input Impact Frame','Impact Frame');
        
        
        breakupfrm(ii)=ind_BrUP;
        close(m1)
% 
%         CIHLocation=[CIHfiles(ii).folder,'\',CIHfiles(ii).name];
%         writeintocih(CIHLocation,'Corona_Breakup_Frame',str2double(ind_BrUP{:}))


        disp([CIHfiles(ii).name, 'changed!'])
    
%     else 
%         disp(['Attention ',ii,' is probably wrong cih file'])
%     end
end


