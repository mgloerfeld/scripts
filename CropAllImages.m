% Crop all images 

Datafolder='F:\Experiments\ImpactPositionVariation\CalibrationExperiments\C001_22�C_0ms_0\BG_Images\';


%% Get all files
Datafiles = [dir([Datafolder '**\*DI*']); dir([Datafolder '**\*BG*'])];

for ii=1:length(Datafiles)
    
    ImageLocation=[Datafiles(ii).folder,'\',Datafiles(ii).name];
    
    img_curr=imread(ImageLocation);
    img_curr=img_curr(:,150:end-150);
    
    
    imwrite(img_curr,ImageLocation)
    
end
