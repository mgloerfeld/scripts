d=0.001;
T= (-60:30);

Oh=water_props(T,'eta')./sqrt(water_props(T,'rho').*water_props(T,'sigma'));


figure();
hold on
plot(T,Oh)
scatter(T,Oh)
plot([T(1),T(end)],[0.0044,0.0044])
hold off

