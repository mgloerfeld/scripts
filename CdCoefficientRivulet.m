function [Cd_out]=CdCoefficientRivulet(Re_riv,theta)
%% FUNCTION THAT GIVES CD VALUE FOR RIVULET IN CROSS WIND BASED ON DATA FROM ADE (2019)

% Input: Re_riv - Reynolds number of rivulet
%        theta - Contact angle of water in corresponding surface in rad

% Output: C_d - Drag coefficient of rivulet in cross wind

theta=rad2deg(theta);


Data90=load('CdRivuletData90deg.mat');
Data90=Data90.Data;
Data60=load('CdRivuletData60deg.mat');
Data60=Data60.Data;
Data30=load('CdRivuletData30deg.mat');
Data30=Data30.Data;

%% Fit for 90 deg

modelfun = @(b,x)(b(1).*(x.^(b(2)))+b(3));
beta0=[1 -0.5 2];
pwlwcoeff90=nlinfit(Data90.Re_rivulet,Data90.Cd_90deg,modelfun,beta0);
pwlwcoeff60=nlinfit(Data60.Re_rivulet,Data60.Cd_60deg,modelfun,beta0);
pwlwcoeff30=nlinfit(Data30.Re_rivulet,Data30.Cd_30deg,modelfun,beta0);


% Caluclate Cd for input theta
arr_availdeg=[90,60,30];
Cd(:,1)=modelfun(pwlwcoeff90,Re_riv); 
Cd(:,2)=modelfun(pwlwcoeff60,Re_riv); 
Cd(:,3)=modelfun(pwlwcoeff30,Re_riv); 

[~,ind_int]=mink(abs(arr_availdeg-theta),2);

% coefflin=polyfit(arr_availdeg(ind_int),Cd(ind_int),1);


Cd_out=(Cd(:,ind_int(2))-Cd(:,ind_int(1)))./(arr_availdeg(ind_int(2))-arr_availdeg(ind_int(1)))*(theta-arr_availdeg(ind_int(1)))+Cd(:,ind_int(1));

% Cd_out(ii)=polyval(coefflin,theta(ii));


%% Validation Plot
% 
% figure();
% 
% scatter(Data90.Re_rivulet,Data90.Cd_90deg)
% hold on
% scatter(Data60.Re_rivulet,Data60.Cd_60deg)
% scatter(Data30.Re_rivulet,Data30.Cd_30deg)
% 
% Re_rivmod=0:1000;
% % plot(Re_rivmod,polyval(coeff90,Re_rivmod))
% % plot(Re_rivmod,polyval(coeff60,Re_rivmod))
% % plot(Re_rivmod,polyval(coeff30,Re_rivmod))
% 
% plot(Re_rivmod,modelfun(pwlwcoeff90,Re_rivmod))
% plot(Re_rivmod,modelfun(pwlwcoeff60,Re_rivmod))
% plot(Re_rivmod,modelfun(pwlwcoeff30,Re_rivmod))
% 
% scatter(Re_riv,Cd_out,'d','filled')
% 
% ylim([0, 3])


end