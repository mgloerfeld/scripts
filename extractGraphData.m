clear all
close all
input = 'C:\Benutzer\Desktop\';

format long e

[filename, input] = uigetfile( [input '\*.png'], 'Select graph');
diagram=imread(fullfile(input,filename));
imshow(diagram);

prompt = {'Logarithmic in X', 'Logarithmic in Y'};
dlg_title = 'Resolution and sequences';
num_lines = 1;
def = {'0','0'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
log_x = str2double (answer (1));
log_y = str2double (answer (2));

prompt = {'x1:', 'x2: ', 'y1: ', 'y2: '};
dlg_title = 'Resolution and sequences';
num_lines = 1;
def = {'0','1','0','1'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
x1_diag = str2double (answer (1));
x2_diag = str2double (answer (2));
y1_diag = str2double (answer (3));
y2_diag = str2double (answer (4));

%click x1, x2, y1, y2
[x, y]=ginput(4);

close all,
imshow(diagram);

[pixValues_x, pixValues_y]=ginput();
close all
both=[pixValues_x pixValues_y];
[Y,I]=sort(both(:,1));
both=both(I,:);
pixValues_x=both(:,1);
pixValues_y=both(:,2);

if log_x==1
values_x=x1_diag*(x2_diag/x1_diag).^(  (abs(pixValues_x-x(1))/abs(max(x(1:2,1))-min(x(1:2,1)))));
elseif log_x==0
    values_x=(pixValues_x-x(1))/(max(x(1:2,1))-min(x(1:2,1)))*(x2_diag-x1_diag)+x1_diag;
end

if log_y==1
values_y=y1_diag*(y2_diag/y1_diag).^( (abs(pixValues_y-y(3))/abs(max(y(3:4,1))-min(y(3:4,1)))));
elseif log_y==0
values_y=(pixValues_y-y(3))/-(max(y(3:4,1))-min(y(3:4,1)))*(y2_diag-y1_diag)+y1_diag;
end
%{
[xData, yData] = prepareCurveData( values_x, values_y );

% Set up fittype and options.
ft = fittype( 'a*((x))^(c)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [-Inf  -Inf -Inf];
%opts.StartPoint = [0.792207329559554 0.0494996713242111 0.959492426392903 0.655740699156587];
opts.Upper = [Inf Inf Inf];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
temp=fit( xData, yData, ft, opts );

MyCoeffs = coeffvalues(temp);
a = MyCoeffs(1);
%b = MyCoeffs(2);
%c = MyCoeffs(2);
d = MyCoeffs(2);

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
hold on
plot(values_x,values_y)
semilogx(values_x,values_y)
axis([min(x1_diag) 1.1*max(x2_diag) min(y1_diag) 1.1*max(y2_diag) ])
%legend(['a*(b-x)^c+d'], ['a=', num2str(a)  ], ['c=', num2str(c) ])
%plot (ivantsov(:,2),ivantsov(:,1))
hold off
%}

dlmwrite(fullfile(input,'extracted_data.txt'),[values_x values_y], 'precision', '%14.9e','delimiter','\t', 'newline', 'pc');
