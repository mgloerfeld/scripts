%% Calculate Parameters for FMP Drop Generator


D=500e-6; % Orifice Diameter in m
delta_P=1e2; % Pressure in Water container in Pa
T=20; % Water Temperature in �C
rho_w=water_props(T,'rho'); % Water density in kg/m^3

% Velocity and Volume Flow
U_D=sqrt(2*delta_P/rho_w); % Jet/Drop velocity in m/s
A=pi*(D/2)^2; % Orifice cross section in m^2
V_dot=U_D*A; % Volume flow through orifice


% Frequency spectrum
f_min=0.3*U_D/(pi*D); % Minimum frequency in Hz
f_max=0.9*U_D/(pi*D); % Maximum frequency in Hz
f_med=(f_min+f_max)/2;

% Drop diameter in m
d_med=(6*V_dot/(pi*f_med))^(1/3);

% Frequency Variation
f_band=(f_min:f_max); % Possible frequency band
arr_d=(6.*V_dot./(pi.*f_band)).^(1/3); % Possible Drop Diameter

%% Plot
fig1=figure();
plot(f_band,arr_d)

xlabel('f/Hz')
ylabel('d/m')
grid on 

fig1=plotprops(fig1);


