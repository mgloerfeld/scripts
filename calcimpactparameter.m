function [var_impdiam,var_sphrcty]=calcimpactparameter(img_impframe,img_BG)

% Calculate impact diameter
img_impframe=imfill((img_BG-images(:,:,ind_dropimp-1)>bw_th),'holes'); % Save frame just before impact
cl_boundaries=cl_boundaries(cellfun('size',cl_boundaries,1)>50);% Delete boundaries with to less coordinates to be a drop
% Find cell entry with lowest mean y-value 
arr_bndmean=cell2mat(cellfun(@mean,cl_boundaries,'UniformOutput',false)); % Calculates all means of coordinates from boundary entries
[~,ind_impdropbnd]=max(arr_bndmean(:,1)); % Get index of drop thats lower positioned in the picture
arr_boundaries=cell2mat(cl_boundaries(ind_impdropbnd));   % save boundary coordinates from drop                   
var_cutpart=0.05; % Percentage of height where boundary is cut to determine impact diameter
arr_bndcut=arr_boundaries(arr_boundaries(:,1)<min(arr_boundaries(:,1))+var_cutpart*(max(arr_boundaries(:,1))-min(arr_boundaries(:,1))),:);
[xc,yc,var_imprad]=circfit(arr_bndcut(:,2),arr_bndcut(:,1));
var_impdiam=2*var_imprad*var_pixsize;
%Test
figure();
 scatter(arr_boundaries(:,2),arr_boundaries(:,1));hold on;
 scatter(arr_bndcut(:,2),arr_bndcut(:,1)); 
 plot(arr_boundaries(:,2),-(sqrt(var_imprad.^2-(arr_boundaries-xc).^2)-yc));
hold off

% Calculate Sphericity at impact
stc_impregprops=regionprops(img_impframe,'Perimeter','Area');
[~,ind_maxobjimp]=max(cat(1,stc_impregprops(:).Area));
var_sphrcty=2*(sqrt(pi*stc_impregprops(ind_maxobjimp).Area)/(stc_impregprops(ind_maxobjimp).Perimeter));