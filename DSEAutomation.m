% Automated Routine for estimating DropSizes of calibration 
% Datafolder='F:\Experiments\Residualmass_extension\Experiments\';
% Datafolder='F:\Experiments\ResidualMass_Experiments\Experiments\';
% Datafolder='C:\Users\ui16ryno\Desktop\Tropfenbestimmung_Test\neu\VideomitBildern\';
% Datafolder='F:\Residualmass_extension\Glycerin Versuche\';
Datafolder='F:\Experiments\ImpactPositionVariation_V2\Experiments\20210908\';
% Datafolder='F:\Experiments\ImpactPositionVariation_V2\Experiments\';


%% Get all files
Datafiles = dir([Datafolder '**\*V*']);
Datafiles=Datafiles(cat(1,Datafiles.isdir));
% DropSizes=table('Size',[length(Calibfiles) 1],'VariableTypes',{'double'});
% DropSizes.Properties.VariableNames={'MeasuredVolume'};

if isempty(Datafiles)
    disp('No files found! Check folder or file marker in dir command!')
end


for ll=1:length(Datafiles)

    Location=[Datafiles(ll).folder,'\',Datafiles(ll).name,'\'];

    % Skip Folder if no images exist
    if exist([Location,'BG_Side.tif'],'file')~=2
        disp([Location,' skipped'])
        continue
    end

    [var_dropvolume]=DropSizeEstimation(Location,'SetBWThreshold',50); % Estimate Drop Volume from rotated side view images 

%         writeintocih(Location,'Excentricity_after_impact',var_excentricity,'Precision',3); % Write Drop Volume into cih file

        writeintocih(Location,'Residual_volume',var_dropvolume,6); % Write Drop Volume into cih file

    disp([Location,' value succesfully added!'])
end