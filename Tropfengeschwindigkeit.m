Location='C:\Users\ui16ryno\Desktop\Promotion\Messungen\Videos\Dropshot\20190417_2\'; % Video Folder Location
Location='C:\Users\ui16ryno\Desktop\Promotion\Messungen\Videos\Dropshot\20190515\'; % Video Folder Location
    %% Get all videos found in Location
    DataFiles = dir([Location '**\*.avi']);
    FileNames = {DataFiles.name};
    FileFolder={DataFiles.folder};
    % Check for marked unusable videos and remove from postprocessing
    log_unusables=~contains(FileNames,'NOTUSABLE'); % Search for marker 'NOTUSABLE' in Filename
    FileNames =FileNames(log_unusables);
    FileFolder=FileFolder(log_unusables);
    % Get number of videos
    numvids=size(FileNames,2);
    
   % Search for maximum number of frames for preallocation
   max_frame=1;
   for ii=1:numvids
       curr_frno=getdatafromcih([FileFolder{ii},'\',strrep(FileNames{ii},'avi','cih')],'NoF');
       if max_frame<curr_frno
           max_frame=curr_frno;
       end
   end
   
    
    %% Preallocation for all videos
    % Preallocate table for dropshot data
    tbl_dropshot=table(NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),NaN(max_frame,1),false(max_frame,1),...
        'VariableNames',{'Radius_Av','Radius_Std','Perimeter','Sphericity','Area','Height','Width','FlyTime','FlyDistance','DropVis'});
    %Preallocate Struct for all videos
    stc_AllVID=repmat(struct('AveragedData',struct('DropSpeed',NaN,'Area_Av',NaN,'ShotPressure',NaN,'Diam_Av',NaN,'WeberNumber',NaN,'Sphericity_Av',NaN,'FileDirectory',strings),'SingleFrameData',tbl_dropshot),numvids,1);
%% Loop through all videos
    for jj=1:numvids
        
    VideoName=erase(FileNames{jj},'.avi'); %Video Name
    Filetype='.avi'; % Type of video
    FileFullDirectory=[FileFolder{jj},'\',FileNames{jj}]; % Concatenate Filename
    if isfile([FileFolder{jj},'\images_',VideoName,'.mat'])
        load([FileFolder{jj},'\images_',VideoName,'.mat'])
    else
        images=getimagefromvideo(FileFullDirectory); % Get Frames from video into one array
        save([FileFolder{jj},'\images_',VideoName,'.mat'],'images') 
    end
    % Save FileDirectory to Struct
     stc_AllVID(jj).AveragedData.FileDirectory=[FileFolder{jj},VideoName];
    
    % Cut Table for single frame data to right size determined by number of frames of current video
    var_curr_NoF=getdatafromcih([FileFolder{jj},'\',strrep(FileNames{jj},'avi','cih')],'NoF');
    stc_AllVID(jj).SingleFrameData=stc_AllVID(jj).SingleFrameData(1:var_curr_NoF,:);
    
    % Get shooting pressure from filename and save to stc_AllVID
    var_press=strtok(FileNames{jj},'bar');
    var_press=strrep(var_press,'_','.');
    stc_AllVID(jj).AveragedData.ShotPressure=str2double(var_press);
    
    %% Get Pixel Size from Calibration Image or load Calibration
    str_location=Location;
    str_refimg='ReferenceImage';
    str_refdata='var_pixsize';
    if isfile([Location,str_refdata,'.mat'])
        load([Location,'var_pixsize'])
    else 
    % Open calibration-image to set reference pixelsize
            [calibfilename, calibpathname]=uigetfile({[str_refimg '*.png']}, 'Select a video file');
            imshow(fullfile(calibpathname, calibfilename));
            set(gcf, 'Position', get(0,'Screensize')); % Maximize figure
            [x_coord,y_coord] = ginput(2);

            prompt = {'Physical length in calibration image [m]:'};
            dlg_title = 'Spatial resolution calibration';
            num_lines = 1;
            def = {'0.06'};
            answer = inputdlg(prompt,dlg_title,num_lines,def);
            var_pixsize = str2double (answer (1))/sqrt((x_coord(2)-x_coord(1))^2+(y_coord(1)-y_coord(2))^2);
            save (fullfile(str_location, 'var_pixsize.mat'),'var_pixsize');
            close(gcf)
    end   

    %% Get Information for video from background image
    img_BG=images(:,:,1);
    bw_th=10; %30;
    area_th=50;
    var_imgwdth=length(images(1,:,1));
    var_imghght=length(images(:,1,1));
    var_fps=getdatafromcih([FileFolder{jj},'\',VideoName,'.cih'],'fps'); % Get FPS from cih file
    
    % Get boundary of shooting stopper as lower limit
    img_BGbw=img_BG<bw_th;
    stc_BG=regionprops(img_BGbw,'Centroid','Area','Extrema'); % Regionprops to get image data of background
    % DEFINE BOUNDARY LINE
    if ~isempty(stc_BG)
        arr_BGextrema=cat(2,stc_BG.Extrema); % Write all extrema to one variable
        [var_bound,ind_bound]=min(min(arr_BGextrema(:,2:2:end))); % Find minimum value (highest point of pipe in picture)
    else
        var_bound=var_imghght;
    end
    images=images(1:ceil(var_bound),:,:);
    img_BG=img_BG(1:ceil(var_bound),:);
    %% Booleans
    bl_skipent=false;
    bl_skipmirr=false;
    bl_skipimp=false;

    %Preallocation
    stc_VID.Data=repmat( struct( 'Area', NaN , 'Centroid', [NaN NaN], 'Extrema', NaN(8,2),'Boundary',NaN(1,2)),length(images(1,1,:)), 1 ); % Preallocate a struct in which all data is restored
    log_emptyimg=true(length(images(1,1,:)),1);
    arr_centroid_curr=[];
    arr_area_curr=[];
    arr_extrema_curr=[];
    var_arealog=0;
    ind_dropent=0;
    
    for ii=1:length(images(1,1,:)) % Go through all images
        
        current_frame=(img_BG-images(:,:,ii))>bw_th; % Turn frame into binary image
        current_frame=imfill(current_frame,'holes'); % Fill holes
        stc_regprops=regionprops(current_frame,'Centroid','Area','Extrema','Perimeter'); % REGIONPROPS TO GET IMAGE DATA
        ind_del=cat(1,stc_regprops.Area)<area_th; % Search for areas to small to be a drop
        stc_regprops(ind_del)=[]; % Delete all objects smaller than Area threshold
             
        if ~isempty(stc_regprops)
            cl_Boundary=bwboundaries(current_frame); % Get boundary coordinates
            cl_Boundary=cl_Boundary(~ind_del); % Delete objects smaller than Area threshold

            arr_centroid_curr=cat(1,stc_regprops.Centroid); % Concatenate Centroid coordinates to array
            arr_area_curr=cat(1,stc_regprops.Area); % Concatenate Area to array
            arr_extrema_curr=cat(2,stc_regprops.Extrema); % Concatenate Extrema coordinates to array
            arr_perimeter_curr=cat(1,stc_regprops.Perimeter); % Concatenate Perimeter to array
            
            % Find index of object closest to drop size and centroid area if multiple objects detected
            arr_mobjdiff=abs(arr_area_curr-var_arealog);
            if sum(abs(arr_mobjdiff(2:end)- arr_mobjdiff(1:end-1))<100)==0 % Check if differences in area are close together
                [~,ind_drop]=min(abs(arr_area_curr-var_arealog));
            else
                [~,ind_drop]=min(sqrt(sum(abs(arr_centroid_curr-var_centrlog).^2,2))); % Get index of minimum difference of current centroids to centroid of drop in last image
            end

            var_arealog=arr_area_curr(ind_drop); % Save current drop area for comparison in next frame
            var_centrlog=arr_centroid_curr(ind_drop); % Save current drop centroid coordinates for comparison in next frame
%% Set Flags for drop image entry and leaving          
            % Find first frame in which drop is fully visible
            if bl_skipent==false && stc_regprops(ind_drop).Extrema(5,2)<var_bound-0.5 && stc_regprops(ind_drop).Extrema(6,2)<var_bound-0.5 % Find first frame in which drop is fully visible
                ind_dropent=ii;
                bl_skipent=true;
            elseif ind_dropent==0
                continue
            end
            
            % Break loop if drop touches upper image boundary
            if min(min(arr_extrema_curr(:,2:2:end)))< 1
                ind_dropex=ii-1;
                log_emptyimg(ii:end)=false;
                break
            end
%% Calculate single image data            
            % Process data from bwboundary
            arr_bound_curr=cat(2,cl_Boundary{ind_drop,1}); % Concatenate boundary from cell to array
            arr_bound_curr=arr_bound_curr(:,end:-1:1); % Rearrange coordinates to first column: x, second column: y
            
            % Calculate local radius of drop
            arr_bound_diff=abs(arr_bound_curr-arr_centroid_curr(ind_drop,:)); % Calculate difference from centroid to boundary points
            arr_radii=sqrt(arr_bound_diff(:,1).^2+arr_bound_diff(:,2).^2); % Calculate radii from centroid to boundary points
            var_radii_av=mean(arr_radii); % Calculate mean radius
            var_radii_std=std(arr_radii); % Calculate standard deviation of radii
            
            
            
%% Write data to struct with drop edge detection outcomes (All pixel Values!!!)
            stc_VID.Data(ii).Centroid=arr_centroid_curr(ind_drop,:);
            stc_VID.Data(ii).Area=arr_area_curr(ind_drop);
            stc_VID.Data(ii).Extrema=arr_extrema_curr(:,2*ind_drop-1:2*ind_drop);
            stc_VID.Data(ii).Boundary=arr_bound_curr;
            stc_VID.Data(ii).Radius_Av=var_radii_av;
            stc_VID.Data(ii).Radius_Std=var_radii_std;
            stc_VID.Data(ii).Perimeter=arr_perimeter_curr(ind_drop);
        else
            log_emptyimg(ii)=false; % If nothing is detected set flag to boolean "log_emptyimg"
        end
        
    end

    %% Set Flags
    % Set up logical array with flags for flight
    log_dropvis=false(length(images(1,1,:)),1); 
    log_dropvis(ind_dropent:end)=true;
    log_dropvis=and(log_dropvis,log_emptyimg);
    stc_VID.Info.Flight=log_dropvis;



    %% Calculate Parameters
    % Position of CENTROID in Flight
    arr_drop_pos=cat(1,stc_VID.Data.Centroid); % Get Centroid coordinates from every frame
    arr_drop_pos=arr_drop_pos(stc_VID.Info.Flight,:); % Get Centroid coordinates from flight
    arr_drop_flydist=cumsum([0;sqrt((arr_drop_pos(2:end,1)-arr_drop_pos(1:end-1,1)).^2+(arr_drop_pos(2:end,2)-arr_drop_pos(1:end-1,2)).^2)]*var_pixsize); % Get distance travelled through flight in [m]
    arr_drop_flytime=linspace(0,((sum(stc_VID.Info.Flight)-1)/var_fps),sum(stc_VID.Info.Flight))'; % Create time scale for linear fit
    
    % Calculate Drop Speed (Considering centroid)
    [arr_fitpar,arr_fiterr]=polyfit(arr_drop_flytime,arr_drop_flydist,1); % Assume linear fit for position
    var_dropspeed=arr_fitpar(1); % Get speed from first parameter from linear fit in m/s
     
     % Calculate Weber number
     var_rho=water_props(20,'rho'); % Get water density at 23�C
     var_sigma=72.75e-3; % Get water surface tension at 20�C
     var_WeDiam=sqrt(mean(cat(1,stc_VID.Data(stc_VID.Info.Flight).Area))*var_pixsize^2/pi); % Calculate Diameter from Average Area
     var_We=(var_rho*var_dropspeed^2*var_WeDiam)/(var_sigma); % Calculate Weber number
     
     
     %% Save data to table for data analysis
            stc_AllVID(jj).AveragedData.DropSpeed=var_dropspeed; % Save drop speed in m/s
            stc_AllVID(jj).AveragedData.Area_Av= mean(cat(1,stc_VID.Data(stc_VID.Info.Flight).Area))*var_pixsize^2; % Save mean area during flight in m^2
            stc_AllVID(jj).AveragedData.Diam_Av= var_WeDiam; % Save mean diameter during flight in m^2
            stc_AllVID(jj).SingleFrameData.DropVis=stc_VID.Info.Flight; % Save flags of drop visibility in image
            stc_AllVID(jj).SingleFrameData.Area=cat(1,stc_VID.Data.Area)*var_pixsize^2; % Save Area of every single frame (cell array) in m^2
            stc_AllVID(jj).SingleFrameData.FlyDistance(ind_dropent:ind_dropex)=arr_drop_flydist; % Save Position of Drop 
            stc_AllVID(jj).SingleFrameData.FlyTime(ind_dropent:ind_dropex)=arr_drop_flytime; % Save FlyTime
            stc_AllVID(jj).SingleFrameData.Radius_Av(ind_dropent:ind_dropex)=cat(1,stc_VID.Data(stc_VID.Info.Flight).Radius_Av)*var_pixsize; % Save Average Drop Radius in every frame [m]
            stc_AllVID(jj).SingleFrameData.Radius_Std(ind_dropent:ind_dropex)=cat(1,stc_VID.Data(stc_VID.Info.Flight).Radius_Std)*var_pixsize; % Save Standard Deviation of Drop Radius in every frame [m]
            stc_AllVID(jj).SingleFrameData.Perimeter(ind_dropent:ind_dropex)=cat(1,stc_VID.Data(stc_VID.Info.Flight).Perimeter)*var_pixsize; % Save Perimeter of Drop
            stc_AllVID(jj).SingleFrameData.Sphericity=2*sqrt((stc_AllVID(jj).SingleFrameData.Area.*pi))./(stc_AllVID(jj).SingleFrameData.Perimeter); % Save Sphericity of each drop image
            stc_AllVID(jj).AveragedData.Sphericity_Av=mean(stc_AllVID(jj).SingleFrameData.Sphericity,'omitnan'); % Save averaged Sphericity during flight
            stc_AllVID(jj).AveragedData.WeberNumber=var_We; % Save Weber Number avergaged over flight
    end
    

%% Sort Stc_AllVID after Velocities 
arr_DSsort=NaN(1,length(stc_AllVID))';
    for ll=1:length(stc_AllVID)
        arr_DSsort(ll)=stc_AllVID(ll).AveragedData.DropSpeed;
    end
[~,ind_DSsort]=sort(arr_DSsort);
stc_AllVID=stc_AllVID(ind_DSsort);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Plots %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot default values
fntsz=32;
FigSvLoc='C:\Users\ui16ryno\Desktop\Promotion\Media\Pr�sentationen\IMCF 2019\';
bl_save=true; % Boolean to activate save options


%% Plot DropSpeed over Shooting pressure
fig1=figure('Position', get(0, 'Screensize'));
hold on
for kk=1:numvids
   scatter(stc_AllVID(kk).AveragedData.ShotPressure,stc_AllVID(kk).AveragedData.DropSpeed,300,'b','Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
   xlabel('Shooting Pressure [bar]')
   ylabel('Drop Speed [m/s^2]')
%    legend('-DynamicLegend','Location','Southeast')
end
hold off
% Make figure nice and save it
set(gca,'FontSize',fntsz);
title('Drop Speed over Shooting Pressure','FontSize',fntsz+18)
set(gca,'FontName','Calibri');
set(gcf,'color','w');
if bl_save==true
    plotframe=getframe(fig1);
    imwrite(plotframe.cdata,[FigSvLoc,'SpeedOverPressure.png'],'png')
end
%% Plot Sphericity over DropSpeed
fig2=figure('Position', get(0, 'Screensize'));
hold on
for kk=1:numvids
   scatter(stc_AllVID(kk).AveragedData.DropSpeed,stc_AllVID(kk).AveragedData.Sphericity_Av,300,'r','Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
   xlabel('Drop Speed [m/s]')
   ylabel('Sphericity')
%    legend('-DynamicLegend','Location','Southeast')
end
hold off
% Make figure nice and save it
set(gca,'FontSize',fntsz);
set(gca,'FontName','Calibri');
set(gcf,'color','w');
title('Sphericity over DropSpeed','FontSize',fntsz+18);
if bl_save==true
    plotframe=getframe(fig2);
    imwrite(plotframe.cdata,[FigSvLoc,'SphericityOverDropSpeed.png'],'png')
end
%% Plot Sphericity over Weber Number
fig3=figure('Position', get(0, 'Screensize'));
hold on
for kk=1:numvids
   scatter(stc_AllVID(kk).AveragedData.WeberNumber,stc_AllVID(kk).AveragedData.Sphericity_Av,300,'Displayname',['\approx', num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
   xlabel('Weber Number')
   ylabel('Sphericity')
%    legend('-DynamicLegend','Location','Southeast')
end
hold off
% Make figure nice and save it
set(gca,'FontSize',fntsz);
set(gca,'FontName','Calibri');
set(gcf,'color','w');
title('Sphericity over Weber Number','FontSize',fntsz+18);
if bl_save==true
    plotframe=getframe(fig3);
    imwrite(plotframe.cdata,[FigSvLoc,'SphericityOverWe.png'],'png')
end
%% Plot Sphericity over FlyDistance with 3 Examples
fig4=figure('Position', get(0, 'Screensize'));
hold on
for kk=[1,numvids/2,numvids-1]
  
   scatter(stc_AllVID(kk).SingleFrameData.FlyDistance,stc_AllVID(kk).SingleFrameData.Sphericity,300,'Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
   xlabel('Distance from Accelerator Exit [m]')
   ylabel('Sphericity')
   legend('-DynamicLegend','Location','Southeast')
end
hold off
% Make figure nice and save it
set(gca,'FontSize',fntsz);
set(gca,'FontName','Calibri');
set(gcf,'color','w');
title('Sphericity over Distance from Exit','FontSize',fntsz+18);
if bl_save==true
    plotframe=getframe(fig4);
    imwrite(plotframe.cdata,[FigSvLoc,'SphericityOverDistance_3Ex.png'],'png')
end
%% Plot y position over time
% figure();
% plot(arr_drop_flytime,arr_drop_flydist,'x',arr_drop_flytime,polyval(arr_fitpar,arr_drop_flytime),'-')

%% Plot area over position
% figure();
% hold on
% for kk=1:numvids
%    scatter(stc_AllVID(kk).SingleFrameData.FlyDistance,stc_AllVID(kk).SingleFrameData.Area,'Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
%    xlabel('Distance [m/s]')
%    ylabel('Area [m^2]')
%    legend('-DynamicLegend','Location','Southeast')
% end
% title('Drop Area over position')
% hold off
%% Plot averaged radius over Flydistance
% figure();
% hold on
% for kk=1:numvids
%    scatter(stc_AllVID(kk).SingleFrameData.FlyTime,stc_AllVID(kk).SingleFrameData.Radius_Av,'Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
%    xlabel('Distance [m/s]')
%    ylabel('Averaged Radius [m]')
%    legend('-DynamicLegend','Location','Southeast')
% end
% title('Averaged Radius over position')
% hold off
%% Plot Normalized Radius Standard Deviation over FlyDistance
% figure();
% hold on
% for kk=1:numvids
%    if kk<=9
%     scatter(stc_AllVID(kk).SingleFrameData.FlyDistance,(stc_AllVID(kk).SingleFrameData.Radius_Std./stc_AllVID(kk).SingleFrameData.Radius_Av),'Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
%    else
%     scatter(stc_AllVID(kk).SingleFrameData.FlyDistance,(stc_AllVID(kk).SingleFrameData.Radius_Std./stc_AllVID(kk).SingleFrameData.Radius_Av),'filled','Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
%    end
%    xlabel('Distance [m]')
%    ylabel('Radius Standard Deviation/Radius Averaged')
%    legend('-DynamicLegend','Location','Northeast')
% end
% title('Normalized Radius Standard deviation over position')
% hold off

 %% Plot Averaged Sphericity over DropSpeed
% figure();
% hold on
% for kk=1:numvids
%     scatter(stc_AllVID(kk).AveragedData.DropSpeed,mean(stc_AllVID(kk).SingleFrameData.Sphericity,'omitnan'),'Displayname',[num2str(round(stc_AllVID(kk).AveragedData.DropSpeed,1)),'m/s'])
%    xlabel('DropSpeed [m/s]')
%    ylabel('Sphericity')
% end
% title('Sphericity over DropSpeed')
% hold off
%% Plot results of edge detection
% figure(); 
% startframe=10;
% kk=1;
% for ii=11
%     framenr=ii;
% %     subplot(2,2,kk)
%     axis tight
%     imshow(images(:,:,framenr))
%     hold on
%     scatter(stc_VID.Data(framenr).Centroid(1),stc_VID.Data(framenr).Centroid(2))
% %     scatter(stc_VID.Data(framenr).Extrema(:,1),stc_VID.Data(framenr).Extrema(:,2))
% %     scatter(stc_VID.Data(framenr).Boundary(:,1),stc_VID.Data(framenr).Boundary(:,2))
%    kk=kk+1; 
% end
% hold off


