function writeintocih(Location,str_valuename,var_setvar,varargin)% Write information into Cih 
%% WRITEINTOCIH Function to write values from any script into cih information file of experiment

%   Syntax
%   -----------------------------------------------
%   WRITEINTOCIH(Location,str_valuename,var_setvalue)
%
%   Description
%   ----------------------------------------------
%   This function writes values from any script 
%   into cih information file of experiment
%

%   Default settings are: 
%   
%   Input:  
%   ----------------------------------------------
%           Location : String that containns path to folder containing cih
%           (including "\" at the end)
%           str_valuename: Name of Value to be set as it is written in cih
%           var_setvar: Value or replacement name that is written to cih file
%           var_pres: Precision of numeric value written to file
%           
%           varargin:
%                   'Precision' - Option to set precision of numeric values written
%                   to file
%                   'Add' - Option to add row behind an existing row named
%                   by str_valuename
%                   'Replace' - Option to replace name of a line in cih
%                   file
%   Output: 
%   ----------------------------------------------
%   ----------------------------------------------
%
%
%   Author: Mark Gloerfeld
%   Date: 16122020

%% Check if Location is valid
    if contains(Location,'.cih') % Check if Location is cih file name or location of file
        FullFileDirectory=Location;
    else
         tempfile=dir([Location,'*.cih']); % Find txt file that contains Drop_Temperature etc.
        
         if isempty(tempfile) % If no cih file is in location return and display message
            disp('No Cih-Files found, check Location Folder!')
            return
         end
        FullFileDirectory=[tempfile.folder,'\',tempfile.name];
        

    end   
    %% Check if line should be added
   bl_set=true;
   bl_add=false; 
   bl_replace=false;
   bl_skip=false;
   
   var_prec=5; % Use 5 digits precision for input as default
   
   for ii=1:length(varargin)
    
       if bl_skip==true
            bl_skip=false;
            continue
       end
       
        switch varargin{ii}
            case 'Precision'
                var_prec=varargin{ii+1}; % Change precision of input if desired

            case 'Add'
                bl_add=true;
                bl_set=false;
                str_aboveline=varargin{ii+1};
                bl_skip=true;
                
            case 'Replace'
                bl_replace=true;
                bl_set=false;
                str_replacename=var_setvar;
        end
    end
    
    
    
    % Open Cih File
    fileID=fopen(FullFileDirectory); % open file 
    cl_info=textscan(fileID,'%s',300,'Delimiter','\n'); % get information from txt file
    cl_info=cl_info{1,1}; % Save information to info
    fclose(fileID); % Close file
    
    if bl_set==true
        ind_setval=contains(cl_info,str_valuename); % Find index of line with value to set in array
        cl_info{ind_setval}=[str_valuename,': ',num2str(var_setvar,var_prec)]; % Replace value
    end
    
    if bl_add==true
        ind_setval=find(contains(cl_info,str_aboveline)); % Find index of line with value to set in array
        if ~isempty(ind_setval)
            cl_insert={[str_valuename,': ',num2str(var_setvar,var_prec)]}; % Create entry that should be added
            cl_info=[cl_info(1:ind_setval); cl_insert; cl_info(ind_setval+1:end)];
        else
            disp(['No line named "',str_aboveline,'" found! No line will be added!'])
            return
        end
    end
    
    if bl_replace==true
        ind_replval=find(contains(cl_info,str_valuename)); % Find index of line with value to set in array
        if ~isempty(ind_replval)
             cl_info{ind_replval}=strrep(cl_info{ind_replval},str_valuename,str_replacename); % Replace name in line
        end
    end

    % Write cell to cih file
    fileIDcihw=fopen(FullFileDirectory,'wt');
    fprintf(fileIDcihw,'%s\n',cl_info{:});
    fclose(fileIDcihw);
end