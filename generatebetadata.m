function [arr_beta,arr_We,arr_Re,arr_Oh]=generatebetadata(arr_v,arr_dropdiam,str_fluid,str_gas,varargin)
% Generate artificial beta data for given velocity range vor comparison


bl_Tlchng=false;
bl_Tgchng=false;
bl_VisChng=false;
bl_skip=false;
bl_kappa=false;


for ii=1:length(varargin)
    
    if bl_skip==true
        bl_skip=false;
        continue
    end
    
    switch varargin{ii}
        
        case 'FluidTemp'
            bl_Tlchng=true;
            bl_skip=true;
            var_Tl=varargin{ii+1};
            
        
        case 'GasTemp'
            bl_Tgchng=true;
            bl_skip=true;
            var_Tg=varargin{ii+1};
            
        case 'ArtViscosity'
            bl_VisChng=true;
            bl_skip=true;
            arr_arteta=varargin{ii+1};
            
        case 'DeformedDrop'
            bl_kappa=true;
            bl_skip=true;
            arr_kappa=varargin{ii+1};
    end
    
end


if bl_Tlchng==false % Set defaulft liquid temperature
    var_Tl=0;  
end

if bl_Tgchng==false % Set default gas temperature
    var_Tg=0;  
end

if bl_kappa==false
    arr_kappa=1./(0.5*arr_dropdiam);
end


%% Fluid properties
switch str_fluid
    
    case 'water' % Fluid is water at 0�C

        var_rho_l=water_props(var_Tl,'rho');
        var_sigma_l=water_props(var_Tl,'sigma');
        var_eta_l=water_props(var_Tl,'eta');
        
    case 'ethanol' % Fluid is ethanol at 20�C
        
        var_rho_l=789;
        var_sigma_l=22.5e-3;
        var_eta_l=1.21e-3;
end

if bl_VisChng==true % Overwrite viscosity with artifical values if needed
    var_eta_l=arr_arteta;
end

%% Gas properties
switch str_gas
    case 'air' % Gas is air 
        
        var_rho_g=air_props(var_Tg,'rho');
        var_eta_g=air_props(var_Tg,'eta');
    case 'nitrogen' % Gas is nitrogen at 0�C
        
        var_rho_g=1.234;
        var_eta_g=16.65e-6;
    case 'carbondioxide' % Gas is carbon dioxide
        
        var_rho_g=1.951;
        var_eta_g=13.71e-6;
end
% Calculate lambda
var_pamb=1.013e5; % Ambient Pressure
var_cmol=sqrt(8*var_pamb/(pi*var_rho_g)); % Mean velocity of molecules
var_lambda_g=var_eta_g/(0.499*var_cmol*var_rho_g); % Mean free path of molecules

arr_curvcorrL=1./(arr_kappa.*0.5.*arr_dropdiam);

arr_Re=var_rho_l.*arr_v.*(0.5*arr_dropdiam).*arr_curvcorrL./var_eta_l; % Re with radius
arr_We=var_rho_l.*arr_v.^2.*(0.5*arr_dropdiam).*arr_curvcorrL./var_sigma_l; % We with radius
arr_Oh=sqrt(arr_We)./arr_Re;



%Preallocate critical time
numdata=max([length(arr_v),length(arr_dropdiam),length(var_eta_l)]);
arr_te=NaN(1,numdata);
syms t_e
for ll=1:numdata
% Determine t,e_crit
var_tecurr=vpasolve((arr_Re(ll)^-2)*(arr_Oh(ll)^-2)==(1.1)^2*(t_e^(3/2))-sqrt(3)/2*(arr_Re(ll)^-1)*t_e^(-1/2),t_e,[0 1]);
arr_te(ll)=double(var_tecurr);
end

arr_curvcorrV=((arr_kappa.*0.5.*arr_dropdiam).^(-1/2));

U_t=arr_curvcorrV.*(sqrt(3)/2).*arr_v.*arr_te.^(-1/2);
% U_t=(sqrt(3)/2).*arr_dropspeed.*arr_te.^(-1/2); % R&G Original
H_t=arr_curvcorrL.*(0.5.*arr_dropdiam).*sqrt(12)./pi.*arr_te.^(3/2); % Corrected Version R&G 2020 with curvature correction
% H_t=(0.5.*arr_avdiamExp0AV).*sqrt(12)./pi.*arr_te.^(3/2); % Corrected Version R&G 2020
K_u=0.3;
alpha=deg2rad(60); % Wegde angle (probably can be ADJUSTED)

% Calculate beta
K_l=-(6./tan(alpha).^2).*(log(19.2.*var_lambda_g./H_t)-log(1+19.2.*var_lambda_g./H_t));
arr_beta=sqrt((K_l.*var_eta_g.*U_t+K_u.*var_rho_g.*U_t.^2.*H_t)./(2.*var_sigma_l)); % 2 in denominator added due correctino of R&G 2020


end