
%% Linear fit function
bl_linfit=true;
bl_nlinfit=true;

if bl_linfit==true
    for ii=1:4
        ind_sel=abs(arr_icefr-(0.09+ii*0.02))<0.01;

        gamma_dot=[arr_dropspeed(ind_sel)./(0.5.*arr_avdiam(ind_sel))];
        arr_Ymodel=[arr_Ymod_gammalim];
        coeff_ft=polyfit(gamma_dot,arr_Ymodel(ind_sel),1);


        slps(ii)=coeff_ft(1);
        Y0(ii)=coeff_ft(2);
        cfig=figure(1);
        hold on
        % scatter(gamma_dot,arr_Ymodel(ind_sel),'filled','Displayname',['\xi_{ice} \approx ',num2str(0.09+ii*0.02)])
        scatter(gamma_dot,arr_Ymodel(ind_sel),'filled','HandleVisibility','off')
        
        X=min(gamma_dot):max(gamma_dot);
        cfig.CurrentAxes.ColorOrderIndex=cfig.CurrentAxes.ColorOrderIndex-1;
        plot(X,polyval(coeff_ft,X),'Displayname',['\xi_{ice} \approx ',num2str(0.09+ii*0.02)])


        xlabel('$$ U_0/D_0 $$','interpreter','latex')
        ylabel('Y','interpreter','latex')
        legend('-DynamicLegend')
        cfig=plotprops(cfig);
        hold off
        SaveName='ShearRateDependenceYconst';
        print(['D:\Promotion\Messungen\Ergebnisse\DendriticImpact\',SaveName],'-dpng')
    
    end
        
    
    
end

%% Power Law fit with finite limit
if bl_nlinfit==true
     cfig=figure(1);
    clf(cfig)
    for ii=1:4
        ind_sel=abs(arr_icefr-(0.09+ii*0.02))<0.01;

        gamma_dot=[arr_dropspeed(ind_sel)./(0.5.*arr_avdiam(ind_sel))];
        arr_Ymodel=arr_Ymod(ind_sel);
        
        chi(ii)=max(arr_Ymodel)./min(arr_Ymodel)-1;
        Y_min(ii)=min(arr_Ymodel);
%         modelfun= @(b,x)(b(1).*(1+b(2)-b(2).*exp(-b(3).*x)));
%         beta0=[Y_min,chi,1e-3];
        modelfun= @(b,x)(Y_min(ii).*(1+chi(ii)-chi(ii).*exp(-b(1).*x)));
        beta0=[1e-3];
        coeff_ft=nlinfit(gamma_dot,arr_Ymodel,modelfun,beta0);
%         coeff_ft=beta0;

        tau(ii)=coeff_ft(1);
       
        hold on
        % scatter(gamma_dot,arr_Ymodel(ind_sel),'filled','Displayname',['\xi_{ice} \approx ',num2str(0.09+ii*0.02)])
        scatter(gamma_dot,arr_Ymodel,'filled','HandleVisibility','off')

%         X=0:max(gamma_dot);
        X=0:10e4;
        cfig.CurrentAxes.ColorOrderIndex=cfig.CurrentAxes.ColorOrderIndex-1;
        plot(X,modelfun(coeff_ft,X),'Displayname',['\xi_{ice} \approx ',num2str(0.09+ii*0.02)])

        
        xlabel('U_0/D_0')
        ylabel('Y')
        legend('-DynamicLegend','Location','Best')
        cfig=plotprops(cfig);
        set(gca, 'XScale', 'log')
        hold off
    end
    
    
    hold on
    
    modelfunav= @(x)((1+mean(chi)-mean(chi).*exp(-mean(tau).*x)));
%     avfig=figure();
    plot(X,mean(Y_min).*modelfunav(X),'--','Displayname','Mean Fit')
%     set(avfig.CurrentAxes, 'XScale', 'log')
   hold off
   
    SaveName='ShearRateDependenceYlimited';
    print(['D:\Promotion\Messungen\Ergebnisse\DendriticImpact\',SaveName],'-dpng') 
   
end