% Create surface plots of scanned splats

[~,~,~,~,arr_pos,arr_data]=ScanAnalysis('F:\Experiments\ImpactOnIce\Experiments\220513\V126_-8�C_0ms\');

%% Cut Data
var_radrng=1.9; % Multiple of drop radius that gets cut

% Erase outlier
ind_del=isoutlier(arr_data,'quartiles');
arr_pos=arr_pos(~ind_del,:);
arr_data=arr_data(~ind_del);


% Get measurements of drop (approximately
ind_drop=arr_data > 1.5*mean(arr_data);
% Calculate drop center and radius
[arr_ct(1),arr_ct(2),R]=circfit(arr_pos(ind_drop,1),arr_pos(ind_drop,2));
arr_ct=arr_ct+[0.5,0.5];
% Get data point closest to center coordinate
[~,ind_ctpt]=min(pdist2(arr_ct,arr_pos));
var_ctdata=arr_data(ind_ctpt);
%Set origin in drop center (approx)
arr_pos=arr_pos-arr_ct;
% Transorm into polar coordinates
[theta,arr_R]=cart2pol(arr_pos(:,1),arr_pos(:,2));
% Cut Data based on radius from circle and increase for surrouding points
ind_plot=arr_R < var_radrng*R;



%% Adjust height data so inclined splat
% var_angadjust=atan(0.0813/14.4);
% arr_data=arr_data+tan(var_angadjust)*(arr_pos(:,1)-min(arr_pos(:,1)));

%% Base Data on surface mean
arr_bslvl=mean(arr_data(~ind_plot),'omitnan');
arr_data=arr_data-arr_bslvl;

%%%%%%%%%%%%%%%%%%%%%%%%%% Plots %%%%%%%%%%%%%%%%%%%%%%%
% SaveLocation='D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\4-Results\figures\';
SaveLocation='D:\Promotion\Dissertation\Figure_Rawfiles\Impactonice\';

%% 3D Scatter plot of calibrated data
fig1=figure(1); 
hold on
% scatter3(arr_vlpos(:,1),arr_vlpos(:,2),arr_vldata)
% scatter3(arr_pos(:,1),arr_pos(:,2),Calibration.sf1(arr_pos))
scatter3(arr_pos(:,1),arr_pos(:,2),arr_data)
% scatter3(arr_pos(ind_drop,1),arr_pos(ind_drop,2),arr_data(ind_drop),'+')
scatter3(arr_pos(ind_plot,1),arr_pos(ind_plot,2),arr_data(ind_plot))
scatter3(arr_ct(1),arr_ct(2),var_ctdata,'*')
% scatter3(arr_pos(ind_del,1),arr_pos(ind_del,2),arr_data(ind_del),'+')

hold off
%% 3D surface plot
fig2=figure(2);
clf(fig2)

xlin_srf = linspace(min(arr_pos(ind_plot,1)),max(arr_pos(ind_plot,1)),75);
ylin_srf = linspace(min(arr_pos(ind_plot,2)),max(arr_pos(ind_plot,2)),75);



[X_srf,Y_srf] = meshgrid(xlin_srf,ylin_srf);
% [theta_srfgrd,R_srfgrd] = meshgrid(theta_srf,R_srf);
f = scatteredInterpolant(arr_pos(ind_plot,1),arr_pos(ind_plot,2),arr_data(ind_plot));
% [theta_srf,R_srf]=cart2pol(X_srf,Y_srf);
% ind_srfplt=R_srf < 1.9*R;

Z_srf= f(X_srf,Y_srf);


ind_pltrad=sqrt(X_srf.^2+Y_srf.^2)>var_radrng*R;
Z_srf(ind_pltrad)=NaN;
surf(X_srf,Y_srf,Z_srf)



% tri=delaunay(arr_pos(ind_plot,1),arr_pos(ind_plot,2));
hold on
% trisurf(tri,arr_pos(ind_plot,1),arr_pos(ind_plot,2),arr_data(ind_plot),'FaceAlpha',0.7);
% tripl=trisurf(tri,arr_vlpos(:,1),arr_vlpos(:,2),mean(arr_vldata)-arr_vldata,'FaceAlpha',0.7);
% hold off
% figure();
% tri_vl=delaunay(arr_vlpos(:,1),arr_vlpos(:,2));
% tripl_vl=trisurf(tri_vl,arr_vlpos(:,1),arr_vlpos(:,2),arr_measdata_angled2(ind_valid,:),'FaceAlpha',0.7);

fig2.CurrentAxes.View=[-108  34.6];

xlabel('$$ x / \mathrm{mm} $$','interpreter','latex');
ylabel('$$ y / \mathrm{mm} $$','interpreter','latex');
zlabel('$$ h_\mathrm{splat}/ \mathrm{mm}$$','interpreter','latex');

fig2=plotprops(fig2);


% SaveName='FrozenSplat_-8�C_0ms';
% matlab2tikz([SaveLocation,SaveName,'_tikz.tex'],'showInfo',false,'height','0.5\textwidth,','width','0.7\textwidth')