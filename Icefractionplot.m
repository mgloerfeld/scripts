% Plot theoretical approach to ice Fraction of 
rho_ice=900; % Density of ice 0�C (VDI 10th Edition)
cp_ice=2000; % Heat capacity of ice at 0�C (VDI 10th Edition)
% T0=(-30:0.5:0); % Considered temperatures
T0=-13.6;
delta_h=333.1e3; % Latent heat of water at melting in [J/kg]
Ice_frac=water_props(T0,'cp').*water_props(T0,'rho').*abs(T0)./(rho_ice*delta_h+abs(T0).*(water_props(0,'cp')*water_props(0,'rho')-rho_ice*cp_ice)); % Ice Fraction of drop after dendritic freezing

figIF=figure(10);

plot(abs(T0),Ice_frac*100,'k','LineWidth',2,'Displayname','DFD ice fraction')

xlim([min(abs(T0)) max(abs(T0))])
xlabel('$$ \Delta\,T/\textrm{K} $$','interpreter','latex')
ylabel('$$ \xi_\textrm{ice}/\% $$','interpreter','latex')
% legend('-DynamicLegend')

figIF=plotprops(figIF,'Tikz'); 

SaveLocation='D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\2-Theory\figures\';
SaveName='AnalyticalIceFraction';
matlab2tikz([SaveLocation,SaveName,'_tikz.tex'],'showInfo',false,'height','0.4\textwidth,','width','0.7\textwidth')


% print(['D:\Promotion\Messungen\Ergebnisse\DendriticImpact\',SaveName],'-dpng')
% matlab2tikz(['D:\Promotion\Messungen\Ergebnisse\DendriticImpact\',SaveName,'_tikz.tex'],'showInfo',false)
