%Check Calibration of DSE and Impact Analysis
Location='F:\Experiments\ImpactPositionVariation_V2\CalibrationExperiments\20210902\';


Datafiles = dir([Location '**\*_Data*']);
numdata=length(Datafiles);

arr_VolDSE=NaN(numdata,1);
arr_VolIA=NaN(numdata,1);
arr_dropdiam=NaN(numdata,1);
arr_diff=NaN(numdata,1);

for ii=1:numdata
    
    FullFileDirectory=[Datafiles(ii).folder,'\',Datafiles(ii).name];
    load(FullFileDirectory);
    
    arr_VolDSE(ii)=stc_VID.Info.ResidualVolume;
    arr_VolIA(ii)=stc_VID.Data.AvDropVolume;
    
    arr_dropdiam(ii)=stc_VID.Data.AvDropDiameter;
    
    arr_diff(ii)=arr_VolIA(ii)-arr_VolDSE(ii);
    
end