% Plot Homogeneous Nucleation
T=-10; % Temperature in�C ( also delta T to T_m)
T_m= 273.15; % Melting temperature in Kelvin
var_L=333.55e3*water_props(T,'rho'); % Latent heat of fusion in J/m^3
sigma=water_props(T,'sigma'); % Surface tension in N/m
var_Gv=(T)*var_L/T_m;

r=0:1e-10:2e-8;
arr_Gls=(4/3).*pi.*r.^3.*var_Gv+4.*pi.*r.^2.*sigma;
arr_Gls_V=(4/3).*pi.*r.^3.*var_Gv;
arr_Gls_A=4.*pi.*r.^2.*sigma;

fig1=figure(1);
hold on
plot([0 7e-9], [0 0],'--','Color',[0.6 0.6 0.6])
plot(r,arr_Gls,'k','LineWidth',2)
plot(r,arr_Gls_V,'-','Color',[0.6 0.6 0.6],'LineWidth',2)
plot(r,arr_Gls_A,'-','Color',[0.6 0.6 0.6],'LineWidth',2)
hold off

xlabel('$$ r_n  $$','interpreter','latex')
ylabel('$$ \Delta G $$','interpreter','latex')

% ylim([-1e-17 1e-17])
set(gca,'XTick',[],'YTick',[0])



fig1=plotprops(fig1,'KeepMarkerSize');
fig1.CurrentAxes.YAxis.TickLabelFormat = '%.0f';
fig1.CurrentAxes.YAxis.Exponent=0;
fig1.Units='pixels';

SaveLocation='D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\2-Theory\figures\';
SaveName='HomNucl';
% matlab2tikz([SaveLocation,SaveName,'_tikz.tex'],'showInfo',false,'height','0.8\textwidth,','width','0.6\textwidth')

