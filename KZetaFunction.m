%% Determine K-function
% Initial conditions

U_0=5;
ice_frac=0.12;


% Specify initial conditions:

%Initial conditions for a time slightly after the moment of contact:
y0 = [0 ;4/3; 2];

%Create an option set that specifies numerical tolerances for the numerical search.
%Increased both tolerances to 10^-9

opt = odeset('RelTol', 10.0^(-9),'AbsTol',10.0^(-9));

%% Solve the Equation system
%define the start and end value of "zeta" for the integration
tspan=[0.001 7];
%solve the equation system with solver for stiff problems ode15s
[t_K,y_K] = ode15s(@(t_K,y_K) odefcn2(t_K,y_K,U_0,ice_frac), tspan, y0,opt);

K=y_K(:,2);
K_modelinput=K;
K_modelinput(find(K<0,1):end)=0;
%% Calculate fit for K

K_coeff=polyfit(t_K,K,7);
K_coeff_Ilia=[-0.2107; 1.04636; -1.58856; 0; 1.3333]; % Coefficients for a= sqrt(2*zeta)

t_Kft=0:0.1:tspan(2);
K_ft=polyval(K_coeff,t_Kft);
K_ft_Ilia=polyval(K_coeff_Ilia,t_Kft);


fig=figure(1);
% plot(t_K,K_modelinput,'Displayname',' ModelInput $$ K=0 \zeta > \approx 2 $$')
hold on
plot(t_Kft,K_ft,'Displayname','Polynomial Fit $$  \mathcal{O}(\zeta^7) $$')
plot(t_Kft,K_ft_Ilia,'Displayname','Roisman (2022)')
plot(t_K,K,':','Displayname',' Numerical Solution')
yline(0,'--','HandleVisibility','off')
hold off

ylim([-2,2])
%% Figure settings

xlabel('$$ \zeta $$ ','interpreter','latex')
ylabel('$$ K(\zeta) $$','interpreter','latex')
legend('-DynamicLegend','interpreter','latex')

fig=plotprops(fig);