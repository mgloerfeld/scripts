% Calculate Boundary layer inside of drop without flow in z direction

% Temperatures
T_wall=-20; % Wall temperature at xi-> infinity [�C]
T_drop=20; % Wall temperature at xi->infinity [�C]
T_cont_init=(T_Wall+T_drop)/2; % Initial guess for contact temperature [�C]

% Loop conditions
T_cont=T_cont_init; % Start with Initial guess of contact temperature
lambda_wall=236; % Thermal conductivity of the wall [W/m K]
lambda_f=water_props(T_cont,'lambda'); % Thermal conductivity of fluid at the wall at T_cont [W/m K]
nu_f= water_props(T_cont,'nu'); % Dynamic viscosity of water at the wall at T_cont

f_prime_init=(lambda_wall*(T_cont-T_wall))/(sqrt(pi*nu_f)*lambda_f*(T_drop-T_cont));

coeff_thdiff=[9.372915024896053e-18,-2.111076647556699e-15,1.676182754602928e-13,-7.448253189326952e-12,5.957354716425945e-10,1.333665679523477e-07]; % Coefficients from polynomial fit for thermal diffusitivy data
a_T=coeff_thdiff(1)*T_in.^5+coeff_thdiff(2)*T_in.^4+coeff_thdiff(3)*T_in.^3+coeff_thdiff(4)*T_in.^2+coeff_thdiff(5)*T_in+coeff_thdiff(6); % Thermal diffusivity calcualted from empiric data


