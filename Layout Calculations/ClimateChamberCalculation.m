%% Calculation of Parameter for a climate Chamber to run a subzero wind channel

%% Material Parameters
g=9.81; % Gravitational constant [m/(s^2)]

% Temperatures
T.ambient=15+273.15; % Temperature of ambient air in [K]
T.chamber=-10+273.15; % Temperature inside of cooling chamber in [K]
T.floor=15+273.15; % Temperature in [K]

%Air Parameters inside chamber

% Regression functions for air parameters between 30�C and -30�C
syms cp(tmp); cp(tmp)= 3.7879e-08*tmp^4 - 2.7778e-06*tmp^3 + 3.4470e-04*tmp^2 + 1.7659e-02*tmp + 1.0059e+03; % heat capacity of air [J/(kgK)]
syms beta_T(tmp); beta_T(tmp)= 5.1393e-08*tmp^2 - 1.3724e-05*tmp + 3.6737e-03; % Beta of ambient air [1/K]
syms rho(tmp); rho(tmp)= 1.1364e-10*tmp^4 - 6.3889e-08*tmp^3 + 1.7451e-05*tmp^2 - 4.6892e-03*tmp + 1.2758; % Density of air [kg/m^3]
syms Pr(tmp); Pr(tmp)= 5.0000e-07*tmp^2 - 1.5500e-04*tmp + 7.1100e-01;  % Prandtl number of air
syms lambda(tmp); lambda(tmp)= 5.5556e-11*tmp^3 - 4.3810e-08*tmp^2 + 7.6533e-05*tmp + 2.4360e-02; % heat conduction coefficient of air [W/(mK)]
syms nu(tmp); nu(tmp)= -1.0221e-16*tmp^5 + 2.0294e-15*tmp^4 + 8.6013e-14*tmp^3 + 1.1215e-10*tmp^2 + 8.8808e-08*tmp + 1.3496e-05; % kinematic viscosity of air [m^2/s]
syms mu(tmp); mu(tmp)= -8.3333e-17*tmp^5 + 7.5758e-16*tmp^4 + 2.0833e-13*tmp^3 - 3.8106e-11*tmp^2 + 5.0030e-08*tmp + 1.7218e-05; % dynamic viscosity of air [Pa*s]


% Insulator parameters
insulator.lambda=0.02; % heat conduction coefficient of styrodur [W/(mK)]
insulator.thickness=0.005; % thickness of insulator material [m]

% Steel plate parameter
steel.lambda=46.5; % heat conduction coefficient of stell [W/(mK)]
steel.thickness=0.0; % Thickness of outer steel plate [m] 

%% Geometric parameters

% Outer Chamber geometries
chamber.w=0.30; % Width of Chamber in [m]
chamber.d=0.250; % Depth of Chamber in [m]
chamber.h=0.15; % Height of Chamber in [m]
chamber.surf=2*chamber.w*chamber.h+2*chamber.d*chamber.h+chamber.w*chamber.d; % Surface of chamber without floor
chamber.vol=(chamber.w-2*insulator.thickness)*(chamber.d-2*insulator.thickness)*(chamber.h-2*insulator.thickness); % Inner volume of chamber

%% Heat Restistances from head conduction in wall
% Heat Resistance Front Wall (equals heat flux over back wall)
R.steel=steel.thickness/(steel.lambda*chamber.w*chamber.h); % Heat Resistance of wooden plates
R.insulator=insulator.thickness/(insulator.lambda*chamber.w*chamber.h); % Heat resistance of Styrodur plate 
R_wall(1)=2*R.steel+R.insulator; % Heat resistance of wall

% Heat Resistance Left Wall (equals heat flux over right wall)
R.steel=steel.thickness/(steel.lambda*chamber.d*chamber.h); % Heat Resistance of wooden plates
R.insulator=insulator.thickness/(insulator.lambda*chamber.d*chamber.h); % Heat resistance of Styrodur plate 
R_wall(2)=2*R.steel+R.insulator; % Heat resistance of wall

% Heat Resistance Ceiling
R.steel=steel.thickness/(steel.lambda*chamber.d*chamber.w); % Heat Resistance of wooden plates
R.insulator=insulator.thickness/(insulator.lambda*chamber.d*chamber.w); % Heat resistance of Styrodur plate 
R_wall(3)=2*R.steel+R.insulator; % Heat resistance of wall


%% Heat Flux
% Heat flux over vertical wall
 T.wall=(T.ambient+T.chamber)/2; % Initial Value for temperature at the wall

for ii=1:50

    Q_wall=(T.wall-T.chamber)/(R_wall(1)); % Heat flux due to heat conduction in wall

    %Heat flux due to natural convection at outer wall (vertical plate)
    T.av=(T.wall+T.ambient)/2; % Average Temperature to determine fluid paramters for calculation
    flwL=chamber.h;
    Gr=(g*flwL^3*double(beta_T(T.av))*(T.ambient-T.wall))/(double(nu(T.av))^2);
    Ra=Gr*double(Pr(T.av));
    f1=(1+(0.492/double(Pr(T.av)))^(9/16))^-(16/9);
    Nu_m=(0.825+0.387*(Ra*f1)^(1/6))^2;
    alpha=Nu_m*double(lambda(T.av))/flwL;
    Q_conv_VW=chamber.w*chamber.h*alpha*(T.ambient-T.wall);

    if abs(Q_wall-Q_conv_VW)<1
        break
    elseif  abs(Q_wall-Q_conv_VW)<2
        T.wall=T.wall+sign(Q_conv_VW-Q_wall)*0.1;
    elseif  abs(Q_wall-Q_conv_VW)<10
        T.wall=T.wall+sign(Q_conv_VW-Q_wall)*0.5;
    else   
        T.wall=T.wall+sign(Q_conv_VW-Q_wall)*1;
    end
end
Q_FBW=Q_wall(1);
Q_LRW=Q_FBW*(chamber.d/chamber.w); % Heat flux over left and right wall from Area Ratio of Wall surfaces

% Heat flux over ceiling
 T.wall=(T.ambient+T.chamber)/2; % Initial Value for temperature at the wall

for ii=1:50

    Q_ceil=(T.wall-T.chamber)/(R_wall(3)); % Heat flux due to heat conduction in wall

    %Heat flux due to natural convection at outer wall (horizontal plate)
    T.av=(T.wall+T.ambient)/2; % Average Temperature to determine fluid paramters for calculation
    flwL=(chamber.w*chamber.d)/(2*chamber.w+2*chamber.d);
    Gr=(g*flwL^3*double(beta_T(T.av))*(T.ambient-T.wall))/(double(nu(T.av))^2);
    f2=(1+(0.322/double(Pr(T.av)))^(11/20))^-(20/11);
    Ra=Gr*double(Pr(T.av));
    if Ra*f2< 7e4
        Nu_m=0.766*(Ra*f2)^(1/5);
    else 
        Nu_m=0.15*(Ra*f2)^(1/3);
    end
    alpha=Nu_m*double(lambda(T.av))/flwL;
    Q_conv_HW=chamber.w*chamber.h*alpha*(T.ambient-T.wall);

    if abs(Q_conv_HW-Q_ceil)<1
        break
    elseif  abs(Q_conv_HW-Q_ceil)<2
        T.wall=T.wall+sign(Q_conv_HW-Q_ceil)*0.1;
    elseif  abs(Q_conv_HW-Q_ceil)<10
        T.wall=T.wall+sign(Q_conv_HW-Q_ceil)*0.5;
    else   
        T.wall=T.wall+sign(Q_conv_HW-Q_ceil)*1;
    end
end
Q_C=Q_ceil; 

%% Heat Flux over walls with heat conduction and wall temperatures from infinte region
Q_amb_hc(1)=2*(T.ambient-T.chamber)/(R_wall(1)); % Front and back wall
Q_amb_hc(2)=2*(T.ambient-T.chamber)/(R_wall(2)); % Left and right wall
Q_amb_hc(3)=(T.ambient-T.chamber)/(R_wall(3)); % Ceiling
Q_fl=(T.floor-T.chamber)/(R_wall(3)); % Heat Flux over floor

Q_tot_hc=sum(Q_amb_hc)+Q_fl;

%% Heat Flux  over walls with free convection in adjacent air
Q_amb_fc(1)=2*Q_FBW; % Front and back wall
Q_amb_fc(2)=2*Q_LRW; % Left and right wall
Q_amb_fc(3)=Q_C; % Ceiling

Q_tot_fc=sum(Q_amb_fc)+Q_fl;



