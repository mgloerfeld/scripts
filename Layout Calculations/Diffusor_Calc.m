% Calculation of geometrical dimennsions of Wind Channel Diffusor

% Entrance
d_en=0.14;
A_en=d_en^2; % [m]
U_en=4*d_en; %[m]
Dhyd_en=4*A_en/U_en; % Hydraulic Diameter
air_vel_en=40; % Air Velocity at entrance in [m/s]

% Exit
d_ex=0.2;
A_ex=d_ex^2; % [m]
U_ex=4*d_ex; % [m]
Dhyd_ex=4*A_ex/U_ex; % Hydraulic Diameter
air_vel_ex=air_vel_en*(A_en/A_ex); % Air Velocity at exit in [m/s]

% Length
L_dif=0.4;  %[m]
% Expansion Angle
Theta_exp=rad2deg(atan(0.5*(Dhyd_ex-Dhyd_en)/L_dif));
% Area Ratio
A_ex/A_en;
