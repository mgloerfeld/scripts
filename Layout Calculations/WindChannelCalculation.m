% Calculation of Wind Channel Parameters based on the book Low Speed Wind
% Tunnel Testing- Chapter 3.3
%% Default Values
% Fluid Properties at -10°C
fluid.rho=air_props(-20,'rho'); % Density of air [kg/m^3]
fluid.nu=air_props(-20,'nu'); % kinematic viscosity of air [m^2/s]
fluid.mu=air_props(-20,'eta'); % dynamic viscosity of air [Pa*s]

%Required Properties
fluid.u_max=0:40; % Maximum Fluid Velocity in Test Section [m/s]


%% Geometric Values 
%------------- Test Section-----------------
TS.w=0.14; % Channel Width in square Cross section [m]
TS.A=TS.w^2; % Cross section of TS [m^2]
TS.C=4*TS.w; % Circumference of TS [m]
TS.dhyd=TS.w; %Hydraulic Diameter of TS [m]
TS.L=0.28; % Length of TS [m]
fluid.vol_flow=fluid.u_max*TS.A; % Volume flow through canal [m^3/2]

% %------------Diffusor 1-----------------
% D1.wen=0.17; % Entrance width of square cross section [m]
% D1.Aen=D1.wen^2; % Entrance Cross Section [m^2]
% D1.dhyd_en=D1.wen; % Hydraulic Diameter of entrance CR [m]
% D1.wex=0.23; % Exit with of square cross section [m]
% D1.Aex=D1.wex^2; % Exit Cross Section [m^2]
% D1.dhyd_ex=D1.wex; % Hydraulic Diameter of exit CR [m]
% D1.AR=D1.Aex/D1.Aen; % Area Ration of Diffusor []
% D1.L=0.3; % Length of diffusor [m]
% D1.Theta=atan((D1.dhyd_ex-D1.dhyd_en)/(2*D1.L)); % Diffusor angle [°]

%------------Exit Diffusor -----------------
DEx.wen=0.17; % Entrance width of square cross section [m]
DEx.Aen=DEx.wen^2; % Entrance Cross Section [m^2]
DEx.dhyd_en=DEx.wen; % Hydraulic Diameter of entrance CR [m]
DEx.wex=0.285; % Exit with of square cross section [m]
DEx.Aex=DEx.wex^2; % Exit Cross Section [m^2]
DEx.dhyd_ex=DEx.wex; % Hydraulic Diameter of exit CR [m]
DEx.AR=DEx.Aex/DEx.Aen; % Area Ration of Diffusor []
DEx.L=0.3; % Length of diffusor [m]
DEx.Theta=atan((DEx.dhyd_ex-DEx.dhyd_en)/(2*DEx.L)); % Diffusor angle [°]

%------------Long Diffusor-----------------
DL.wen=0.17; % Entrance width of square cross section [m]
DL.Aen=DL.wen^2; % Entrance Cross Section [m^2]
DL.dhyd_en=DL.wen; % Hydraulic Diameter of entrance CR [m]
DL.wex=0.235; % Exit with of square cross section [m]
DL.Aex=DL.wex^2; % Exit Cross Section [m^2]
DL.dhyd_ex=DL.wex; % Hydraulic Diameter of exit CR [m]
DL.AR=DL.Aex/DL.Aen; % Area Ration of Diffusor []
DL.L=0.5; % Length of diffusor [m]
DL.Theta=atan((DL.dhyd_ex-DL.dhyd_en)/(2*DL.L)); % Diffusor angle [°]

%------------Test Sektion Diffusor-----------------
DTS.wen=0.14; % Entrance width of square cross section [m]
DTS.Aen=DTS.wen^2; % Entrance Cross Section [m^2]
DTS.dhyd_en=DTS.wen; % Hydraulic Diameter of entrance CR [m]
DTS.wex=0.17; % Exit with of square cross section [m]
DTS.Aex=DTS.wex^2; % Exit Cross Section [m^2]
DTS.dhyd_ex=DTS.wex; % Hydraulic Diameter of exit CR [m]
DTS.AR=DTS.Aex/DTS.Aen; % Area Ration of Diffusor []
DTS.L=0.3; % Length of diffusor [m]
DTS.Theta=atan((DTS.dhyd_ex-DTS.dhyd_en)/(2*DTS.L)); % Diffusor angle [°]

% %----------Wide Angle Diffusor--------------
% WAD.wen=0.23; % Entrance width of square cross section [m]
% WAD.Aen=WAD.wen^2; % Entrance Cross Section [m^2]
% WAD.dhyd_en=WAD.wen; % Hydraulic Diameter of entrance CR [m]
% WAD.wex=0.320; % Exit with of square cross section [m]
% WAD.Aex=WAD.wex^2; % Entrance Cross Section [m^2]
% WAD.dhyd_ex=WAD.wex; % Hydraulic Diameter of exit CR [m]
% WAD.AR=WAD.Aex/WAD.Aen; % Area Ration of Diffusor []
% WAD.L=0.25; % Length of diffusor [m]
% WAD.Theta=atan((WAD.dhyd_ex-WAD.dhyd_en)/(2*WAD.L)); % Diffusor angle [°]
% WAD.K_req=(WAD.AR-1)/(1.14); % Mininum required Total Screen Loss to obtain isentropic turbulence

% %-------------Corner 1----------------------
% C1.w=0.23; % Width of square cross section [m]
% C1.Aen=C1.w^2; % Cross Section [m^2]
% C1.dhyd_en=C1.w; % Hydraulic Diameter of entrance CR [m]
% C1.ChL=99.54e-3; % Chord length of turning vane

%-------------Corner 2----------------------
C2.w=DTS.wex; % Width of square cross section [m]
C2.Aen=C2.w^2; % Cross Section [m^2]
C2.dhyd_en=C2.w; % Hydraulic Diameter of entrance CR [m]
C2.ChL=99.54e-3; % Chord length of turning vane

% %-------------Corner 3----------------------
% C3.w=0.14; % Width of square cross section [m]
% C3.Aen=C3.w^2; % Cross Section [m^2]
% C3.dhyd_en=C3.w; % Hydraulic Diameter of entrance CR [m]
% C3.ChL=99.54e-3; % Chord length of turning vane

% %---------Constant Duct----------------------
% CD.w=0.2; % Width of square cross section [m]
% CD.A=CD.w^2; % Cross Section [m^2]
% CD.dhyd=CD.w; % Hydraulic Diameter of CD [m]
% CD.L=0.244; % Length of Constant Duct

%-------------Turbulence Control Screen------------------
S_TC.w=0.32; % Width of Screen
S_TC.A=S_TC.w^2; % Overall Area of Screen
S_TC.dhyd=S_TC.w; % Hydraulic Diameter of Screen
S_TC.wd=0.0003; % Wire diameter
S_TC.Nm=200; % Number of mesh cell 
S_TC.mw=(S_TC.w-S_TC.Nm*S_TC.wd)/(S_TC.Nm+1); % Mesh Cell Width
S_TC.por=(1-S_TC.wd/S_TC.mw)^2; % Porosity of Mesh
S_TC.sol=1-S_TC.por; % Solidity of Mesh

% %-------------Screen Behind Test Section------------------
% S_PS.w=0.14; % Width of Screen
% S_PS.A=S_PS.w^2; % Overall Area of Screen
% S_PS.dhyd=S_PS.w; % Hydraulic Diameter of Screen
% S_PS.wd=0.0008; % Wire diameter
% S_PS.Nm=12; % Number of mesh cell 
% S_PS.mw=(S_PS.w-S_PS.Nm*S_PS.wd)/(S_PS.Nm+1); % Mesh Cell Width
% S_PS.por=(1-S_PS.wd/S_PS.mw)^2; % Porosity of Mesh
% S_PS.sol=1-S_PS.por; % Solidity of Mesh

% -------------Hexagonal Honeycombs---------------------------------
% HC.w=0.32; % Width of Honeycomb section
% HC.A=HC.w^2; % Area of HC Section
% HC.c_th=0.0007; % Thickness of cell wall
% HC.c_N1=75; % Number of cells in center line direction
% HC.c_w=(HC.w-(HC.c_N1-1)*HC.c_th)/(HC.c_N1); % Inner diameter of honeycomb cell
% HC.c_N2=(HC.w-((1/sqrt(3))*(2*HC.c_th+HC.c_w)))/(sqrt(3)*HC.c_w+(2/sqrt(3))*HC.c_th); % Number of cells orthogonal to direction 1
% HC.c_Ntot=HC.c_N1*HC.c_N2; % Total number of HC cells
% HC.A_mesh=HC.c_Ntot*(2*sqrt(3)*(2*HC.c_w*(HC.c_th/2)+(HC.c_th/2)^2)); % Area blocked by HC
% HC.c_dhyd=HC.c_w; % Hydraulic diameter of cell
% HC.c_dhyd=sqrt(3)*HC.c_w/2; % Hydraulic diameter of cell
% HC.dpt=6*HC.c_dhyd; % Streamwise depth of Honeycombs
% HC.por=((HC.A-HC.A_mesh)/HC.A); % Porosity of HC
% HC.rough=5e-6; % Material Roughness for Steel with Mill Finish


% %------------- Round Cell Honeycombs---------------------------------
HC.w=0.32; % Width of Honeycomb section
HC.A=HC.w^2; % Area of HC Section
HC.c_th=0.0007; % Thickness of cell wall
HC.c_w=0.004; % Inner diameter of honeycomb cell
HC.c_N1=HC.w/(HC.c_w+2*HC.c_th); % Number of cells in center line direction
HC.c_N2=2*HC.w/(HC.c_w*(1+(sqrt(2)/2))+4*HC.c_th); % Number of cells orthogonal to direction 1
HC.c_Ntot=HC.c_N1*HC.c_N2; % Total number of HC cells
HC.A_mesh=(pi*(HC.c_w*HC.c_th+HC.c_th^2))*HC.c_Ntot; % Area blocked by HC
HC.c_dhyd=HC.c_w; % Hydraulic diameter of cell
HC.dpt=6*HC.c_dhyd; % Streamwise depth of Honeycombs
HC.por=((HC.A-HC.A_mesh)/HC.A); % Porosity of HC
HC.rough=5e-6; % Material Roughness for Steel with Mill Finish

%% 
%---------Settling Chamber Constant Duct-----------------  
SC.w=0.32; % Width of Settling Chamber
SC.A=SC.w^2; % Cross Section Area of Settling Chamber
SC.dhyd=SC.w; % Hydraulic Diameter of SC [m]
SC.L=0.39; % Length of Settling Chamber Constant Duct

%------------------Nozzle------------------------
N.wen=0.32; % Entrance width of Nozzle
N.dhyden=N.wen; % Hydraulic diamter of squared cross section at entrance
N.Aen=N.wen^2; % Cross sectional area at entrance
N.wex=0.14; % Exit width of Nozzle
N.dhydex=N.wex; % Hydraulic diamter of squared cross section at exit
N.Aex=N.wex^2; % Cross sectional area at entrance
N.AR=N.Aen/N.Aex; % Area ration of cross sections of entrance and exit
N.L=0.32; % Nozzle length
% Nozzle outline geometry
k_N=[6*(N.wex/2-N.wen/2)+0.5,-15*(N.wex/2-N.wen/2)-1,10*(N.wex/2-N.wen/2)+0.5,0,0,N.wen/2]; % Coefficients for Nozzle Geometry
X_N=(0:0.001:1)*N.L;
N.lD=2*(k_N(1).*(X_N/N.L).^5+k_N(2).*(X_N/N.L).^4+k_N(3).*(X_N/N.L).^3+k_N(4).*(X_N/N.L).^2+k_N(5).*(X_N/N.L)+k_N(6)); % Local Width of the nozzle (equals local nozzle hydraulic diameter)

%--------------------Exit geometry--------------------
Ex.w=0.285; % Fan exit outlet width (square outlet)
Ex.A=Ex.w^2; % Fan exit outlet Area
Ex.dhyd=Ex.w; % Fan exit hydraulic diameter

%--------------------Inlet geometry--------------------
In.w=0.32; % Inlet width (square outlet)
In.A=In.w^2; % Fan exit outlet Area
In.dhyd=In.w; % Fan exit hydraulic diameter

%%%%%%%%Calculation of Energy Losses in Wind Channel Components %%%%%%%%%%
%% First Power Consideration 
P_jet=0.5*fluid.rho*TS.A.*fluid.u_max.^3; % Required Jet Power inside TS [W]
%% Test Section Loss
u_fluid.TS=fluid.u_max;
Re_TS= fluid.u_max.*TS.dhyd./fluid.nu; % TS Reynoldsnumber with max vel
pdyn.TS=0.5*fluid.rho.*fluid.u_max.^2; % TS dynamic pressure

%Friction loss coefficient
f.TS=1;
for ii=1:6
   f.TS= (2*log10(Re_TS.*sqrt(f.TS))-0.8).^-2;
end

% Test Section loss
Kl.TS=f.TS.*TS.L./TS.dhyd; % Local loss coefficient of Test Section
Klt.TS=Kl.TS; % Normalized loss coefficient of Test Section (Same as Local)

%% Inlet Loss Coefficients
u_fluid.In=fluid.u_max*(TS.A/In.A); % Fluid velocity at Diffusor entrance
Re_In=u_fluid.In*In.dhyd./fluid.nu; % Reynoldsnumber at exit
pdyn.In=0.5*fluid.rho*u_fluid.In.^2; % dynamic pressure at Exit

Kl.In=fluid.rho*u_fluid.In.^2*0.5./(pdyn.In); % Loss Coefficient from Carnot Shock at Exit
Klt.In=Kl.In.*(pdyn.In./pdyn.TS); % Normalized loss coefficient of Carnot Shock

%% Settling Chamber Constant Area Loss
u_fluid.SC=fluid.u_max*(TS.A/SC.A);
Re_SC= u_fluid.SC.*SC.dhyd./fluid.nu; % SC Reynoldsnumber with max vel
pdyn.SC=0.5*fluid.rho.*u_fluid.SC.^2; % SC dynamic pressure

%Friction loss coefficient
f.SC=1;
for ii=1:6
   f.SC= (2*log10(Re_SC.*sqrt(f.SC))-0.8).^-2;
end

% Constant Duct Loss
Kl.SC=f.SC*SC.L./SC.dhyd; % Local loss coefficient of CD
Klt.SC=Kl.SC.*(pdyn.SC./pdyn.TS); % Normalized loss coefficient of CD
%% Honeycomb loss
u_fluid.HC=fluid.u_max.*(TS.A./HC.A);
pdyn.HC=0.5*fluid.rho.*u_fluid.HC.^2; % HC dynamic pressure
Re_roughHC=u_fluid.HC.*HC.rough./fluid.nu; % Reynolds number based on roughness

lambda_HC=0.375*(Re_roughHC.^-0.1).*(HC.rough./HC.c_dhyd).^0.4; % Roughness parameter

%Honeycomb loss coefficient
Kl.HC=lambda_HC.*((HC.dpt./HC.c_dhyd)+3).*(1./HC.por).^2+((1./HC.por)-1).^2; % HC loss coefficient after Eckert
Klt.HC=Kl.HC.*(pdyn.HC./pdyn.TS); % Normalized loss coefficient
disp(Klt.HC(end))
%% Turbulence Control Screen loss
u_fluid.S_TC=fluid.u_max*(TS.A./S_TC.A);
Re_S_TC= u_fluid.S_TC.*S_TC.wd./fluid.nu; % Mesh Reynoldsnumber with max vel
pdyn.S_TC=0.5*fluid.rho.*u_fluid.S_TC.^2; % TS dynamic pressure

K_Rn= 0.785*((Re_S_TC./241)+1).^-4+1.01; % Reynolds loss coefficient
K_mesh=1.3; % Mesh loss factor after Idel'Chik

% Screen loss coefficient
Kl.S_TC=K_mesh.*K_Rn.*S_TC.sol+(S_TC.sol.^2./S_TC.por.^2); % Screen loss coefficient
Klt.S_TC=Kl.S_TC.*(pdyn.S_TC./pdyn.TS); % Normalized loss coefficient
%% Nozzle Loss
% Entrance properties
u_fluid.Nen=fluid.u_max*(TS.A/N.Aen); % Entrance velocity
Re_Nen= u_fluid.Nen*N.dhyden./fluid.nu; % Entrance Reynoldsnumber with nozzle entrance vel
pdyn.Nen=0.5*fluid.rho*u_fluid.Nen.^2; % Entrance dynamic pressure nozzle
%Friction loss coefficient
f.Nen=1;
for ii=1:6
   f.Nen= (2*log10(Re_Nen.*sqrt(f.Nen))-0.8).^-2;
end

% Exit Dynamics 
u_fluid.Nex=fluid.u_max*(TS.A/N.Aex); % Exit velocity
Re_Nex= u_fluid.Nex*N.dhydex./fluid.nu; % Exit Reynoldsnumber with nozzle entrance vel
pdyn.Nex=0.5*fluid.rho*u_fluid.Nex.^2; % Exit dynamic pressure nozzle
%Friction loss coefficient
f.Nex=1;
for ii=1:6
   f.Nex= (2*log10(Re_Nex.*sqrt(f.Nex))-0.8).^-2;
end
% Nozzle loss
f.Nav=(f.Nen+f.Nex)./2; % Average friction loss coefficient
fun= @(X) N.dhyden.^5./(2*(k_N(1).*(X).^5+k_N(2).*(X).^4+k_N(3).*(X).^3+k_N(4).*(X).^2+k_N(5).*(X)+k_N(6))).^5;
Kl.N=f.Nav*(N.L/N.dhyden)*integral(fun,0,1);
Klt.N=Kl.N.*(pdyn.Nen./pdyn.TS);


%% Test Section Diffusor loss
u_fluid.enDTS=fluid.u_max*(TS.A/DTS.Aen); % Fluid velocity at Diffusor entrance
Re_enDTS=u_fluid.enDTS*DTS.dhyd_en/fluid.nu; % D1 Reynoldsnumber with max vel 
pdyn.DTS=0.5*fluid.rho*u_fluid.enDTS.^2; % D1 dynamic pressure at entrance

%-------------- Fricion loss of Diffusor----------------

%Friction loss coefficient f.D1
f.DTS=1;
for ii=1:6
   f.DTS= (2*log10(Re_enDTS.*sqrt(f.DTS))-0.8).^-2;
end

K_DTSf=(1-(1/DTS.AR^2))*(f.DTS/(8*sin(DTS.Theta))); % Diffusor Friction loss

%-------------Expansion loss----------------------------
K_e=1.22156e-1-2.29480e-2*2*DTS.Theta+5.50704e-3*(2*DTS.Theta)^2-4.08644e-4*(2*DTS.Theta)^3-3.84056e-5*(2*DTS.Theta)^4+8.74969e-6*(2*DTS.Theta)^5-3.65217e-7*(2*DTS.Theta)^6; % Numerical expression for Expanding loss Coefficient after Eckert et al (1976)
K_DTSe=K_e*(DTS.AR-1/DTS.AR)^2; % Expansion loss coefficient of Diffusor

% Diffusor Loss Coefficient
Kl.DTS=K_DTSf+K_DTSe; % Local loss coefficient of Diffusor
Klt.DTS=Kl.DTS*(pdyn.DTS/pdyn.TS); % Normalized loss coefficient of Diffusor

%% Corner 2 loss
u_fluid.C2=fluid.u_max.*(TS.A./C2.Aen); % Fluid Velocity in Corner2
Re_chord_C2=u_fluid.C2.*C2.ChL./fluid.nu; % Chord Reynoldsnumber in Corner 2
pdyn.C2=0.5*fluid.rho.*u_fluid.C2.^2; % Dynamic pressure at entrance

% Corner loss coefficient
Kl.C2=0.1+(4.55./(log10(Re_chord_C2).^2.58));
Klt.C2=Kl.C2.*(pdyn.C2./pdyn.TS); % Normalized loss coefficient of Corner


%% Second/Long Diffusor Loss
u_fluid.enDL=fluid.u_max*(TS.A./DL.Aen); % Fluid velocity at Diffusor entrance
Re_enDL=u_fluid.enDL*DL.dhyd_en./fluid.nu; % D1 Reynoldsnumber with max vel 
pdyn.DL=0.5*fluid.rho*u_fluid.enDL.^2; % D1 dynamic pressure at entrance

%-------------- Fricion loss of Diffusor----------------

%Friction loss coefficient f.D1
f.DL=1;
for ii=1:6
   f.DL= (2*log10(Re_enDL.*sqrt(f.DL))-0.8).^-2;
end

K_DLf=(1-(1./DL.AR.^2))*(f.DL./(8*sin(DL.Theta))); % Diffusor Friction loss

%-------------Expansion loss----------------------------
K_e=1.22156e-1-2.29480e-2*2*DL.Theta+5.50704e-3*(2*DL.Theta).^2-4.08644e-4*(2*DL.Theta).^3-3.84056e-5*(2*DL.Theta).^4+8.74969e-6*(2*DL.Theta).^5-3.65217e-7*(2*DL.Theta).^6; % Numerical expression for Expanding loss Coefficient after Eckert et al (1976)
K_DLe=K_e*(DL.AR-1./DL.AR).^2; % Expansion loss coefficient of Diffusor

% Diffusor Loss Coefficient
Kl.DL=K_DLf+K_DLe; % Local loss coefficient of Diffusor
Klt.DL=Kl.DL.*(pdyn.DL./pdyn.TS); % Normalized loss coefficient of Diffusor

%% Exit Diffusor Loss
u_fluid.enDEx=fluid.u_max*(TS.A./DEx.Aen); % Fluid velocity at Diffusor entrance
Re_enDEx=u_fluid.enDEx*DEx.dhyd_en./fluid.nu; % D1 Reynoldsnumber with max vel 
pdyn.DEx=0.5*fluid.rho*u_fluid.enDEx.^2; % D1 dynamic pressure at entrance

%-------------- Fricion loss of Diffusor----------------

%Friction loss coefficient f.D1
f.DEx=1;
for ii=1:6
   f.DEx= (2*log10(Re_enDEx.*sqrt(f.DEx))-0.8).^-2;
end

K_DExf=(1-(1./DEx.AR.^2))*(f.DEx./(8*sin(DEx.Theta))); % Diffusor Friction loss

%-------------Expansion loss----------------------------
K_e=1.22156e-1-2.29480e-2*2*DEx.Theta+5.50704e-3*(2*DEx.Theta).^2-4.08644e-4*(2*DEx.Theta).^3-3.84056e-5*(2*DEx.Theta).^4+8.74969e-6*(2*DEx.Theta).^5-3.65217e-7*(2*DEx.Theta).^6; % Numerical expression for Expanding loss Coefficient after Eckert et al (1976)
K_DExe=K_e*(DEx.AR-1./DEx.AR).^2; % Expansion loss coefficient of Diffusor

% Diffusor Loss Coefficient
Kl.DEx=K_DExf+K_DExe; % Local loss coefficient of Diffusor
Klt.DEx=Kl.DEx.*(pdyn.DEx./pdyn.TS); % Normalized loss coefficient of Diffusor

%% Outlet Loss Coefficients
u_fluid.Ex=fluid.u_max*(TS.A/Ex.A); % Fluid velocity at Diffusor entrance
Re_Ex=u_fluid.Ex*Ex.dhyd./fluid.nu; % Reynoldsnumber at exit
pdyn.Ex=0.5*fluid.rho*u_fluid.Ex.^2; % dynamic pressure at Exit

Kl.Ex=fluid.rho*u_fluid.Ex.^2*0.5./(pdyn.Ex); % Loss Coefficient from Carnot Shock at Exit
Klt.Ex=Kl.Ex.*(pdyn.Ex./pdyn.TS); % Normalized loss coefficient of Carnot Shock

%% Diffusor 1 loss
% u_fluid.enD1=fluid.u_max*(TS.A/D1.Aen); % Fluid velocity at Diffusor entrance
% Re_enD1=u_fluid.enD1*D1.dhyd_en/fluid.nu; % D1 Reynoldsnumber with max vel 
% pdyn.D1=0.5*fluid.rho*u_fluid.enD1^2; % D1 dynamic pressure at entrance
% 
% %-------------- Fricion loss of Diffusor----------------
% 
% %Friction loss coefficient f.D1
% f.D1=1;
% for ii=1:6
%    f.D1= (2*log10(Re_enD1*sqrt(f.D1))-0.8)^-2;
% end
% 
% K_D1f=(1-(1/D1.AR^2))*(f.D1/(8*sin(D1.Theta))); % Diffusor Friction loss
% 
% %-------------Expansion loss----------------------------
% K_e=1.22156e-1-2.29480e-2*2*D1.Theta+5.50704e-3*(2*D1.Theta)^2-4.08644e-4*(2*D1.Theta)^3-3.84056e-5*(2*D1.Theta)^4+8.74969e-6*(2*D1.Theta)^5-3.65217e-7*(2*D1.Theta)^6; % Numerical expression for Expanding loss Coefficient after Eckert et al (1976)
% K_D1e=K_e*(D1.AR-1/D1.AR)^2; % Expansion loss coefficient of Diffusor
% 
% % Diffusor Loss Coefficient
% Kl.D1=K_D1f+K_D1e; % Local loss coefficient of Diffusor
% Klt.D1=Kl.D1*(pdyn.D1/pdyn.TS); % Normalized loss coefficient of Diffusor

%% Constant Duct loss
% u_fluid.CD=fluid.u_max*(TS.A/CD.A);
% Re_CD= u_fluid.CD*CD.dhyd/fluid.nu; % TS Reynoldsnumber with max vel
% pdyn.CD=0.5*fluid.rho*u_fluid.CD^2; % TS dynamic pressure
% 
% %Friction loss coefficient
% f.CD=1;
% for ii=1:6
%    f.CD= (2*log10(Re_CD*sqrt(f.CD))-0.8)^-2;
% end
% 
% % Constant Duct Loss
% Kl.CD=f.CD*CD.L/CD.dhyd; % Local loss coefficient of CD
% Klt.CD=Kl.CD*(pdyn.CD/pdyn.TS); % Normalized loss coefficient of CD

%% Corner 1 loss
% u_fluid.C1=fluid.u_max*(TS.A/C1.Aen); % Fluid Velocity in Corner1
% Re_chord_C1=u_fluid.C1*C1.ChL/fluid.nu;% Chord Reynoldsnumber 
% pdyn.C1=0.5*fluid.rho*u_fluid.C1^2; % Dynamic pressure at entrance
% 
% % Corner loss coefficient
% Kl.C1=0.1+(4.55/(log10(Re_chord_C1)^2.58));
% Klt.C1=Kl.C1*(pdyn.C1/pdyn.TS); % Normalized loss coefficient of Corner

%% Wide Angle Diffusor Loss
% u_fluid.enWAD=fluid.u_max*(TS.A/WAD.Aen); % Fluid velocity at Diffusor entrance
% Re_enWAD=u_fluid.enWAD*WAD.dhyd_en/fluid.nu; % D1 Reynoldsnumber with max vel 
% pdyn.WAD=0.5*fluid.rho*u_fluid.enWAD^2; % D1 dynamic pressure at entrance
% 
% %-------------- Fricion loss of Diffusor----------------
% 
% %Friction loss coefficient f.D1
% f.WAD=1;
% for ii=1:6
%    f.WAD= (2*log10(Re_enWAD*sqrt(f.WAD))-0.8)^-2;
% end
% 
% K_WADf=(1-(1/WAD.AR^2))*(f.WAD/(8*sin(WAD.Theta))); % Diffusor Friction loss
% 
% %-------------Expansion loss----------------------------
% K_e=1.22156e-1-2.29480e-2*2*WAD.Theta+5.50704e-3*(2*WAD.Theta)^2-4.08644e-4*(2*WAD.Theta)^3-3.84056e-5*(2*WAD.Theta)^4+8.74969e-6*(2*WAD.Theta)^5-3.65217e-7*(2*WAD.Theta)^6; % Numerical expression for Expanding loss Coefficient after Eckert et al (1976)
% K_WADe=K_e*(WAD.AR-1/WAD.AR)^2; % Expansion loss coefficient of Diffusor
% 
% % Diffusor Loss Coefficient
% Kl.WAD=K_WADf+K_WADe; % Local loss coefficient of Diffusor
% Klt.WAD=Kl.WAD*(pdyn.WAD/pdyn.TS); % Normalized loss coefficient of Diffusor
%% Protection Screen loss
% u_fluid.S_PS=fluid.u_max*(TS.A/S_PS.A);
% Re_S_PS= u_fluid.S_PS*S_PS.wd/fluid.nu; % Mesh Reynoldsnumber with max vel
% pdyn.S_PS=0.5*fluid.rho*u_fluid.S_PS^2; % TS dynamic pressure
% 
% K_Rn= 0.785*((Re_S_PS/241)+1)^-4+1.01; % Reynolds loss coefficient
% K_mesh=1.3; % Mesh loss factor after Idel'Chik
% 
% % Screen loss coefficient
% Kl.S_PS=K_mesh*K_Rn*S_PS.sol+(S_PS.sol^2/S_PS.por^2); % Screen loss coefficient
% Klt.S_PS=Kl.S_PS*(pdyn.S_PS/pdyn.TS); % Normalized loss coefficient
%% Corner 3 Loss
% u_fluid.C3=fluid.u_max*(TS.A/C3.Aen); % Fluid Velocity in Corner2
% Re_chord_C3=u_fluid.C3*C3.ChL/fluid.nu; % Chord Reynoldsnumber in Corner 2
% pdyn.C3=0.5*fluid.rho*u_fluid.C3^2; % Dynamic pressure at entrance
% 
% % Corner loss coefficient
% Kl.C3=0.1+(4.55/(log10(Re_chord_C3)^2.58));
% Klt.C3=Kl.C3*(pdyn.C3/pdyn.TS); % Normalized loss coefficient of Corner

%% %%%%%%%%%%%%%%%%%%%%%% Energy Ratio and Total Pressure Loss %%%%%%%%%%%%%%%%%%%%%

E_R=1./(Klt.TS+Klt.C2+Klt.HC+Klt.S_TC+Klt.SC+Klt.N+Klt.DTS+Klt.DL+Klt.DEx+Klt.Ex+Klt.In);
E_R_woExIn=1./(Klt.TS+Klt.C2+Klt.HC+Klt.S_TC+Klt.SC+Klt.N+Klt.DTS+Klt.DL+Klt.DEx+Klt.In);


pressure_loss_HC=Kl.HC.*pdyn.HC;
pressure_loss=Kl.TS.*pdyn.TS+Kl.C2.*pdyn.C2+Kl.HC.*pdyn.HC+Kl.S_TC.*pdyn.S_TC+Kl.SC.*pdyn.SC+Kl.N.*(pdyn.Nen+pdyn.Nex)/2+Kl.DEx.*pdyn.DEx+Kl.DTS.*pdyn.DTS+Kl.DL.*pdyn.DL+Kl.Ex.*pdyn.Ex+Kl.In.*pdyn.In;
P_loss=P_jet./E_R_woExIn;


P_loss_TS=P_jet.*Klt.TS;
P_loss_C2=P_jet.*Klt.C2;
P_loss_HC=P_jet.*Klt.HC;
P_loss_S_TC=P_jet.*Klt.S_TC;
P_loss_SC=P_jet.*Klt.SC;
P_loss_N=P_jet.*Klt.N;
P_loss_DTS=P_jet.*Klt.DTS;
P_loss_DL=P_jet.*Klt.DL;
P_loss_DEx=P_jet.*Klt.DEx;
P_loss_In=P_jet.*Klt.In;
% Losses are included in P_jet
P_loss_Ex=0;
% P_loss_Ex=P_jet.*Klt.Ex;



Press_loss_TS=pdyn.TS.*Kl.TS;
Press_loss_C2=pdyn.C2.*Kl.C2;
Press_loss_HC=pdyn.HC.*Kl.HC;
Press_loss_S_TC=pdyn.S_TC.*Kl.S_TC;
Press_loss_SC=pdyn.SC.*Kl.SC;
Press_loss_N=(pdyn.Nen+pdyn.Nex)/2.*Kl.N;
Press_loss_DTS=pdyn.DTS.*Kl.DTS;
Press_loss_DL=pdyn.DL.*Kl.DL;
Press_loss_DEx=pdyn.DEx.*Kl.DEx;
Press_loss_Ex=pdyn.Ex.*Kl.Ex;
Press_loss_In=pdyn.In.*Kl.In;

Press_loss_ges=Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N+Press_loss_DTS+Press_loss_DL+Press_loss_DEx+Press_loss_Ex+Press_loss_In;
%% Plot System characteristic

figure(1);
hold on
% yyaxis left
% plotyy(fluid.vol_flow*3600,pressure_loss,fluid.vol_flow*3600,u_fluid.TS)
plot(fluid.vol_flow*3600,pressure_loss)
xlabel('Volume Flow [m^3/h]')
ylabel('Presssure Loss [Pa]')

% yyaxis right
% ylabel('TS velocity [m/s]')
grid on
hold off

%% Plot Energy Dissipation 
% figure(2); 
% hold on
% plot(fluid.vol_flow*3600,P_loss)
% xlabel('Volume Flow [m^3/h]')
% ylabel('Energy Dissipation [W]')
% grid on
% hold off

%% Plot Total Input Energy
% figure(3); 
% hold on
% area(fluid.vol_flow*3600,P_loss+P_jet)
% area(fluid.vol_flow*3600,P_loss)
% xlabel('Volume Flow [m^3/h]')
% ylabel('Total Input Energy  [W]')
% set(gcf, 'Position', get(0, 'Screensize')); % Set Figure to FullScreen
% set(gca,'FontSize',26)
% 
% 
% hold off

%% Plot Total Input Energy
Q_HC=452; % Heat loss over Cold Chamber in [W] 
fig3=figure(3);
clf(fig3)
var_alpha=0.7;
hold on
area(fluid.u_max,Q_HC+P_loss+P_jet,'Displayname','Air Stream Kinetic Energy','FaceAlpha',var_alpha)
area(fluid.u_max,P_loss+Q_HC,'Displayname','Dissipation Energy Wind Tunnel','FaceAlpha',var_alpha)
area(fluid.u_max,Q_HC*ones(length(fluid.u_max),1),'Displayname','Heat Loss Cold Chamber Wall','FaceAlpha',var_alpha)
plot(fluid.u_max,P_loss+P_jet+Q_HC,'r','LineWidth',2,'Displayname','Total Input Energy')
xlabel('$$U_\mathrm{air,TS}/(\mathrm{ms}^{-1})$$','interpreter','latex')
ylabel('$$ W_\mathrm{tot}$$/W','interpreter','latex')
legend('-DynamicLegend','Location','NorthWest')
grid on
hold off
% Plot settings
% set(gcf, 'Position', get(0, 'Screensize')); % Set Figure to FullScreen
% set(gca,'FontSize',26)
% set(gcf,'color','w');
% set(gca,'Fontname','Charter');
fig3=plotprops(fig3);

% print('D:\Promotion\Media\Präsentationen\SLA_Lunch Meeting 2020\InputEnergy','-dpng')
SaveName='WindTunnelHeat';
% matlab2tikz(['D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\3-SetupMaterials\figures\',SaveName,'_tikz.tex'],'showInfo',false,'width','0.8\textwidth') 


%% Plot Pressure Loss of Components
fig4=figure(4);
clf(fig4)
var_alpha=0.6;
hold on
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N+Press_loss_DTS+Press_loss_DL+Press_loss_DEx+Press_loss_Ex+Press_loss_In,'Displayname','$$\Delta P$$-Inlet','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N+Press_loss_DTS+Press_loss_DL+Press_loss_DEx+Press_loss_Ex,'Displayname','$$\Delta P$$-Outlet','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N+Press_loss_DTS+Press_loss_DL+Press_loss_DEx,'Displayname','$$\Delta P$$-Exit Diffusor','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N+Press_loss_DTS+Press_loss_DL,'Displayname','$$\Delta P$$-2nd Diffusor','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N+Press_loss_DTS,'Displayname','$$\Delta P$$-1st Diffusor','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC+Press_loss_N,'Displayname','$$\Delta P$$-Nozzle','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC+Press_loss_SC,'Displayname','$$\Delta P$$-Settling Chamber Duct','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC+Press_loss_S_TC,'Displayname','$$\Delta P$$-Turbulence Control Screen','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2+Press_loss_HC,'Displayname','$$\Delta P$$-Honey Comb','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS+Press_loss_C2,'Displayname','$$\Delta P$$-Corner','FaceAlpha',var_alpha);
area(fluid.u_max,Press_loss_TS,'Displayname','$$\Delta P$$-Test Section','FaceAlpha',var_alpha);
plot(fluid.u_max,pressure_loss,'r','Displayname','Overall Pressure Loss','LineWidth',2)

xlabel('$$U_\mathrm{air,TS}/(\mathrm{ms}^{-1})$$','interpreter','latex')
ylabel('$$\Delta P/$$ Pa','interpreter','latex')
legend('-DynamicLegend','Location','NorthWest','interpreter','latex')
grid on
hold off

fig4=plotprops(fig4); 
% Plot settings
% set(gcf, 'Position', get(0, 'Screensize')); % Set Figure to FullScreen
% set(gca,'FontSize',26)
% set(gcf,'color','w');
% set(gca,'Fontname','Charter');

SaveName='WindTunnelPressureLoss';
% matlab2tikz(['D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\3-SetupMaterials\figures\',SaveName,'_tikz.tex'],'showInfo',false,'width','0.8\textwidth') 
% print('D:\Promotion\Media\Präsentationen\SLA_Lunch Meeting 2020\PressureLoss','-dpng')

%% Plot Jet Energy
% figure(5); 
% hold on
% plot(fluid.vol_flow*3600,P_jet)
% xlabel('Volume Flow [m^3/h]')
% ylabel('Jet Energy  [W]')
% grid on
% hold off
