
ind_sel=abs(arr_impexcntr)<0.0015;

DefVel_Exp=arr_cordropspeed(ind_sel)-arr_dropspeed(ind_sel);

U_rel=arr_dropspeed(ind_sel);
rho_air=air_props(arr_airtemp(ind_sel),'rho');
rho_wt=water_props(arr_droptemp(ind_sel),'rho');
DefVel_Mod=sqrt(rho_air./rho_wt).*U_rel;

% Added stagnation point pressure from air flow
DefVel_Mod2=U_rel.*(-rho_air-sqrt(rho_air.^2+1.5.*rho_air.*rho_wt))./rho_wt;

surf_rad=0.015;
char_L=arr_avdiamExp0AV(ind_sel);
% char_L=surf_rad;
delta_r=DefVel_Mod2.*char_L./arr_dropspeed(ind_sel);
kappa_mod=1./(0.5.*arr_avdiamExp0AV(ind_sel)-delta_r);

%% Deformation Velocity
fig1=figure(1);

scatter(DefVel_Exp,DefVel_Mod,[],U_rel,'o','filled','Displayname','w/o Stagnation Pressure')
hold on
% scatter(DefVel_Exp,DefVel_Mod2,[],U_rel,'d','filled','Displayname','incl. Stagnation Pressure')
hold off


    title('Deformation Velocity Exp-Mod')
    xlabel('$$ U_{def,Exp}$$')
    ylabel('$$ U_{def,Mod}$$')
%     legend('-DynamicLegend')
    
     cb=colorbar;
     colormap jet
    
    fig1=plotprops(fig1);
    
%% Curvature Change
fig2=figure(2);
scatter(arr_impcurv(ind_sel),kappa_mod,'filled')

    title('Curvature change Exp-Mod')
    xlabel('$$ \kappa_{def,Exp}$$')
    ylabel('$$ \kappa_{def,Mod}$$')

     fig2=plotprops(fig2);
    