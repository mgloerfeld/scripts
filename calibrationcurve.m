% Plot Calibration Curve from Setra ASL Pressure Transducer


% arr_press=0:2.5:25;
% arr_outV=[0.10,1.012,2.013,3.014,4.017,5.016,6.018,7.02,8.022,9.022,10.024];
arr_press=[1000,980,960,940,1020,1040,1060,1080,1100,1160];
arr_outV=[6.226,6.101,5.976,5.851,6.351,6.477,6.601,6.726,6.849,7.226];

% Polyfit for linear regression
coeff_lin=polyfit(arr_outV,arr_press,1);
coeff_sq=polyfit(arr_outV,arr_press,2);

% X-values for target vector
X=arr_outV;
% Target Vector
Y_lin=coeff_lin(1)*X+coeff_lin(2);
Y_sq=coeff_sq(1)*X.^2+coeff_sq(2).*X+coeff_sq(3);

% Difference
diff_lin=abs(arr_press-Y_lin);
diff_sq=abs(arr_press-Y_sq);
% Mean of differences
meandiff_lin=mean(diff_lin);
meandiff_sq=mean(diff_sq);
% Standard deviation
std_diff_lin=std(diff_lin);
std_diff_sq=std(diff_sq);

plot(Y_lin,X)
hold on
% plot(Y_sq,X)
scatter(arr_press,arr_outV)
hold off