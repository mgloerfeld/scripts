Filename='F:\Experiments\Burzynski_impactConditionsAndEjectedVolume_Corona.txt';


if isfile(Filename)
    fid=fopen(Filename);
    Data=textscan(fid,'%f%f%f%f%f%f%f%f%f%f%f%f%f','Headerlines',2);
    fclose(fid);
else
    disp('No file found')
    return
end

Data=Data';

D=Data{1,1};
D_std=Data{2,1};
U=Data{3,1};
U_std=Data{4,1};
rho_l=Data{5,1};
mu_l=Data{6,1};
sigma=Data{7,1};
rho_g=Data{8,1};
mu_g=Data{9,1};
lambda_g=Data{10,1}.*10.^-9;
beta=Data{11,1};
mean_Vd_VD=Data{12,1}/100;
std_Vd_VD=Data{13,1}/100;


Re=rho_l.*D.*U./mu_l;
We=rho_l.*(U.^2).*D./sigma;
Oh=sqrt(We)./Re;


Data_Burzynski.EthanolCorona=table(D,D_std,U,U_std,rho_l,mu_l,sigma,rho_g,mu_g,lambda_g,beta,mean_Vd_VD,std_Vd_VD,Re,We,Oh);

