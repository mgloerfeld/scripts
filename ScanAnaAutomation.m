% Automated Routine for estimating Splat geometry from impact on ice
% Scanfolder='F:\Experiments\ImpactOnIce\Experiments\';
% Scanfiles = dir([Scanfolder '**\*V*']);
% Scanfiles=Scanfiles(cat(1,Scanfiles.isdir));
% DropSizes=table('Size',[length(Calibfiles) 1],'VariableTypes',{'double'});
% DropSizes.Properties.VariableNames={'MeasuredVolume'};
% CalibFileNames = {Scanfiles.name};
% log_unusables=~contains(CalibFileNames,'NOTUSABLE'); % Search for marker 'NOTUSABLE' in Filename
% Scanfiles =Scanfiles(log_unusables);
% % kk=0;

for ll=1:length(Scanfiles)
    
     if Scanfiles(ll).LineMeasGood==1 || Scanfiles(ll).LineMeasGood==-1
         continue
     end
     
    Location=[Scanfiles(ll).folder,'\',Scanfiles(ll).name,'\'];
    
    % Skip Folder if no images exist
    if exist([Location,Scanfiles(ll).name,'_scan'],'file')~=2
        disp([Location,' skipped'])
        continue
    end
    
%     if getdatafromcih([Location,Scanfiles(ll).name,'.cih'],'dropdendritic')==1
       
       [var_Lamellathick,var_SplatDiam,var_Rimheight,var_MaxLamellaTip]=ScanAnalysis(Location);
       
        writeintocih(Location,'Splat_Lamella_Thickness',var_Lamellathick); % Write Drop Diameter into cih file
        writeintocih(Location,'Splat_Diameter',var_SplatDiam); % Write Drop Diameter into cih file
        writeintocih(Location,'Splat_Rim_Height',var_Rimheight); % Write Drop Diameter into cih file
        writeintocih(Location,'Splat_Max_Tip',var_MaxLamellaTip); % Write Drop Diameter into cih file
        
        
%     end
end
