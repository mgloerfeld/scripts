% Reproduce Nucleation Map from Frank 1982

load('Data_Franks1982') % load Data
% 

fig1=figure(1); 


modelfun = @(b,x)(b(1).*(x.^(b(2)))+b(3));
beta0=[-10,-1,-40];
coeff_hetfit=nlinfit(Data_Franks1982.HetNucleation.Line(:,1),Data_Franks1982.HetNucleation.Line(:,2),modelfun,beta0);


% beta02=[1,1,-40];
% modelfun2 = @(b,x)(-b(1).*log(x).^(b(2))+b(3));
% coeff_homfit=nlinfit(Data_Franks1982.HomNucleation.Line(:,1),Data_Franks1982.HomNucleation.Line(:,2),modelfun2,beta02);
% fit_Hom=fit(Data_Franks1982.HomNucleation.Line(:,1),Data_Franks1982.HomNucleation.Line(:,2),'cubicinterp');


hold on 
% Plot Het Data
Het_Linerng=(100e-6:0.01:0.04);
plot(Het_Linerng,modelfun(coeff_hetfit,Het_Linerng),'--k','Displayname',' Het. Nucleation (Brigg)');
plot(Data_Franks1982.HomNucleation.Line(:,1),Data_Franks1982.HomNucleation.Line(:,2),'k','Displayname','Hom. Nucleation (Mossop)')

scatter(Data_Franks1982.HetNucleation.Points(:,1),Data_Franks1982.HetNucleation.Points(:,2),'ko','Displayname','Brigg');
scatter(Data_Franks1982.HomNucleation.Carte(:,1),Data_Franks1982.HomNucleation.Carte(:,2),'+','Displayname','Carte')
scatter(Data_Franks1982.HomNucleation.Bigg(:,1),Data_Franks1982.HomNucleation.Bigg(:,2),'ko','HandleVisibility','off')
scatter(Data_Franks1982.HomNucleation.Mossop(:,1),Data_Franks1982.HomNucleation.Mossop(:,2),'k^','Displayname','Mossop')
scatter(Data_Franks1982.HomNucleation.LangMas_Med(:,1),Data_Franks1982.HomNucleation.LangMas_Med(:,2),'kd','Displayname','Langham & Mason (Median)')
scatter(Data_Franks1982.HomNucleation.LangMas_Low(:,1),Data_Franks1982.HomNucleation.LangMas_Low(:,2),'ks','Displayname','Langham & Mason (Lowest)')

hold off

xlim([1e-6,0.1]);
ylim([-50,-10])

set(gca,'xscale','log')

xlabel('$$ D_0 / \mathrm{m} $$','interpreter','latex')
ylabel('$$ T_0 / ^\circ\mathrm{C} $$','interpreter','latex')
legend('-DynamicLegend','Location','Northwest')

fig1=plotprops(fig1);

% SavingLocation='D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\2-Theory\figures\';
SavingLocation='D:\Promotion\Dissertation\Figure_Rawfiles\';

SaveName='NucleationMapFranks1982_Replication';
%     print([SavingLocation,SaveName],'-dpng')
%  matlab2tikz([SavingLocation,SaveName,'_tikz.tex'],'showInfo',false,'width','0.8\textwidth')