% Calculating rotational speed/ radius of rotating plate 

r=6e-2:1e-2:0.20; % Plate radius in m
v= 250; % Translational speed of disc
rpm=v*60./(2.*pi.*r); % Rotations per minute of disc
omega=2*pi*rpm/60;

%% 
fig1=figure(); 

plot(r,rpm);

xlabel('R/m')
ylabel('RPM')
fig1=plotprops(fig1,'BigFont');
print('D:\Promotion\Nebenprojekte\NANNY\RPMoverR','-dpng')

%% Rotational energy
rho= 2700; % Density of aluminum
h=0.002; % Height of Disc


J=0.5.*rho*h*pi.*(r.^2);
E_rot=J.*(r.^2).*(omega.^2); 


% Plot Rotational Energy
figure();
plot(r,E_rot)


title('Rotational Energy')
xlabel('Radius/m')
ylabel('Rotational Energy /Nm')


% Plot Moment of inertia
figure();
plot(r,J)


title('Moment of inertia')
xlabel('Radius/m')
ylabel('Moment of inertia /Nm')

r_des=0.12; 
ind_r=find(r==r_des);
W_eng=900;
acc_time=E_rot(ind_r)./W_eng;

disp(['For radius r= ',num2str(r_des),': RPM is ', num2str(rpm(ind_r)), ' and needed torque is ', num2str(J(ind_r)),' and acceleration time  is ', num2str(acc_time/60),' mins']);






