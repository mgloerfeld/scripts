% Calculate the velocity of a spherical drop in a free stream

%% Default values
% Drop Properties
d_drop=2.5e-3;                              % Drop Diameter
rho_w=water_props(-10,'rho');               % Water density (20�C)
m_drop=(4/3)*pi*((0.5*d_drop)^3)*rho_w;     % Drop Mass
A_drop=pi*(0.5*d_drop)^2;                   % Drop Cross section
% Air Properties
rho_air=air_props(-10,'rho');               % Air density
nu_air=air_props(-10,'nu');                 % Air kinematic viscosity
g=9.81;                                     % Gravitational constant
% u_air0=2.2222;
uex_des=22;                                 % Desired Maxium Air Velocity 
% Nozzle Properties
den_nzl= 0.32;
dex_nzl= 0.14;
H_nzl=0.32;
d_pipe=dex_nzl;
H_pipe=0.4;
% den_dif=d_pipe;
% dex_dif=0.1;
% H_dif=0.2;

theta_nzl=atan(0.5*(den_nzl-dex_nzl)/H_nzl);
Arat_nzl=den_nzl/dex_nzl;

u_air0=uex_des*(dex_nzl^2/den_nzl^2); % Initial air velocity 
%% Timestep and Preallocation
time=0.5; % Time of fall
timestep=0.00001; % Time step of discretisation

drop_vel=[4]; % Initial Velocity of Drop

%% Preallocation
Vd=NaN(int32(time/timestep),length(drop_vel)); % Drop Velocity
Xd=NaN(int32(time/timestep),length(drop_vel)); % Drop Location
Ad=NaN(int32(time/timestep),length(drop_vel)); % Drop Acceleration
U_air=NaN(int32(time/timestep),length(drop_vel)); % Air Velocity

%% Discrete solution of DGL

for kk=1:length(drop_vel)
    
    V_drop0=drop_vel(kk);
        U_air(:,kk)=0;
    
%% Iterative Solution
    Vd(1,kk)=V_drop0;
    Xd(1,kk)=0;
    Ad(1,kk)=0;
    
%     syms vrel
    for ii=2:(time/timestep)
        try
        %% Calculate Air Velocity
        
            if Xd(ii-1,kk)<=H_nzl
                U_air(ii,kk)=(u_air0*den_nzl^2*H_nzl^2)/(((dex_nzl-den_nzl)*Xd(ii-1,kk)+den_nzl*H_nzl)^2); % Air velocity inside of a nozzle
                A_dif(ii,kk)=(pi/(4*H_nzl^2))*(((dex_nzl-den_nzl)*(Xd(ii-1,kk))+den_nzl*H_nzl)^2);
            elseif Xd(ii-1,kk)>=H_nzl %&& Xd(ii-1,kk)<=(H_pipe+H_nzl)
                U_air(ii,kk)=U_air(ii-1,kk);
                U_dif0=U_air(ii,kk);
            elseif Xd(ii-1,kk)>(H_pipe+H_nzl) && Xd(ii-1,kk)<=(H_pipe+H_nzl+H_dif)
                U_air(ii,kk)=(U_dif0*den_dif^2*H_dif^2)/(((dex_dif-den_dif)*(Xd(ii-1,kk)-(H_pipe+H_nzl))+den_dif*H_dif)^2);
                A_dif(ii,kk)=(pi/(4*H_dif^2))*(((dex_dif-den_dif)*(Xd(ii-1,kk)-(H_pipe+H_nzl))+den_dif*H_dif)^2);
            elseif Xd(ii-1,kk)>(H_pipe+H_nzl+H_dif)
                U_air(ii,kk)=U_air(ii-1,kk);
            end


            Re_rel=abs(U_air(ii,kk)-Vd(ii-1,kk))*d_drop/nu_air; % Reynoldsnumber of relativ Velocity
            
            %% Calculate CD-Coefficient of a sphere with respect to Reynoldsnumber after Fauchais (2014)
            if Re_rel<0.2
                Cd_sphere=(24/Re_rel);
            elseif Re_rel>0.2 && Re_rel<2
                Cd_sphere=(24/Re_rel)*(1+0.187*Re_rel);
            elseif Re_rel>2 && Re_rel<20
                Cd_sphere=(24/Re_rel)*(1+0.11*Re_rel^0.81);
            elseif Re_rel>20 && Re_rel<200
                Cd_sphere=(24/Re_rel)*(1+0.189*Re_rel^0.62);
            elseif Re_rel>200
                Cd_sphere=(24/Re_rel)+0.4+(6/(1+Re_rel^0.5));
            end
            
            
            %% Solve differential equation
            
%             eqn= vrel==sqrt((((vrel-(U_air(ii,kk)-Vd(ii-1,kk)))/timestep)-g)/(0.5*(rho_air/m_drop)*(Cd_sphere)*A_drop));
%             Vrel=solve(eqn,v);
%             Vd(ii,kk)=Vrel+U_air(ii,kk);
            
            if U_air(ii,kk)>=Vd(ii-1,kk) % Check if drag is accelerant or decellerant and change sign of force
            Ad(ii,kk)=g+0.5*(rho_air/m_drop)*(Cd_sphere)*A_drop*(U_air(ii,kk)-Vd(ii-1,kk))^2; 
            elseif U_air(ii,kk)<Vd(ii-1,kk)
            Ad(ii,kk)=g-0.5*(rho_air/m_drop)*(Cd_sphere)*A_drop*(U_air(ii,kk)-Vd(ii-1,kk))^2; 
            end
            Vd(ii,kk)=Vd(ii-1,kk)+((Ad(ii,kk)+Ad(ii-1,kk))/(2))*timestep;
            Xd(ii,kk)= Xd(ii-1,kk)+((Vd(ii,kk)+Vd(ii-1,kk))/(2))*timestep;
        catch
            disp('Iteration aborted! Displaying predetermined values.')
            break
        end
    end
Re_Ch= U_air(end-1,kk)*dex_nzl/nu_air; % Calculate Channel Reynoldsnumber in flow area
% [val_rel25,ind_rel25]=min(abs(U_air(:,kk)-Vd(:,kk)-25));

% %Plot drop velocity over time
%     figure(1);
%     hold on;
%     times=0:timestep:(length(Vd(:,kk))*timestep)-timestep;
%     plot(times,Vd(:,kk),'Displayname',['u_{d=' num2str(d_drop) 'm,0,approx}=' num2str(drop_vel(kk)) 'm/s'])
%     title(['Drop Velocity over Time with u_{a,0}=' num2str(u_air0)])
%     legend('-DynamicLegend')
%     xlabel('Time [t]')
%     ylabel('Vrel[m/s]')
%     hold off

% Plot drop, air and relative velocity over distance   
    fig2=figure(2);
    hold on;
    set(gca,'FontName','Calibri')
    lnwdth=2;
    plot(Xd(:,kk),Vd(:,kk),'Displayname','u_{drop}','LineWidth',lnwdth)
    plot(Xd(:,kk),U_air(:,kk),'Displayname','AirVelocity','LineWidth',lnwdth)
    plot(Xd(:,kk),U_air(:,kk)-Vd(:,kk),'Displayname','Rel. Velocity','LineWidth',lnwdth)
%     line([Xd(ind_rel25,kk) Xd(ind_rel25,kk)],[0 max(U_air(:,kk))]); % Plot a line at relative velocity of 25 m/s
    line([(H_pipe+H_nzl) (H_pipe+H_nzl)],[0 max(U_air(:,kk)+10)],'Color',[0.5 0.5 1],'LineStyle','--','Displayname','ImpactSurfaceLocation','LineWidth',lnwdth);
%     title(['Drop- and Air Velocity in Wind Tunnel u_{air,0}=' num2str(round(u_air0,1)),'m/s',' and ','u_{d=' num2str(d_drop) 'm,0}=' num2str(drop_vel(kk)) 'm/s'],'FontSize',30)
    leg=legend('-DynamicLegend','Location','NorthWest');
    xlabel('x/m','FontSize',24)
    ylabel('v/(ms-^1)','FontSize',24)
    set(leg,'Fontsize',20)
    set(gca,'Fontsize',20)
    set(gca,'Fontname','Charter');
    set(gcf,'color','w');
    xlim([0 1])
    ylim([0 max(U_air)+10])
    hold off
    grid on
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    
    fig2=plotprops(fig2,'MediumFont');
%     set(gcf, 'Toolbar', 'none', 'Menu', 'none');
%     print('D:\Promotion\Media\Pr�sentationen\SLA_Lunch Meeting 2020\DropInFreeStream_20ms','-dpng')

    
% %Plot air velocity over distance
%     figure(3);
%     hold on;
%     plot(Xd(:,kk),U_air(:,kk),'Displayname','AirVelocity')
%     title(['Air Velocity over Distance with u_{a,0}=' num2str(u_air0)])
%     legend('-DynamicLegend')
%     xlabel('Distance[m]')
%     ylabel('v[m/s]')
%     hold off
%     grid on

    %% Numerical Solver Solution
%     % Velocity
%     syms v(t)
% 
%     cond = v(0)==v_rel0;
%     ode45 = diff(v,t)+sgn*0.5*(rho_air/m_drop)*Cd_sphere*A_drop*v^2==g;
%     vSol(t) = dsolve(ode45,cond);
%     vSol=simplify(vSol);

%     % Distance
%     syms x(t)
%     Dx(t)= diff(x(t),t);
% 
%     cond1 = x(0)==0;
%     cond2 = Dx(0)==v_rel0;
%     conds = [cond1 cond2];
%     ode45 = diff(x,t,2)+sgn*0.5*(rho_air/m_drop)*(24/(diff(x,t)*d_drop/nu_air))+0.4+(6/(1+(diff(x,t)*d_drop/nu_air)^0.5))*A_drop*diff(x,t)^2==g;
%     xSol(t) = dsolve(ode45,conds);
%     xSol=simplify(xSol);
%     
   

    
%     figure(1);
%     hold on;
%     times=0:timestep:2;
%     plot(times,vSol(times),'Displayname',['u_{d=' num2str(d_drop) 'm,0}=' num2str(drop_vel(kk)) 'm/s'])
%     title(['Rel. Velocity over Time with u_{a,0}=' num2str(u_air)])
%     legend('-DynamicLegend')
%     xlabel('Time [t]')
%     ylabel('Vrel[m/s]')
%     hold off

%     figure(2);
%     hold on;
%     plot(xSol(times),vSol(times),'Displayname',['u_{d=' num2str(d_drop) 'm,0}=' num2str(drop_vel(kk)) 'm/s'])
%     title(['Rel. Velocity over Distance with u_{a,0}=' num2str(u_air)])
%     legend('-DynamicLegend')
%     xlabel('Distance[m]')
%     ylabel('vrel[m/s]')
%     xlim([0 inf])
%     hold off
    
%     figure(3);
%     hold on;
%     plot(times,xSol(times),'Displayname',['u_{d=' num2str(d_drop) 'm,0}=' num2str(drop_vel(kk)) 'm/s'])
%     title(['Distance over time with u_{a,0}=' num2str(u_air)])
%     legend('-DynamicLegend')
%     xlabel('Time[s]')
%     ylabel('Distance[m]')
%     xlim([0 inf])
%     hold off
    
    
%     figure(4);
%     hold on;
%     plot(xSol(times),-vSol(times)+u_air,'Displayname',['u_{d=' num2str(d_drop) 'm,0}=' num2str(drop_vel(kk)) 'm/s'])
%     title(['Drop Velocity over Distance with u_{a,0}=' num2str(u_air)])
%     legend('-DynamicLegend','Location','NorthWest')
%     xlabel('Distance[m]')
%     ylabel('U_{drop}[m/s]')
%     xlim([0 2.5])
%     hold off

end