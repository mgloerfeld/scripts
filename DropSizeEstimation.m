function [var_dropvolume,var_excntrcty]=DropSizeEstimation(Location,varargin)% Analyze Current Drop
%% DROPSIZEESTIMATION Function to determine drop volume from several side view images

%   Syntax
%   -----------------------------------------------
%   DropSizeEstimation(Location)
%
%   Description
%   ----------------------------------------------%   This function determines the drop volume from several side view images
%   of a sessile drop
%
%   Default settings are: 
%   
%   Input:  
%   ----------------------------------------------
%           Location : String that contains path to folder containing cih
%           (including "\" at the end)
%           'SetBWThreshold' : Set custom black and white threshold
%           (Default: 30)
% 
% 
% 
%   Output: 
%           var_dropvolume : Volume of drop in m^3
%   ----------------------------------------------
%   ----------------------------------------------
%
%
%   Author: Mark Gloerfeld
%   Date: 16122020

bl_bwset=false;
bl_skip=false;

for ii=1:length(varargin)
    
    if bl_skip==true
        bl_skip=false;
        continue
    end
    
    switch varargin{ii}
        case 'SetBWThreshold'
            bl_bwset=true;
            var_bwthr=varargin{ii+1};
            bl_skip=true;
    end

end


    % Set boolean for validation Plots
    bl_ValidPlot=true;
    bl_overview=false;
    bl_saveplts=true;
%     bl_calibrun=true;
    
    
    
    % Load all side view images
    img_BG_SideOR=imread([Location,'BG_Side'],'tiff');
    if length(img_BG_SideOR(1,1,:))>1
        img_BG_SideOR=rgb2gray(img_BG_SideOR);
    end
%     img_BG_SideOR=imadjust(img_BG_SideOR,[0 0.88]);

        
    
   
    % Get Pixelsize for SideView
    var_pixsize_Side=getpixelsize(Location);

    % Get all Side Views available
    ImageFiles = dir([Location '**\DI*.tif']);
    FileNames = {ImageFiles.name};
    FileFolder={ImageFiles.folder};
    var_nimg=length(ImageFiles); % Get number of images

    %Rearrange file to 0D being analysed first
    arr_angles=str2double(cellfun(@(x)regexp(x, '[+-]?\d+\.?\d*', 'match'),FileNames)); % Get all angels from Filenames
    [~,ind_angsrt]=sort(abs(arr_angles)); % Get index of sorted array so 0D is first
    arr_angles=arr_angles(ind_angsrt); % Sort angles
    FileFolder=FileFolder(ind_angsrt); % Sort FileFolder
    FileNames=FileNames(ind_angsrt); % Sort Filenames


    % Preallocate Cell boundary array
    cl_dropcoord=cell(1,var_nimg);

    % Parameters for image analyis
    if bl_bwset==false
        var_bwthr=30; % Default black and white treshold
    end
    var_bounddistthr=0.9; % Percentage of boundary coordinates distance deviation
    var_anglesecs=8; % Number of angle sections to erase errorneous data inside of drop
    
    % Get Surface position and inclination in BG image
%     [~,~,var_srfangBG,~,arr_srfedgeptsBG]=getsurfaceposition(img_BG_SideOR,var_pixsize_Side,'Bottom');

    for ii=1:var_nimg
        
        img_current=imread([FileFolder{ii},'\',FileNames{ii}]);
        if length(img_current(1,1,:))>1
            img_current=rgb2gray(img_current);
        end
%         img_current=imadjust(img_current,[0 0.95]);
        img_original=img_current;
        
        %% Adjust background image due to possible change in surface position
        if ii==1
            [optimizer, metric] = imregconfig('monomodal'); % Initiale opts for imgregtform
            tform=imregtform(img_BG_SideOR,img_current(:,:),'translation',optimizer,metric); % Get transformation for translation of image based on 300px wide section of surface        

    %         % Get Surface position and inclination in BG image
    %         [~,~,~,~,arr_srfedgeptsBG]=getsurfaceposition(img_BG_SideOR,var_pixsize_Side,'Bottom');
    %     
            % Get Surface Inclination from Drop image
            [~,~,var_srfang,~,~]=getsurfaceposition(img_original,var_pixsize_Side,'Bottom');

            img_BG_Side=imwarp(img_BG_SideOR,tform,'bilinear','OutputView',imref2d(size(img_current)),'FillValues',NaN); % Adjust BG Image for better BG substraction          
%             img_BG_Side=img_BG_SideOR;
        end
        
        var_surfslp=tan(var_srfang); % Save slope from surface inclination angle for fitting surface line to drop
        %% Get surface centerline value from 0D image
        if ii==1
            var_surfcntrcoord=getsurfaceposition(img_original,var_pixsize_Side,'Bottom');
        end
        
        %% Image Analysis
        img_current=(img_BG_Side-img_current)>var_bwthr; % Convert to BW image with increased threshold
        img_current=imfill(img_current,'holes'); % Fill holes        
        
        %% Edge Detection for Side View for filtering purpose
        % Use Regionprops to get area and centroids of drop object
        stc_regprops_Side=regionprops(img_current,'Area','PixelList','Centroid','Extrema','Extent');
        % Delete image noise
    %     ind_del=cat(1,stc_regprops_Side.Area)<var_area_thr; % Search for areas to small to be a drop
        [~,ind_drop]=max(cat(1,stc_regprops_Side.Area));
        stc_regprops_Side=stc_regprops_Side(ind_drop); % Delete all objects except for drop

        %% Use Watershedding to separate drop from image errors indicated by excentricity
%         if stc_regprops_Side.Extent<0.5
%             img_wtshdbw=zeros(size(img_current)); % Create Empty image
%             img_wtshdbw(sub2ind(size(img_current),stc_regprops_Side.PixelList(:,2),stc_regprops_Side.PixelList(:,1)))=1; % Put drop object in image
%             img_dst=-bwdist(~img_wtshdbw); % Create complement of distance transform of bw image
%             msk_extmin=imextendedmin(img_dst,2);
%             img_dst2=imimposemin(img_dst,msk_extmin);
%             img_wtshd=watershed(img_dst2); % Watershed the drop object
%             
%             img_wtshd(~img_wtshdbw)=0; % Erase everything but drop object
%             [~,~,var_wtshdDO]=find((img_wtshd)',1); % Find value of cut drop object
%             img_current=img_wtshd==var_wtshdDO; % Delete every object that is not drop object
%             stc_regprops_Side=regionprops(img_current,'Area','PixelList','Centroid','Extrema','Extent');
%         end
        %% Get Boundary coordinates
%         arr_boundcoordsbwb=cell2mat(bwboundaries(img_current,'noholes')); % Get boundary coordinates of all objects in image
%         arr_boundcoords=[arr_boundcoordsbwb(:,2) arr_boundcoordsbwb(:,1)]; % Switch x-y colums 
%         arr_boundcoords=intersect([arr_boundcoordsbwb(:,2) arr_boundcoordsbwb(:,1)],stc_regprops_Side.PixelList,'rows'); % Get boundary objects of drop

        % Use SubpixelEdgeDetection for boundary coordinates and erase all
        % points not from drop
%         TJ_boundcoords=subpixelEdges(img_BG_Side-img_original,var_bwthr);
        TJ_boundcoords=subpixelEdges(img_current,var_bwthr);
        TJ_2dcoords=[TJ_boundcoords.x TJ_boundcoords.y];
        TJ_dist=pdist2(stc_regprops_Side.PixelList,TJ_2dcoords); % Calculate pairwise distance between TJ points and PixelList of regprops
        ind_bndpt=TJ_dist<2; % Get all points with distance <2 
        arr_boundcoords=TJ_2dcoords(any(ind_bndpt==1),:); % Erase all points with pixel distance greater than 2 to points from biggest object (PixelList)
        
        % Cluster data to remove errorneous points
%         TJ_2dcoords=TJ_2dcoords(any(ind_bndpt==1),:); % Erase all points with pixel distance greater than 2 to points from biggest object (PixelList)
%         lbl_pts=clusterdata(TJ_2dcoords,'Criterion','distance','Cutoff',20); % Cluster data with distance smaller than 3
%         arr_boundcoords=TJ_2dcoords(lbl_pts==lbl_pts(TJ_2dcoords(:,2)==min(TJ_2dcoords(:,2))),:);
        
        % Crop boundary coordinates with left and right extrema 
        arr_surf=[mean(stc_regprops_Side.Extrema(3:4,:)); mean(stc_regprops_Side.Extrema(7:8,:))]; % Mean of left and right extrema for surface
        arr_boundcoords=arr_boundcoords(arr_boundcoords(:,2)<=mean(arr_surf(:,2)),:); % Cut coordinates at surface line

        %% Create Surface line from max and min points of boundary and Surface inclination from BG image
            % Cut boundary points below     
            arr_surf=[mean(arr_boundcoords(arr_boundcoords(:,1)==min(arr_boundcoords(:,1)),:),1); mean(arr_boundcoords(arr_boundcoords(:,1)==max(arr_boundcoords(:,1)),:),1)]; % Get mean of left and right extrema
        %     arr_coeff=polyfit(arr_surf(:,1),arr_surf(:,2),1); % Calculate surface line
            arr_coeff(1)=var_surfslp; % Put precalculated slope from surface position as first coefficient
            arr_coeff(2)=mean(arr_surf(:,2))-(arr_coeff(1)*mean(arr_surf(:,1))); % Calculate best fit Y0 of line
            arr_boundcoords=arr_boundcoords(arr_boundcoords(:,2)<=polyval(arr_coeff,arr_boundcoords(:,1)),:); % Cut coordinates at surface line

        
        
        
        
        %% Calculate intersection point with surface line
        %Left point
        arr_dstL=pdist2(arr_boundcoords,[arr_surf(1,1),polyval(arr_coeff,arr_surf(1,1))]);
        arr_ftptsL=arr_boundcoords(ismember(arr_dstL,mink(arr_dstL,5)),:); % Get 5 closest points to surface touch point
        arr_srfcffL=polyfit(arr_ftptsL(:,2),arr_ftptsL(:,1),1); % Calculate best fitting line one points ( f(y)to avoid bad fitting for 90� CA) 
        arr_srftchpt=[polyval(arr_srfcffL,(arr_coeff(1)*arr_srfcffL(2)+arr_coeff(2))/(1-arr_srfcffL(1)*arr_coeff(1))),(arr_coeff(1)*arr_srfcffL(2)+arr_coeff(2))/(1-arr_srfcffL(1)*arr_coeff(1))]; % Calculate intersection point of both lines
        % Right point
        arr_dstR=pdist2(arr_boundcoords,[arr_surf(2,1),polyval(arr_coeff,arr_surf(2,1))]);
        arr_ftptsR=arr_boundcoords(ismember(arr_dstR,mink(arr_dstR,5)),:); % Get 5 closest points to surface touch point
        arr_srfcffR=polyfit(arr_ftptsR(:,2),arr_ftptsR(:,1),1); % Calculate best fitting line one points ( f(y)to avoid bad fitting for 90� CA)
        arr_srftchpt(2,:)=[polyval(arr_srfcffR,(arr_coeff(1)*arr_srfcffR(2)+arr_coeff(2))/(1-arr_srfcffR(1)*arr_coeff(1))),(arr_coeff(1)*arr_srfcffR(2)+arr_coeff(2))/(1-arr_srfcffR(1)*arr_coeff(1))]; % Calculate intersection point of both lines

        ind_delclsrf=sum(abs(arr_boundcoords(:,1)-arr_srftchpt(:,1)')<0.4,2,'native'); % Check for points with x-value too close to surface points to avoid malfunction in interpolation
        arr_boundcoords(ind_delclsrf,:)=[]; % Delete points too close to surface points
        arr_boundcoords=[arr_boundcoords; arr_srftchpt]; % Add surface touch points to array of points

        %% Erase points from glare points etc. inside of drop
        arr_origin=[mean(arr_boundcoords(:,1)),polyval(arr_coeff,mean(arr_boundcoords(:,1)))]; % Calculate origin on surface line for distance measure
        arr_bounddist=pdist2(arr_boundcoords,arr_origin); % Calculate distances from origin
        arr_boundang=acos(abs(arr_boundcoords(:,1)-arr_origin(:,1))./arr_bounddist); % Get absolute value of all angles of boundary points to horizontal 
        % Check for errorenous points in var_angelsecs ranges 
        arr_saveerr=NaN(length(arr_boundcoords),2); % Empty Error saving array
        for nang=1:var_anglesecs
            arr_ctang=[0 pi/(2*var_anglesecs)]+(pi/(2*var_anglesecs))*(nang-1); % Create angle range
            ind_curpts=arr_boundang>=arr_ctang(1) & arr_boundang<arr_ctang(2); % Get indices of all points with an angle smaller than var_ctang
            if std(arr_bounddist(ind_curpts))>5 % Skip deletion if standard deviation is lower than 10 (no errors present)
                ind_del=ind_curpts & arr_bounddist<mean(arr_bounddist(ind_curpts))*var_bounddistthr;
                arr_saveerr(ind_del,:)=arr_boundcoords(ind_del,:); % Save Errors
                arr_boundcoords(ind_del,:)=NaN; % Change all errors to NaN Values
            end
        end
        arr_boundcoords(isnan(arr_boundcoords(:,1)),:)=[]; % Erase all NaN values 
        
        if ii==1
        % Save Excentricity of drop after receding from first image
        var_excntrcty=abs((mean(arr_boundcoords(:,1))-var_surfcntrcoord)*var_pixsize_Side); 
        end
        
        % Save boundary cooridinates of all points of drop
        cl_dropcoord{ii}=arr_boundcoords;

    %% Validation Plot
    if bl_ValidPlot==true
        var_imgsct=60;
        figure(1);
%         subplot(3,3,ii)
        imshow(img_original)
        xlim([arr_origin(1)-var_imgsct*2,arr_origin(1)+var_imgsct*2]);
        ylim([arr_origin(2)-var_imgsct,arr_origin(2)+var_imgsct]);
        hold on
        scatter(arr_origin(1),arr_origin(2),'*')
        scatter(arr_boundcoords(:,1),arr_boundcoords(:,2),'.');
        scatter(arr_saveerr(:,1),arr_saveerr(:,2),'+','r'); % Plot Errors
        scatter(arr_srftchpt(:,1),arr_srftchpt(:,2),'*')
        % scatter(stc_regprops_Side.Extrema(3,1),stc_regprops_Side.Extrema(3,2))
        % scatter(stc_regprops_Side.Extrema(4,1),stc_regprops_Side.Extrema(4,2))
        % scatter(stc_regprops_Side.Extrema(7,1),stc_regprops_Side.Extrema(7,2))
        % scatter(stc_regprops_Side.Extrema(8,1),stc_regprops_Side.Extrema(8,2))
        plot((0:2000),arr_coeff(1)*(0:2000)+arr_coeff(2));
        % scatter(arr_boundaries(:,2),arr_boundaries(:,1));
        % title(['BW-Threshold:', num2str(var_bw_thr)])
        hold off;
        title([num2str(arr_angles(ii)),'�'])
        
    end
    end
    %% Create 3D image
    arr_angles_rad=arr_angles*(2*pi/360); % Angles in rad


    % Create original height from Highest and Lowest angle views to get value
    % in center of the surface
    ind_mxmn=arr_angles==min(arr_angles) | arr_angles==max(arr_angles); % Get cell entries of highest and lowest angle views
    var_orighgt=mean(cellfun(@(x)max(x(:,2)),cl_dropcoord(ind_mxmn)))-mean(cellfun(@(x)min(x(:,2)),cl_dropcoord(ind_mxmn))); % Calculate mean height from each cell entry of highest and lowest angle views y-coordinates

    % Get all points into 3D-array
    arr_3dpoints=[];
    % arr_3dpoints=NaN(length(unique(cl_dropcoord{1}(:,2)))*var_nimg,3);

    for jj=1:var_nimg
        arr_2dcoords=cl_dropcoord{jj};
        arr_2dcoords_tip(1)=median(arr_2dcoords(arr_2dcoords(:,2)==min(arr_2dcoords(:,2)),1)); % Get x value of tip point of drop
        arr_2dcoords_tip(2)=min(arr_2dcoords(:,2)); % Get y value of tip
        arr_2dcoords=arr_2dcoords-arr_2dcoords_tip; % Set Tip point as origin for all curves
        arr_2dcoords=arr_2dcoords*(var_orighgt/max(arr_2dcoords(:,2))); % Scale points onto origin curve
        if arr_angles(jj)==0
    %         var_orighgt=max(arr_2dcoords(:,2)); % Get height of drop in origin cross section
    %         arr_inthght=unique(arr_2dcoords(:,2)); % Save heights for interpolation of other curves
              arr_inthght=linspace(min(arr_2dcoords(:,2)),max(arr_2dcoords(:,2)),200)';  
        end
    %         arr_2dcoords=arr_2dcoords*(var_orighgt/max(arr_2dcoords(:,2))); % Scale points onto origin curve

        %% Split coordinates in pos. and neg. x-Coordinates :
        % Interpolate Values for left side at arr_inthghts from piecewise interpolation
        arr_2dcoordsL=sortrows(arr_2dcoords(arr_2dcoords(:,1)<0,:),2); % Get all coords with neg. x-values
        breaks=arr_2dcoordsL(:,2); % Initalize y-values as breaks for pcw. int.
        slope=(arr_2dcoordsL(1:end-1,1)-arr_2dcoordsL(2:end,1))./(arr_2dcoordsL(1:end-1,2)-arr_2dcoordsL(2:end,2)); % Calculate linear slope in between points
        coeffs=[slope,arr_2dcoordsL((1:end-1),1)]; % Initalize x-values as coeffs for pcw. int. with   
        pp=mkpp(breaks,coeffs); % Calculate piecewise interpolation
        arr_2dcoords_intL=[ppval(pp,arr_inthght),arr_inthght]; % Save coordinates
        % Check for error points
        arr_dist=squareform(pdist(arr_2dcoords_intL)); % Create distance matrix
        arr_dist(arr_dist==0)=NaN; % Erase zeros on diagonal
        ind_err=~any(arr_dist<2,2); % Check for points further than 2 pixel away from every other point
        arr_2dcoords_intL(ind_err,:)=[]; % Erase point
        

        % Interpolate Values for right side at arr_inthghts from piecewise interpolation
        arr_2dcoordsR=sortrows(arr_2dcoords(arr_2dcoords(:,1)>0,:),2); % Get all coords with pos x-values and sort them monotonically
        breaks=arr_2dcoordsR(:,2); % Initalize y-values as breaks for pcw. int.
        slope=(arr_2dcoordsR(1:end-1,1)-arr_2dcoordsR(2:end,1))./(arr_2dcoordsR(1:end-1,2)-arr_2dcoordsR(2:end,2)); % Calculate linear slope in between points
        coeffs=[slope,arr_2dcoordsR((1:end-1),1)]; % Initalize x-values as coeffs for pcw. int. with   
        pp=mkpp(breaks,coeffs); % Calculate piecewise interpolation
        arr_2dcoords_intR=[ppval(pp,arr_inthght),arr_inthght]; % Calculate and save coordinates        
        % Check for error points
        arr_dist=squareform(pdist(arr_2dcoords_intR)); % Create distance matrix
        arr_dist(arr_dist==0)=NaN; % Erase zeros on diagonal
        ind_err=~any(arr_dist<2,2); % Check for points further than 2 pixel away from every other point
        arr_2dcoords_intR(ind_err,:)=[]; % Erase point
        
        
        
        
        arr_2dcoords=[arr_2dcoords_intL; arr_2dcoords_intR];

        arr_curr3dpts=[arr_2dcoords(:,1)*cos(arr_angles_rad(jj)),arr_2dcoords(:,1)*sin(arr_angles_rad(jj)),arr_2dcoords(:,2)];
        arr_3dpoints=[arr_3dpoints;arr_curr3dpts];

            %% Plot boundary curves 
%         figure(35);
%         hold on
%         scatter(arr_2dcoords(:,1),arr_2dcoords(:,2),'Displayname',num2str(arr_angles(jj)))
%         scatter(0,0,'*')
%         hold off
%         legend('-DynamicLegend')

        %% Plot boundary curves 3D
%         figure(3);
%         hold on
%         scatter3(arr_curr3dpts(:,1),arr_curr3dpts(:,2),arr_curr3dpts(:,3))
%         hold off
    end

    %% Fit an ellipse to every height of points   
    arr_heights=arr_inthght; % Heights at which ellipse is fitted
    theta_r = linspace(0,2*pi,360); % Angles at which points of fitted ellipse are saved 

    arr_3dellipse=[0 0 0]; % Add origin as tip

    for jj=1:length(arr_heights)

            curr_points=arr_3dpoints(abs(arr_heights(jj)-arr_3dpoints(:,3))<1,:); % Get all points in current height

            curr_points(isinf(curr_points))=NaN; % Turn infinity values to NaN
            [arr_illrows, ~] = find(isnan(curr_points)| isinf(curr_points)); %rows in curr_points that are inf or NaN
            curr_points_naninf=unique(arr_illrows); 
            curr_points(curr_points_naninf,:)=[]; %delete rows with inf or NaN

        if arr_heights(jj)~=0

            if length(curr_points)<5
                disp('Skipped ellipse fit at z=',num2str(arr_heights(jj)))
                continue
            end

            curr_ellipse=fit_ellipse(curr_points(:,1),curr_points(:,2)); % Fit an ellipse to all points from current height
%             bl_symmetry=curr_ellipse.X0/curr_ellipse.Y0>4 || curr_ellipse.X0/curr_ellipse.Y0<1/4; % Check if the ellipse is to unsymmetric to be valid
            
            if ~isempty(curr_ellipse) && ~strcmp(curr_ellipse.status,'Hyperbola found') && ~strcmp(curr_ellipse.status,'Parabola found')
                R = [ cos(curr_ellipse.phi) sin(curr_ellipse.phi); -sin(curr_ellipse.phi) cos(curr_ellipse.phi)];

                ellipse_x_r     = curr_ellipse.X0 + curr_ellipse.a*cos( theta_r );
                ellipse_y_r     = curr_ellipse.Y0 + curr_ellipse.b*sin( theta_r );
                arr_rotated_ellipse = (R * [ellipse_x_r;ellipse_y_r])';
                arr_curr3dellipse=[arr_rotated_ellipse,arr_heights(jj)*ones(length(arr_rotated_ellipse),1)];

                arr_3dellipse=[arr_3dellipse;arr_curr3dellipse];
            end

        else
            arr_3dellipse=[arr_3dellipse;curr_points];
        end
        
%            figure();
%            scatter(curr_points(:,1),curr_points(:,2))
%            hold on
%            plot(arr_rotated_ellipse(:,1),arr_rotated_ellipse(:,2))
%            hold off

    end

    arr_3dellipse(:,3)=abs(arr_3dellipse(:,3)-max(arr_3dellipse(:,3))); %Flip drop upside down for plotting
    arr_3dpoints(:,3)=abs(arr_3dpoints(:,3)-max(arr_3dpoints(:,3))); %Flip drop upside down for plotting

    %% Calculate Volume
    arr_3dellipse_sc=arr_3dellipse*var_pixsize_Side; % Scale coordinates with pixsize
    [arr_3dsurfpt,var_dropvolume]=convhull(arr_3dellipse_sc(:,1),arr_3dellipse_sc(:,2),arr_3dellipse_sc(:,3));
    disp(['Der Tropfen hat ein Volumen von ',num2str(var_dropvolume),' m^3'])

    %% Plot Reconstruction
    % Choose saving location for plots
    %SavingLocation='D:\Promotion\Dissertation\Dissertation_Manuskript\Chapters\3-SetupMaterials\figures\';
    SavingLocation='D:\Promotion\Dissertation\Figure_Rawfiles\Alt\';
    % SavingLocation=Location;
    if bl_ValidPlot==true
        if bl_overview==true
            figure(1);
            subplot(3,3,8);
        else
            fig1=figure();
        end
        
        
        scatter3(arr_3dpoints(:,1),arr_3dpoints(:,2),arr_3dpoints(:,3),'r');
        
      
        axis equal
        view(-45,15)
        
        xlabel('X/px')
        ylabel('Y/px')
        zlabel('Z/px')
        % Set Figure Font Size
        set(gca,'FontSize',36)
        % Set Font
        fig5ax=gca;
        fig5ax.FontName='Charter';

        % Set Figure to fullscreen for saving
        set(gcf,'units','normalized','outerposition',[0 0 1 1])
        set(gcf,'color','w');
        
         print([Location,'DropContours'],'-dpng');
        
        

        hold on
        scatter3(arr_3dellipse(:,1),arr_3dellipse(:,2),arr_3dellipse(:,3),'k','.')
        hold off
        
        % Save figure      
        if bl_saveplts==true
            SaveName='DropPointCloud';
        % print(['D:\Promotion\Media\Texte\SFB-Berichte\SFB TRR 75 - Abschlussbericht\figures\','DropPointCloud'],'-dpng');
%         matlab2tikz([SavingLocation,SaveName,'_tikz.tex'],'showInfo',false,'height','0.24\textwidth,','width','0.4\textwidth') 
        print([SavingLocation,'DropPointCloud'],'-dpng'); 
        end
    end
    %% Plot Drop Reconstruction
    if bl_ValidPlot==true
        if bl_overview==true % Check if plot needs to be put into subplot
            figure(1);
            subplot(3,3,9)
        else
            fig2=figure();
        end
        
        
%         surf_res=100;
%         xlin = linspace(min(arr_3dellipse_sc(:,1)),max(arr_3dellipse_sc(:,1)),surf_res);
%         ylin = linspace(min(arr_3dellipse_sc(:,2)),max(arr_3dellipse_sc(:,2)),surf_res);
%         [X,Y] = meshgrid(xlin,ylin);
% 
%         
%         [theta,r]=cart2pol(arr_3dellipse_sc(:,1),arr_3dellipse_sc(:,2));
%         thetalin = linspace(min(theta),max(theta),surf_res);
%         rlin = linspace(min(r),max(r),surf_res);
%         [Theta_mesh,R_mesh]=meshgrid(thetalin,rlin);       
%         [X,Y]=pol2cart(Theta_mesh,R_mesh);
%         
%         
%         f = scatteredInterpolant(arr_3dellipse_sc(:,1),arr_3dellipse_sc(:,2),arr_3dellipse_sc(:,3));
%         Z = f(X,Y);
% 
%         Z(Z<0)=0;
%         
%         s=surf(X,Y,Z,'FaceAlpha',0.8);
        trisurf(arr_3dsurfpt,arr_3dellipse_sc(:,1),arr_3dellipse_sc(:,2),arr_3dellipse_sc(:,3))
           

        axis equal
%         zlim([0.00001 Inf])    
        view(-45,15)
        xlabel('X/m')
        ylabel('Y/m')
        zlabel('Z/m')
        % Set Figure Font Size
        set(gca,'FontSize',36)
        % Set Font
        fig5ax=gca;
        fig5ax.FontName='Charter';

        % Set Figure to fullscreen for saving
        set(gcf,'units','normalized','outerposition',[0 0 1 1])
        set(gcf,'color','w');

        % Save figure
        % print(['D:\Promotion\Media\Texte\SFB-Berichte\SFB TRR 75 - Abschlussbericht\figures\','DropTriangulated'],'-dpng');
        % print([Location,'DropTriangulated'],'-dpng');   
    
    
        if bl_saveplts==true
            if bl_overview==true
                SaveName='EstimationOverview';
            else
                SaveName='DropTriangulation';
            end
%             matlab2tikz([SavingLocation,SaveName,'_tikz.tex'],'showInfo',false,'height','0.24\textwidth,','width','0.4\textwidth') 
            print([SavingLocation,SaveName],'-dpng');   % Save Overview figure
%             saveas(fig2,[SavingLocation,SaveName,'.pdf'])
        end
    
    end
    
end