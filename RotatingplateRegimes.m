% Calculate Weber and Reynolds number regimes 

% D_0=(50:100:2500)*1e-6; % Drop Sizes in m
D_0=[10,25,50,75,150,250,500,1000,2000]*1e-6;  % Drop Sizes in m
T_0=20; % Drop Temperature in �C
U_0=20:250; % Plate Velocity in m/s

rho=water_props(T_0,'rho');
nu=water_props(T_0,'nu');
sigma=water_props(T_0,'sigma');

rho_air=air_props(T_0,'rho');


fig1=figure();
hold on 

for ii=1:length(D_0)

Re=U_0.*D_0(ii)./nu;
We=rho.*U_0.^2*D_0(ii)./sigma;

We_air=rho_air.*U_0.^2.*D_0(ii)./sigma;

%% Plots
plot(Re,We)
text(Re(end)*1.07,We(end)*1.2,['$$ D_0 = $$ ',num2str(D_0(ii)*1e6),'$$ \mu m $$'],'rotation',41,'Fontname','Charter','FontSize',18,'interpreter','latex')

end


ylim([-Inf 5e6])

set(gca,'xscale','log')
set(gca,'yscale','log')

xlabel('Re');
ylabel('We');

fig1=plotprops(fig1,'BigFont');

grid on
grid minor

hold off