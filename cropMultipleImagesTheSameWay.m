inputpath='C:\Users\ui16ryno\Desktop\Promotion\Media\Texte\ICMF 2019\images\';
[examplefilename, examplepathname] = uigetfile( [inputpath '*.tif'], 'Select an image to define ROI');

D=dir([examplepathname, '*.tif']);
numImages = length(D(not([D.isdir])));

files = cell(1, numImages);
for i = 1:numImages
    files{i} = fullfile(examplepathname, D(i).name);
end


refframe = imread(char(files(1,1))) ;
[refframe, rect]=imcrop(refframe);


for i = 1:numImages
    im = imread(char(files(1,i))) ;
    im = imcrop(im, rect);
    filename=[inputpath,D(i).name'];
    imwrite(im,filename)
end


cropped=imcrop(frame,rect);
iwmrite(cropped,[fullfile(examplepathname,filename(1:size(filename,2)-4)),'_cropped.tif']);
