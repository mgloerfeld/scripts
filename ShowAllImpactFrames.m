%% Video Folder Location (Adjust for different video file location) ***Add \ at the end of Location***
% Location='D:\Promotion\Nebenprojekte\NANNY\Tropfengenerator MIT\V001_2_2500rpm\';
% Location='D:\Promotion\Nebenprojekte\NANNY\Tropfengenerator MIT\V002_0_2_2500rpm\';
% Location='E:\NANNY\20221207_2\V022_0_6mm_20ms\';
Location='F:\NANNY\Experiments\20221207\';
      
%% Get all videos found in Location
    DataFiles = dir([Location '**\*ImpactFrame*']);
    FileNames = {DataFiles.name};
    FileFolder={DataFiles.folder};
    % Check for marked unusable videos and remove from postprocessing
    log_unusables=~(contains(FileNames,'NOTUSABLE') | contains(FileNames,'stamped') | contains(FileNames,'spin') | contains(FileNames,'fps') |  contains(FileNames,'Calibration')); % Search for marker 'NOTUSABLE' in Filename
    FileNames =FileNames(log_unusables);
    FileFolder=FileFolder(log_unusables);
    % Get number of videos
    numvids=size(FileNames,2);
    if numvids==0
        disp('No Videos found!Check location folder')
        return
    end
    
    for ii=1:length(FileNames)
        img_curr=imread([FileFolder{ii},'\',FileNames{ii}]);
        
        figure();
        imshow(img_curr)
        title(FileNames{ii})
        
        
        
        
        
    end
    