bl_glyc=true;
if bl_glyc==true
    drop_rho=1131; % kg/(m^3)
    drop_eta=6.91e-3; % Pa s
    drop_sigma=61.02e-3; % N/m
else
    drop_rho=water_props(20,'rho');
    drop_eta=water_props(20,'eta');
    drop_sigma=water_props(20,'sigma');
end    
    
drop_d=0.002; %m
drop_nu=drop_eta/drop_rho;
Re=1500;
% Velocity
drop_v=Re*drop_nu/drop_d;
% Webernumber
We=drop_rho*drop_v^2*drop_d/drop_sigma;

disp(We)