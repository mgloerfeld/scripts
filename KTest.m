%% Specify initial conditions:

%Initial conditions for a time slightly after the moment of contact:
y0 = [0 ;4/3; 2];

%Create an option set that specifies numerical tolerances for the numerical search.
%Increased both tolerances to 10^-9

opt = odeset('RelTol', 10.0^(-9),'AbsTol',10.0^(-9));

%% Solve the Equation system
%define the start and end value of "zeta" for the integration
tspan=[0.001 3.1];
%solve the equation system with solver for stiff problems ode15s
[t,y] = ode15s(@(t,y) odefcn2(t,y), tspan, y0,opt);

%% Calculate fit for K
K=y(:,2);
coeff_K=polyfit(t,K,8);


%% Plot
cfig=figure(1);
subplot(1,3,1)
plot(t,a)
xlabel('\zeta')
ylabel('a')

subplot(1,3,2)
plot(t,K)
hold on
plot(t,polyval(coeff_K,t))
hold off
xlabel('\zeta')
ylabel('K')

subplot(1,3,3)
plot(t,h)
xlabel('\zeta')
ylabel('h')


cfig.Position=[13 331.4000 1.5208e+03 430.6000];