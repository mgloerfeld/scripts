% Plot development of boundary layer thickness in test section and equal
% cross sectional area

%% Air Properties
rho_air=1.3245;
nu_air=126.2e-7;
U_air=80;

%% Plot Boundary Layer Development
X=0:0.0001:4;
Y=5*sqrt(nu_air*X/U_air);

figure();
plot(X,Y)
title(['Boundary Layer Thickness for U_{Air}=' num2str(U_air)])
xlabel('Distance [m]')
ylabel('\delta_{BL} [m]')


