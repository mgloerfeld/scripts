%% Estimate timescales of freezing etc.

T=-10; % Temp in �C
D_0=0.0005; % Diam in m
rho=water_props(T,'rho');
sigma=water_props(T,'sigma');
nu=water_props(T,'nu');
v_f=DendriteFrontVelocity(T); % Dendrite front velocity in m/s
U_0=3; % Impact Vel

% Timescale of lamella freezing
t_frz=D_0./(U_0.^(2/3).*v_f.^(1/3));
% Timescale of impact
t_imp=10*D_0/U_0;
% Timescale of surface tension (receding)
t_rec=D_0.^(3/2).*rho.^(1/2)./sigma.^(1/2);
% Timescale of viscous forces
t_visc=(U_0.*D_0./nu).^(1/5).*(D_0/U_0);

% Compare
Bal1=t_frz/t_imp;

Bal2=t_frz/t_rec;

Bal3=t_frz/t_visc;

Bal3_alt=nu^(1/5)*U_0^(2/15)/(v_f^(1/3).*D_0^(1/5));
