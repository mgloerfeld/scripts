function [arr_Uloc,arr_DPloc,arr_Urloc,arr_Uzloc]=AirFlowProps(r,z,var_InpAirVel)
% AIRFLOWPROPS Function to get air flow velocity and pressure field of numeric simulation of stagnation point flow on the surface


%           Input:  
%   ----------------------------------------------
%           r: coordinate of desired point in [m]
%           z: coordinate of desired point in [m]
%           var_Airvel: Bulk air velocity of flow field

%           Output:  
%   ----------------------------------------------
%           arr_Uloc: local velocity at given point in [m/s]
%           arr_Urloc: local radial velocity at given point in [m/s]
%           arr_Uzloc: local vertical velocity at given point in [m/s]
%           arr_Ploc: local pressure difference at given point in [Pa]        


% Scale input coordinates (simulation data is nondimnesional)
var_Dtrgt=0.03; % Target radius in [m]
r=r/var_Dtrgt;
z=z/var_Dtrgt;

r_sign=sign(r);
r=abs(r);

% Check if input range is valid
if any(abs(r) > 1.5) || any(z > 1.5) || any(z < -1)
    disp('Input coordinates outside of simulated range! Check again!')
    return
end

if (var_InpAirVel>0 && var_InpAirVel<5) || (var_InpAirVel > 25 && var_InpAirVel < 30) 
        disp('Input Velocities outside of simulated range! Values will be extrapolated! Interpret with caution!')
elseif var_InpAirVel > 30
    disp('Input Velocities too far outside of simulated range! Choose other velocity!')
    return
elseif var_InpAirVel==0 % If velocity is 0, local velocity and pressure is 0
    arr_Uloc=zeros(length(r),1);
    arr_Urloc=zeros(length(r),1);
    arr_Uzloc=zeros(length(r),1);
    arr_DPloc=zeros(length(r),1);
    
    return
end

if length(r)~=length(z)
    disp('Input coordinates have to be pairs, i.e. r & z have to be same size!')
    return
end


% Check if Data for required velocity exists (otherwise chose two nearest
% for interpolation)
arr_availData=[5,10,15,20,25]';
if ~ismember(var_InpAirVel,arr_availData)
    % Get two nearest velocities for interpolation
   [~,ind_avldata]=sort(abs(arr_availData-var_InpAirVel)); 
   arr_AirVel=arr_availData(ind_avldata(1:2));
   bl_interpolData=true; % Set flag for interpolation
else
   arr_AirVel=var_InpAirVel;
   bl_interpolData=false;
end

% Preallocation
cl_Uloc=cell(length(arr_AirVel),1);
cl_Urloc=cell(length(arr_AirVel),1);
cl_Uzloc=cell(length(arr_AirVel),1);
cl_DPloc=cell(length(arr_AirVel),1);
% Location of files
Location='F:\Experiments\TargetFlowSimulation';


for ii=1:length(arr_AirVel)
% Load data for specicic velocity
Data=load([Location,'/',num2str(arr_AirVel(ii)),'ms/02_targetImp_kOmegaSST_',num2str(arr_AirVel(ii)),'ms/targetImp_data.mat']);

% Create gridded interpolant of upper half (leave out NaN region)
% ind_surf=find(Data.Z(:,1)==0); % Find index where surface begins (z=0)
% arr_Y=Data.Ystar(ind_surf:end,:); % Y-Data
% arr_Z=Data.Z(ind_surf:end,:); % Z-Data
% arr_U=Data.UGrid(ind_surf:end,:); % U-Data
% arr_DP=Data.CpGrid(ind_surf:end,:); % Delta P-Data

% Calculate gridded interpolant for U
fun_Uint=griddedInterpolant(Data.Ystar',Data.Zstar',Data.UGrid');
% Caluclate velocity at specified point(s)
cl_Uloc{ii}=fun_Uint(r,z)*arr_AirVel(ii);

% Calculate gridded interpolant for u_z and u_w
fun_Urint=griddedInterpolant(Data.Ystar',Data.Zstar',Data.vStar');
% Caluclate velocity at specified point(s)
cl_Urloc{ii}=fun_Urint(r,z)*arr_AirVel(ii);
fun_Uzint=griddedInterpolant(Data.Ystar',Data.Zstar',Data.wStar');
% Caluclate velocity at specified point(s)
cl_Uzloc{ii}=fun_Uzint(r,z)*arr_AirVel(ii);


% Calculate gridded interpolant for P
fun_DPint=griddedInterpolant(Data.Ystar',Data.Zstar',Data.CpGrid');
% Caluclate pressure diff at specified point(s)
% cl_DPloc{ii}=(fun_DPint(r,z)+1)*(0.5*air_props(-10,'rho')*arr_AirVel(ii)^2);  % Use density at -10�C for estimation 
cl_DPloc{ii}=(fun_DPint(r,z)/air_props(-10,'rho')+1)*(0.5*air_props(-10,'rho')*arr_AirVel(ii)^2);  % Use density at -10�C for estimation 


end

if bl_interpolData==true
    % Calculate interpolated values (has to work for arrays as well) 
   arr_Uloc=(cl_Uloc{2}-cl_Uloc{1})./(arr_AirVel(2)-arr_AirVel(1))*(var_InpAirVel-arr_AirVel(1))+cl_Uloc{1};   
   arr_Urloc=(cl_Urloc{2}-cl_Urloc{1})./(arr_AirVel(2)-arr_AirVel(1))*(var_InpAirVel-arr_AirVel(1))+cl_Urloc{1};
   arr_Uzloc=(cl_Uzloc{2}-cl_Uzloc{1})./(arr_AirVel(2)-arr_AirVel(1))*(var_InpAirVel-arr_AirVel(1))+cl_Uzloc{1};
   arr_DPloc=(cl_DPloc{2}-cl_DPloc{1})./(arr_AirVel(2)-arr_AirVel(1))*(var_InpAirVel-arr_AirVel(1))+cl_DPloc{1}; 

else
    % Change cell array to double array output
    arr_Uloc=cell2mat(cl_Uloc);
    arr_DPloc=cell2mat(cl_DPloc);
    arr_Urloc=cell2mat(cl_Urloc);
    arr_Uzloc=cell2mat(cl_Uzloc);
    
end

   arr_Urloc=arr_Urloc.*r_sign; % Adjust direction of r according to sign

end