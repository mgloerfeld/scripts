function [cfig,lghndl]=plotprops(cfig,varargin)
% Manipulate axis and figure properties to make nice plots and graphs
%           Input: cax - Axis of current plot
%           Output: cax - Manipulated axis

bl_skip=false;
bl_BF=false;
bl_MF=false;
bl_cstmrksz=false;
bl_cstlg=false;
bl_tikz=false;


for ii=1:length(varargin)
    
    if bl_skip==true
        bl_skip=false;
        continue
    end
    
    switch varargin{ii}
        case 'BigFont'
            bl_BF=true;
        case 'MediumFont'
            bl_MF=true;
        case 'KeepMarkerSize'
            bl_cstmrksz=true;
        case 'CustomLegend'
            bl_cstlg=true;
            lghndl=varargin{ii+1};
            bl_skip=true;
        case 'Tikz'
            bl_tikz=true;
    end

end


cax=cfig.CurrentAxes; % Extract current axes from figure
%% Colors
cfig.Color=[1 1 1]; % Turn Background white

%% Plot Size
if bl_tikz==false
    cfig.Units='normalized'; % Change Units to normalized
    cfig.OuterPosition=[0 0 0.99 0.99]; % Make Figure Full Screen
else
    cfig.ToolBar='none';
end


% Change legend marker size
if bl_cstlg==true
    grpobj=findobj(lghndl,'Type','hggroup'); % Get 
    ind_ch=ismember(lghndl,grpobj);
    mrkobj=findobj(lghndl,'-property','Markersize'); % Get 
    set(mrkobj,'MarkerSize',34)
    lghndl(ind_ch)=mrkobj;
end



%% Font Name and Sizes


if bl_BF==false && bl_MF==false
    var_fntsz=18; % Font Size 
elseif bl_BF==true
    var_fntsz=34; % Font Size
elseif bl_MF==true
    var_fntsz=28;
end

cax.FontName='Charter'; % Change Font
cax.FontSize=var_fntsz;
cax.LineWidth=2;
% set(lghndl(~ind_ch),'FontName','Charter')
% set(lghndl(~ind_ch),'FontSize',var_fntsz/1.1)

%% Number representations

% cax.XAxis.Exponent=3; % Implement scientific number notation

%% Set MarkerSize
    var_markersize=4*cax.FontSize;
    for ii=1:length(cax.Children)
        if bl_cstmrksz==false
            if strcmp(cax.Children(ii).Type,'line')
                cax.Children(ii).MarkerSize=var_markersize;
                cax.Children(ii).MarkerEdgeColor='k';
                cax.Children(ii).LineWidth=2;
            end
            if strcmp(cax.Children(ii).Type,'scatter')
                cax.Children(ii).SizeData=var_markersize;
                cax.Children(ii).MarkerEdgeColor='k';
                cax.Children(ii).LineWidth=1.5;
            end
            if strcmp(cax.Children(ii).Type,'constantline')
                cax.Children(ii).LineWidth=2.5;
            end
            
            
        else
            cax.Children(ii).MarkerEdgeColor='k';
        end
        
    end

cfig.CurrentAxes=cax; 
    


% set(cfig,'DefaultTextInterpreter','latex')
cfig.Units='pixels';
end 
