% Plot properties of Air

arr_temp=-30:10; % Temperature in �C
cl_props={'rho','eta','sigma','cp','nu','lambda','thdiff'}; % Properties to plot
p=6;
arr_yval=air_props(arr_temp,cl_props{p});


fig1=figure();
% Plot 2
ind_avldata=arr_temp>=-25;

plot(arr_temp(ind_avldata),arr_yval(ind_avldata),'k')
% hold on
% plot(arr_temp(~ind_avldata),arr_yval(~ind_avldata),'--k')
% hold off

xlabel('$$ T/ ^\circ \mathrm{C} $$','interpreter','latex')

% ylabel('$$ \rho /(\mathrm{kg\,m}^{-3}) $$','interpreter','latex')
% ylabel('$$ \eta /(\mathrm{N\,s\,m}^{-2}) $$','interpreter','latex')
% ylabel('$$ c_p/(\mathrm{J\,kg}^{-1}\mathrm{\,K}^{-1}) $$','interpreter','latex')
% ylabel('$$ \sigma/(\mathrm{N\,m}^{-1}) $$','interpreter','latex')
ylabel('$$ \lambda /(\mathrm{W}\, \mathrm{m}^{-1} \mathrm{K}^{-1}) $$','interpreter','latex')

fig1=plotprops(fig1,'MediumFont');
% Plot 2
% arr_yval=water_props(arr_temp,cl_props{2});
% sb2=subplot(1,2,2);
% plot(arr_temp,arr_yval,'k')
% 
% ylabel('$$ \eta /(\mathrm{N\,s\,m}^{-2}) $$','interpreter','latex')
% 
% 
% fig1=plotprops(fig1);

SaveLocation='D:\Promotion\Dissertation\Verteidigung\';
SaveName=['AirProps_',cl_props{p}];

print([SaveLocation,SaveName],'-dpng')

% matlab2tikz([SaveLocation,SaveName,'_tikz.tex'],'showInfo',false,'height','0.5\textwidth,','width','0.7\textwidth')
% 

