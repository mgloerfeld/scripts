% Calculate Deformation Velocity of a droplet

d_drop=1e-3; %[m]
rho_w=998.13; % [kg/m^3]
% rho_fluid=798; % [kg/m^3]
u_drop=0:80; %[m/s]
sigma_w=75.65e-3; %[N/m]
% sigma_eth=22.55e-3; %[N/m]
mu_w=2644.2e-6; % [kg/ms]
% mu_fluid=1.2e-3; % [kg/ms]

m_drop=(4/3)*pi*(0.5*d_drop)^3*rho_w; %[kg]
A_drop=pi*(0.5*d_drop)^2; % [m^2]
rho_air=1.3245; % [kg/m^3]
nu_air=126.2e-7; % [m^2/s]
g=9.81; % [m/s^2]
u_air=0; %[m/s]
v_rel=abs(u_air-u_drop); %[m/s]

%% Dimensionless numbers
We_d=(rho_air.*d_drop.*v_rel.^2.)/sigma_w; % Weber number
We_d_w=(rho_w.*d_drop.*v_rel.^2.)/sigma_w;
Re_ad=u_air.*d_drop./nu_air; % Reynolds number
Oh_d=mu_w/sqrt(d_drop*rho_w*sigma_w); % Ohnesorg number

%% Deformation Velocity
V_def=v_rel*sqrt(rho_air)/sqrt(rho_fluid);


figure();
plot(u_air,V_def)
title('Vdef')

figure();
plot(u_drop,We_d)
title('We_d')
ylim([0 50]);
xlim([0 30])
grid on



