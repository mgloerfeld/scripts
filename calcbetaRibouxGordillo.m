% Calculate the parameter beta after Riboux and Gordillo (2014)

stc_Data=Data_Burzynski.WaterPrompt;

arr_D=stc_Data.D;
arr_U=stc_Data.U;
arr_eta_l=stc_Data.mu_l;
arr_sigma=stc_Data.sigma;
arr_rho_l=stc_Data.rho_l;
arr_rho_g=stc_Data.rho_g;
arr_eta_g=stc_Data.mu_g;
arr_lambda_g=stc_Data.lambda_g;

arr_ReNum_R=arr_rho_l.*(0.5*arr_D).*arr_U./arr_eta_l;
arr_WebNum_R=arr_rho_l.*arr_U.^2.*(0.5*arr_D)./arr_sigma;
arr_OSNum_R=sqrt(arr_WebNum_R)./arr_ReNum_R;


arr_te=NaN(length(arr_D),1);
syms t_e
for ll=1:length(arr_D)
% Determine t,e_crit
var_tecurr=vpasolve((arr_ReNum_R(ll)^(-2))*(arr_OSNum_R(ll)^(-2))==(1.1)^2*(t_e^(3/2))-(sqrt(3)/2)*(arr_ReNum_R(ll)^(-1))*t_e^(-1/2),t_e,[0 1]);
arr_te(ll)=double(var_tecurr);
end

V_t=(sqrt(3)/2).*arr_U.*arr_te.^(-1/2);
H_t=(0.5.*arr_D).*sqrt(12).*arr_te.^(3/2)./pi;
% H_t=(0.5.*arr_D).*sqrt(12).*arr_te.^(3/2)./pi; % Without 2.8 facor (Burzynski 2020)

K_u=0.3;
alpha=deg2rad(60); % Wegde angle (probably has to be ADJUSTED)
% Calculate beta
K_l=-(6./(tan(alpha).^2)).*(log((19.2).*arr_lambda_g./H_t)-log(1+((19.2).*arr_lambda_g./H_t)));
beta_recalc=((K_l.*arr_eta_g.*V_t+K_u.*arr_rho_g.*(V_t.^2).*H_t)./(2*arr_sigma)).^(1/2);

Data_Burzynski.WaterPrompt.betarecalc=beta_recalc;


error=beta_recalc./Data_Burzynski.WaterPrompt.beta;

