function [value, isterminal, direction] = myEvent(t, oput)
value      = (oput(3) < 0);
isterminal = 1;   % Stop the integration
direction  = 0;