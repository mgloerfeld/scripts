function [ p_d] = secondode( t, p )
%% simultaneous second order differentials for projectile
% motion with air resistance 
% output vector p has the four differential outputs
% assumed units: meters, seconds, Newtons, kg, radians
global C D0 g nu_air U_inf% these are defined globally so they can be changed

% outside the function - means this function doesn't need editing
% for different coordinate systems
%evalin('base','hess')
p_d = zeros(4,1); % initialize output space
p_d(1) = p(2); % p_d(1) is the differential of x, which is x_dot, which is p(2)
u_lx=0;
u_ly=U_inf;
v=sqrt((u_lx-p(2))^2 + (u_ly-p(4))^2); %Relative velocity between drop and air
if v==0
v=1e-6;
end
Re=D0*v/nu_air ;
Cd=(24./Re.*(1+0.15.*Re.^0.687))+0.42./(1+(4250./Re.^1.16)); %Drag

p_d(2) = Cd*C*v*(u_lx-p(2)); % from the function
p_d(3) = p(4); % same idea as p(1), but for y
p_d(4) = -g+Cd*C*v*(u_ly-p(4)); % from the function
end
