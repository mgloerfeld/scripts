function [var_deltaT,arr_Qdot]=DropTemperature(var_DropDiam,var_DropSpeed,var_AirVel,var_DropTemp,var_AirTemp)
    global C D0 g nu_air U_inf% these are defined globally so they can be changed

    %% Input
    % D0=3e-3; %Tropfengr��e in m
    % U_inf=-25; % Kanalgeschwindigkeit in Koordinate-z (negativ beim Tropfenfall)
    % U0=-2; %Startgeschwindigkeit Tropfen in Koordinate-z (negativ beim Tropfenfall)
    % T_inf=-14.5; %Temperatur Kanal
    % T_drop=-13; %Initiale Temperatur Tropfen


    D0=var_DropDiam; %Tropfengr��e in m
    U_inf=-var_AirVel; % Kanalgeschwindigkeit in Koordinate-z (negativ beim Tropfenfall)
    U0=-2; %Startgeschwindigkeit Tropfen in Koordinate-z (negativ beim Tropfenfall)
    U_end=-var_DropSpeed;
    T_inf=var_AirTemp; %Temperatur Kanal
    T_drop=-0; %Initiale Temperatur Tropfen (dendritic drop = 0�)

    

    fall=0.7; %Fallh�he des Tropfens in Meter
    bl_plot=false; % Define if plots are made

    %% Pre-Processing
        t_max=fall/-U0; %Max. Fallzeit des Tropfens mit mittlerer geschwindigkeit
    
%     t_max=fall/(-0.5*(U0+U_end)); %Max. Fallzeit des Tropfens mit mittlerer geschwindigkeit
    
    

    g=9.81; % acceleration due to gravity constant in negative z-Richtung
    nu_air=air_props(T_drop,'nu'); % kin. viscosity of air at T=-10�C, m^2/s
    rho_water=water_props(T_drop,'rho');
    rho_air=air_props(T_drop,'rho'); % density of air at T=-10�C, kg/m^3 
    m_drop=pi/6*D0^3*rho_water;
    A=pi*D0^2/4; % silhouette area, m^2
    C=A*rho_air/(2*m_drop); % the drag force constant

    %% Berechnung der relativen Geschwindigkeit des fallenden Tropfens
    tspan = [0 t_max];
    % initial conditions as [x0, vx0, y0, vy0]
    IC = [0; 0 ; fall; U0]; 
    Opt=odeset('MaxStep',t_max/100,'Events', @myEvent);
    [t, oput] = ode45(@secondode, tspan, IC, Opt); % Runge-Kutta to solve
    x= oput(:,1); % extract x-position from 1st column
    vx= oput(:,2); % extract x-velocity from 2nd column
    z= oput(:,3); % extract y-position from 3rd column
    Uz= oput(:,4); % extract y-velocity from 4th column
    Urel=abs(U_inf-Uz);

    %% Preallocation
    Tm=NaN(length(t)-1,1);
    Nu_abs=NaN(length(t)-1,1);
    alpha=NaN(length(t)-1,1);
    Q_dot=NaN(length(t)-1,1);
    T_drop=[T_drop;NaN(length(t)-2,1)];

    %% Berechnung der Abk�hlung des fallenden Tropfens
    for ii=1:1:length(t)-1
    dt=t(ii+1)-t(ii);
    % Stoffgr��en f�r Luft
    Tm(ii)=(T_inf+T_drop(ii))/2;
    nu_air=air_props(Tm(ii),'nu'); %kin. Viskosit�t Luft @Tm(i)
    lambda_air=air_props(Tm(ii),'lambda'); %w�rmeleitung Luft @Tm(i)
    Pr=air_props(Tm(ii),'Pr'); %Prandtl-Zahl Luft @Tm(i)

    % Stoffgr��en Wasser
    rho_water=water_props(T_drop(ii),'rho');
    cp_water=water_props(T_drop(ii),'cp');

    % Dimensionslose Gr��en
    Re=Urel(ii)*D0/nu_air; %Air Reynolds-Zahl
    Gr=(T_drop(ii)-T_inf)/(T_drop(ii)+273.15)*((9.81*D0^3)/(nu_air^2)); %Grashof-Zahl
    Ra=Gr*Pr; %Rayleight-Zahl
    Re_frei=sqrt(0.4*Gr);
    Re_res=sqrt(Re^2+Re_frei^2);

    % W�rme�bertragung nach TTD
    Nu_frei=0.56*((Pr/(0.846+Pr))*Ra)^0.25+2;
    Nu_lam=0.664*Re^(1/2)*Pr^(1/3);
    Nu_turb=(0.037*Re^(0.8)*Pr)/(1+2.443*Re^(-0.1)*(Pr^(2/3)-1));
    Nu_Kugel=2+sqrt(Nu_lam^2+Nu_turb^2);
    Nu_abs(ii)=(Nu_Kugel^3+Nu_frei^3)^(1/3);

    % % W�rme�bertragung nach KIT
    % Nu_lam2=(2/(1+22*Pr))^(1/6)*sqrt(Re_Res*Pr);
    % Nu_turb2=(0.037*Re_Res^(0.8)*Pr)/(1+2.443*Re_Res^(-0.1)*(Pr^(2/3)-1));
    % Nu_abs2=2+sqrt(Nu_lam2^2+Nu_turb2^2);

    alpha(ii)=Nu_abs(ii)*lambda_air/D0;
    Q_dot(ii)=alpha(ii)*(pi*D0^2)*(T_drop(ii)-T_inf); %W�rmestrom vom Tropfen auf Luft
    Q=Q_dot(ii)*dt;

    % Abk�hlung Tropfen (Upper-Bound)
    DT=Q/(m_drop*cp_water);
    T_drop(ii+1)= T_drop(ii)-DT;
   
    
    
    end

    DT_total=(T_drop(end)-T_drop(1)); %Totale Abk�hlung Tropfen
    disp(['Abk�hlung liegt bei ' num2str(DT_total) '�C'])
    
       
    var_deltaT=DT_total;
    arr_Qdot=Q_dot;

    if bl_plot==true % Check if plots should be created
    %% Plot fall height over temperature decrease
    figure(1)
    set(gcf,'units','centimeters','position',[2,2,16,12])

    plot(T_drop,z,'r','LineWidth',1.2)

    set(gca,'FontSize',12,'FontName','helvetica','LineWidth',1.10,'TickLabelInterpreter','latex')
    ylabel('1')
    xlabel('2')
    set(get(gca,'xlabel'),'String','$T_\mathrm{drop}~[^\circ\mathrm{C}]$','FontSize',14,'fontname','helvetica','interpreter','latex')
    set(get(gca,'ylabel'),'String','$z$ [m]','FontSize',14,'fontname','helvetica','interpreter','latex')

    %% Plot drop temperature over time
    figure(2)
    set(gcf,'units','centimeters','position',[2,2,16,12])

    plot(t,T_drop,'r','LineWidth',1.2)

    set(gca,'FontSize',12,'FontName','helvetica','LineWidth',1.10,'TickLabelInterpreter','latex')
    ylabel('1')
    xlabel('2')
    set(get(gca,'ylabel'),'String','$T_\mathrm{drop}~[^\circ\mathrm{C}]$','FontSize',14,'fontname','helvetica','interpreter','latex')
    set(get(gca,'xlabel'),'String','$t$ [s]','FontSize',14,'fontname','helvetica','interpreter','latex')
    end


end