% Plot Variation of Yield Strength


var_U=3.5;
arr_Y=1000:1000:7000;
var_icefr=0.1;
var_R0=0.003;



fig1=figure(1);
hold on


for ii=1:length(arr_Y)
    
    [~,~,~,~,~,~,~,modh_,modt_,modU_]=PlasticFlowDendriticImpact(var_U,arr_Y(ii),var_icefr,'YieldStrengthVelocityDependence',var_R0);
    
    modh_=[modh_;modh_(end)];
    modt_=[modt_;10];
    
    plot(modt_,modh_,'--')
    
end

 hold off

xlim([0 10])

xlabel('$$ \overline{t} $$','interpreter','latex')
ylabel('$$ h/R_0 $$','interpreter','latex')

fig1=plotprops(fig1); % Change Figure Setting to nice figure