function [f]=Frictionfactor(Re_Ch)% Calculate Friction Factor for given Reynoldsnumber in test section

f=1;
for ii=1:10
    fnew=(2*log10(Re_Ch*sqrt(f)))^-2;
    
    if abs(f-fnew)<=0.0001
        break
    else
        f=fnew;
    end  
end

end