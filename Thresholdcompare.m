% Compare Film and Aerodynamik threshold at different temperatures

R=8.3145; % Universal gas constant [J/(mol K)]
kappa=1.402; % Isentropic exponent air
M=0.02896; % Average Molmass air [kg/mol] 

Saero=0.9;          % Threhsold value for aerodynamic splash
Sfilm=3.45e4;       % Threshold value for film splash

DT=(-20:20:20)%(-20:20:20); % Drop Temperature in �C
AT=-20; % Air Temperature in �C

arr_D=(0.1:0.1:4)*10^-3;  % Drop Diameter to plot
U_film=NaN(length(DT),length(arr_D)); % Preallocate data array for film splash

fig1=figure(1);


for jj=1:length(DT)
    %% Threshold for Film Splash
    U_film(jj,:)=((Sfilm*water_props(DT(jj),'sigma')*water_props(DT(jj),'nu')^0.44.*arr_D.^(-1.44))/(water_props(DT(jj),'rho'))).^(1/2.44);

    hold on
    p1=plot(arr_D,U_film(jj,:),'Displayname',['Film Trsh.(',num2str(DT(jj)),'�C)']); % Plot Film Splash Threshold
    hold off

    curr_col=get(p1,'color'); % Get color of current line plot

    %% Calculate and plot threshold for different air temperatures
    U_aero=NaN(length(AT),length(arr_D));
%     for ii=1:length(AT)
        %Threshold for Aerodynimic splash
%         U_aero(ii,:)=(Saero^2*water_props(DT(jj),'sigma')^2*M)./(air_props(AT(ii),'rho')^2*kappa*R*(AT(ii)+273.15)*water_props(DT(jj),'nu').*arr_D);
        U_aero=(Saero^2*water_props(DT(jj),'sigma')^2*M)./(air_props(AT,'rho')^2*kappa*R*(AT+273.15)*water_props(DT(jj),'nu').*arr_D);
        hold on
        plot(arr_D,U_aero,'--','Color',curr_col,'Displayname',['Aerodynamic Trsh. DT=(',num2str(DT(jj)),'�C)']) % Plot Aerodynamic threshold
        hold off

%     end

end
% Plot Parameters
xlabel('Drop Diameter [m]')
ylabel('Drop Velocity [m/s]')
title('Comparison of Thresholds Aero and Film')
legend('-DynamicLegend')
ylim([0,20]);


