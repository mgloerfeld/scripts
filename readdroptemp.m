function [Data]=readdroptemp(Location,datarownmb)% function to read drop temperature from text file
% Location='C:\Users\Measure\Desktop\Temperaturmessung\Arduino_Temp_Humidity_sensor101019_143856.txt';
datype='%f';
dataform=datype;
for ii=1:datarownmb-1
   dataform=[dataform,datype];
end

FileID = fopen(Location);
Data = textscan(FileID,dataform,'Delimiter','\n','headerlines',1);
fclose(FileID);