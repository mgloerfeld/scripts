function [out]=nitrogen_props(T_in,varargin) 

% NITROGEN_PROPS % Function to calculate nitrogen(gaseous) parameters for distinct
% temperatures BETWEEN -196�C and 200�C
% at ambient pressure p_0= 1 bar;
% Values taken from VDI W�rmeatlas 10.Edition (2006)
%
%   Input: 
%           T_in = Temperature in �C
%
%           'rho': Density
%           'cp': Heat Capacity
%           'beta': Thermal Coefficient
%           'lambda': Thermal Conductivity
%           'eta': Dynamic Viscosity
%           'Pr': Prandtl number
%           'nu': Kinematic Viscosity
%
%           'Recalculate' : Recalculate coefficients for polynomial fit
%           from data for certain Temperature range given as [T1 T2],
%           T1 < T2, T2-T1>=10            
%
%           'ValidationPlot' Plots the fit of the desired parameters for
%           validation of calculated values
%
%
%   Output:
%           Desired parameter defined in input at input temperature
%           
%           Example:
%           nitrogen_props(15,'rho') gives density of water at 15�C (at p_0)
%            nitrogen_props(30,'rho','Recalculate',[0 40]) gives density of
%           nitrogen at 15�C (at p_0) for recalculated fit for given range

%% Default booleans
        bl_rho=false;
        bl_cp=false;
        bl_betaThC=false;
        bl_lambda=false;
        bl_eta=false;
        bl_Pr=false;
        bl_nu=false;
        bl_recal=false;
        bl_skip=false;
        bl_range=false;
        bl_plot=false;
%% Interpret additional input

for ii=1:length(varargin)
    
    if bl_skip==true
        bl_skip=false;
        continue
    end
    
    switch varargin{ii}

        case 'rho'
            bl_rho=true;
        case 'cp'
            bl_cp=true;
        case 'beta'
            bl_betaThC=true;
        case 'lambda'
            bl_lambda=true;
        case 'eta'
            bl_eta=true;
        case 'Pr'
            bl_Pr=true;
        case 'nu'
            bl_nu=true;
            bl_rho=true;
            bl_eta=true;
        case 'Recalculate'
            bl_recal=true;
            if length(varargin)>=ii+1 && length(varargin{ii+1})==2
                temp_range=varargin{ii+1};
                bl_range=true;
                bl_skip=true;
            end
        case 'ValidationPlot'
            bl_plot=true;  
        otherwise
            disp(['Unkown input:',varargin{ii},'! Insert right name for desired parameter (For fluid properties try rho,cp,beta,lambda,eta,Pr or nu!)'])
            return

    end
     
end
% Display Warning if input temperature is outside of defined temperature range
if T_in > 200 || T_in < -196
    disp('ATTENTION: Input temperature outside of defined temperature range. Parameter may be estimated wrong.')
end


%% Define literature data
if bl_recal==true || bl_plot==true % If true recalculate interpolations with given data below
    
    arr_T_lit=[-196,-190:10:200]'; % Temperature in �C for which data is available
    arr_rho_lit=[4.5102,4.1949,3.7067,3.3259,3.0187,2.7651,2.5517,2.3695,2.2119,2.0742,1.9529,1.8451,1.7486,1.6619,1.5833,1.5119,1.4467,1.3869,1.3319,1.2811,1.2340,1.1903,1.1496,1.1116,1.0760,1.0426,1.0113,0.98177,0.95392,0.92762,0.90273,0.87914,0.85676,0.83549,0.81525,0.79597,0.77758,0.76003,0.74325,0.72719,0.71181]'; % Corresponding Density data from literature in [kg/m^3]
    arr_cp_lit=[1.122,1.102,1.080,1.068,1.061,1.056,1.052,1.050,1.048,1.047,1.045,1.045,1.044,1.043,1.043,1.042,1.042,1.042,1.042,1.042,1.041,1.041,1.041,1.041,1.041,1.042,1.042,1.042,1.042,1.043,1.043,1.044,1.044,1.045,1.046,1.047,1.048,1.049,1.050,1.051,1.053]'*10^3; % Corresponding Heat capacity data from literature in [J/(kgK)]
    arr_betaThC_lit=[14.75,13.32,11.53,10.22,9.200,8.380,7.703,7.132,6.643,6.218,5.847,5.518,5.224,4.961,4.724,4.508,4.312,4.132,3.967,3.814,3.673,3.542,3.420,3.307,3.200,3.101,3.007,2.919,2.836,2.757,2.683,2.613,2.546,2.483,2.422,2.365,2.310,2.258,2.208,2.160,2.114]'; %Corresponding Thermal Coefficient data from literature in [1/K]
    arr_lamda_lit=[7.419,8.061,9.108,10.13,11.13,12.10,13.04,13.96,14.86,15.74,16.59,17.43,18.24,19.04,19.83,20.59,21.35,22.09,22.81,23.53,24.23,24.92,25.60,26.27,26.93,27.59,28.23,28.87,29.50,30.13,30.75,31.36,31.97,32.58,33.18,33.78,34.37,34.96,35.55,36.14,36.72]'*10^-3; % Corresponding thermal conductivity data from literature in [W/(mK)]
    arr_eta_lit=[5.023,5.470,6.201,6.912,7.603,8.276,8.931,9.568,10.19,10.79,11.38,11.96,12.53,13.08,13.62,14.15,14.67,15.18,15.68,16.17,16.65,17.13,17.60,18.06,18.51,18.96,19.40,19.83,20.26,20.69,21.10,21.52,21.93,22.33,22.73,23.13,23.52,23.90,24.29,24.67,25.04]'*10^-6; % Corresponding dynamic viscosity data from literature in [m^2/s]
    arr_Pr_lit=[0.7599,0.7477,0.7355,0.7288,0.7248,0.7222,0.7205,0.7193,0.7185,0.7179,0.7174,0.7170,0.7167,0.7165,0.7163,0.7162,0.7161,0.7160,0.7159,0.7158,0.7158,0.7158,0.7157,0.7157,0.7157,0.7158,0.7158,0.7158,0.7159,0.7160,0.7160,0.7161,0.7163,0.7164,0.7165,0.7167,0.7169,0.7171,0.7173,0.7175,0.7177]'; % Corresponding  Prandtl number data from literature 
    
    tbl_litprops=table(arr_T_lit,arr_rho_lit,arr_cp_lit,arr_lamda_lit,arr_eta_lit,arr_Pr_lit,arr_betaThC_lit,'VariableNames',{'T','rho','cp','lambda','eta','Pr','betaThC'});

    if bl_range==true % Find indices of literature data of desired temperature range 
        ind_rnge(1)=find(arr_T_lit==temp_range(1));
        ind_rnge(2)=find(arr_T_lit==temp_range(2));
    else % Use whole range as default
        ind_rnge=[1,length(arr_T_lit)];
    end
end
    
if bl_recal==true
        % Fit polynom to literature data
        % ---------------------------Density---------------------------------------
        coeff.rho=polyfit(tbl_litprops.T(ind_rnge(1):ind_rnge(2)),tbl_litprops.rho(ind_rnge(1):ind_rnge(2)),5); % Calculate polynomial fit to literature data
        % ----------------------------Heat Capacity---------------------------------
        coeff.cp=polyfit(tbl_litprops.T(ind_rnge(1):ind_rnge(2)),tbl_litprops.cp(ind_rnge(1):ind_rnge(2)),8); % Calculate polynomial fit to literature data
        % ----------------------------Thermal Coefficient---------------------------
        coeff.betaThC=polyfit(tbl_litprops.T(ind_rnge(1):ind_rnge(2)),tbl_litprops.betaThC(ind_rnge(1):ind_rnge(2)),5); % Calculate polynomial fit to literature data
        % ----------------------------Thermal Conductivity--------------------------
        coeff.lambda=polyfit(tbl_litprops.T(ind_rnge(1):ind_rnge(2)),tbl_litprops.lambda(ind_rnge(1):ind_rnge(2)),5); % Calculate polynomial fit to literature data
        % ----------------------------Dynamic Viscosity---------------------------
        coeff.eta=polyfit(tbl_litprops.T(ind_rnge(1):ind_rnge(2)),tbl_litprops.eta(ind_rnge(1):ind_rnge(2)),5); % Calculate polynomial fit to literature data
        % ----------------------------Prandtl number---------------------------
        coeff.Pr=polyfit(tbl_litprops.T(ind_rnge(1):ind_rnge(2)),tbl_litprops.Pr(ind_rnge(1):ind_rnge(2)),8); % Calculate polynomial fit to literature data
      
else % Use default coefficients from first fit for data from -196�C to 200�C
        coeff.rho=[-2.857748153870328e-12 6.669536182316400e-10 -1.673208974035212e-08 9.183052976679484e-06 -0.004749456956680 1.250350354708362]; % Coefficients from polynomial fit 
        coeff.cp=[5.419401665928179e-17 -9.562862051334159e-15 -2.555426858994270e-12 3.749569435670367e-10 4.827752754740469e-08 -5.034143724810311e-06 1.701708650638879e-05 0.006502340352368 1.041492018900066e+03]; % Coefficients from polynomial fit 
        coeff.betaThC=[-1.290383072352381e-11 2.645469068600991e-09 6.317935944500622e-08 1.508870796364310e-05 -0.014899290531055 3.753887161700323]; % Coefficients from polynomial fit 
        coeff.lambda=[2.953635877949863e-17 -1.172193854799284e-13 1.173074497407036e-10 -5.475604128740505e-08 6.961146234419175e-05 0.024228447331723]; % Coefficients from polynomial fit 
        coeff.eta=[8.272401224943973e-20 -1.136710671761543e-16 6.943863316843847e-14 -3.980733502997342e-11 4.790155484068936e-08 1.665420347882488e-05]; % Coefficients from polynomial fit 
        coeff.Pr=[3.224186801177889e-20 -5.936024436570562e-18 -1.507720992097259e-15 2.399583883133530e-13 2.721576922640306e-11 -3.380903247113881e-09 -6.406684774554052e-08 6.312107704968258e-06 0.715891272722238]; % Coefficients from polynomial fit
end
%% Calculate desired parameter
if bl_rho==true   %Parameter : rho
    rho_Tin= coeff.rho(1)*T_in.^5+coeff.rho(2)*T_in.^4+coeff.rho(3)*T_in.^3+coeff.rho(4)*T_in.^2+coeff.rho(5)*T_in+coeff.rho(6); % Calculate temperature dependent parameter
    out=rho_Tin; % Define output parameter
end

if bl_cp==true   %Parameter : cp
    cp_Tin= coeff.cp(1)*T_in.^8+coeff.cp(2)*T_in.^7+coeff.cp(3)*T_in.^6+coeff.cp(4)*T_in.^5+coeff.cp(5)*T_in.^4+coeff.cp(6)*T_in.^3+coeff.cp(7)*T_in^2+coeff.cp(8)*T_in+coeff.cp(9); % Calculate temperature dependent parameter
    out=cp_Tin; % Define output parameter
end

if bl_betaThC==true   %Parameter : betaThC
    betaThC_Tin= coeff.betaThC(1)*T_in.^5+coeff.betaThC(2)*T_in.^4+coeff.betaThC(3)*T_in.^3+coeff.betaThC(4)*T_in.^2+coeff.betaThC(5)*T_in+coeff.betaThC(6); % Calculate temperature dependent parameter
    out=betaThC_Tin; % Define output parameter
end

if bl_lambda==true   %Parameter : lambda
    lambda_Tin= coeff.lambda(1)*T_in.^5+coeff.lambda(2)*T_in.^4+coeff.lambda(3)*T_in.^3+coeff.lambda(4)*T_in.^2+coeff.lambda(5)*T_in+coeff.lambda(6); % Calculate temperature dependent parameter
    out=lambda_Tin; % Define output parameter
end

if bl_eta==true   %Parameter : eta
    eta_Tin= coeff.eta(1)*T_in.^5+coeff.eta(2)*T_in.^4+coeff.eta(3)*T_in.^3+coeff.eta(4)*T_in.^2+coeff.eta(5)*T_in+coeff.eta(6); % Calculate temperature dependent parameter    
    out=eta_Tin; % Define output parameter
end

if bl_Pr==true   %Parameter : Pr
    Pr_Tin= coeff.Pr(1)*T_in.^8+coeff.Pr(2)*T_in.^7+coeff.Pr(3)*T_in.^6+coeff.Pr(4)*T_in.^5+coeff.Pr(5)*T_in.^4+coeff.Pr(6)*T_in.^3+coeff.Pr(7)*T_in^2+coeff.Pr(8)*T_in+coeff.Pr(9); %Calculate temperature dependent parameter
    out=Pr_Tin; % Define output parameter
end

if bl_nu==true   %Parameter : nu 
    nu_Tin=eta_Tin/rho_Tin;% Calculate temperature dependent parameter from dynamic viscosity and density
    out=nu_Tin; % Define output parameter
end

    
%% Control plots
if bl_plot==true
    
    %Rho
    if bl_rho==true && bl_nu==false
        syms rho(T); rho(T)= coeff.rho(1)*T^5+coeff.rho(2)*T^4+coeff.rho(3)*T^3+coeff.rho(4)*T^2+coeff.rho(5)*T+coeff.rho(6); % Define function of rho
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,rho(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Rho [kg/m^3]')
        xlabel('T [�C]')
        title('Rho')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.rho,'Displayname','Lit. Data')
        scatter(T_in,rho_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
    
    %Cp
    if bl_cp==true
        syms cp(T); cp(T)= coeff.cp(1)*T.^8+coeff.cp(2)*T.^7+coeff.cp(3)*T.^6+coeff.cp(4)*T.^5+coeff.cp(5)*T.^4+coeff.cp(6)*T.^3+coeff.cp(7)*T^2+coeff.cp(8)*T+coeff.cp(9); % Define function of cp
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,cp(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Cp [J/(kgK)]')
        xlabel('T [�C]')
        title('Cp')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.cp,'Displayname','Lit. Data')
        scatter(T_in,cp_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
    
    %betaThC
    if bl_betaThC==true
        syms betaThC(T); betaThC(T)= coeff.betaThC(1)*T^5+coeff.betaThC(2)*T^4+coeff.betaThC(3)*T^3+coeff.betaThC(4)*T^2+coeff.betaThC(5)*T+coeff.betaThC(6); % Define function of betaThC
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,betaThC(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Beta [1/K]')
        xlabel('T [�C]')        
        title('betaThC')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.betaThC,'Displayname','Lit. Data')
        scatter(T_in,betaThC_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
    
    %Lambda
    if bl_lambda==true
        syms lambda(T); lambda(T)= coeff.lambda(1)*T^5+coeff.lambda(2)*T^4+coeff.lambda(3)*T^3+coeff.lambda(4)*T^2+coeff.lambda(5)*T+coeff.lambda(6); % Define function of lambda
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,lambda(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Lambda [W/(mK)]')
        xlabel('T [�C]')
        title('Lambda')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.lambda,'Displayname','Lit. Data')
        scatter(T_in,lambda_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
    
    %Eta
    if bl_eta==true && bl_nu==false
        syms eta(T); eta(T)= coeff.eta(1)*T.^5+coeff.eta(2)*T.^4+coeff.eta(3)*T.^3+coeff.eta(4)*T.^2+coeff.eta(5)*T+coeff.eta(6); % Define function of eta
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,eta(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Eta [Pa s]')
        xlabel('T [�C]')        
        title('Eta')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.eta,'Displayname','Lit. Data')
        scatter(T_in,eta_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
    
    %Pr
    if bl_Pr==true
        syms Pr(T); Pr(T)= coeff.Pr(1)*T.^8+coeff.Pr(2)*T.^7+coeff.Pr(3)*T.^6+coeff.Pr(4)*T.^5+coeff.Pr(5)*T.^4+coeff.Pr(6)*T.^3+coeff.Pr(7)*T^2+coeff.Pr(8)*T+coeff.Pr(9); % Define function of Pr
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,Pr(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Pr')
        xlabel('T [�C]')        
        title('Pr')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.Pr,'Displayname','Lit. Data')
        scatter(T_in,Pr_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
    
    %Nu
    if bl_nu==true
        syms rho(T); rho(T)= coeff.rho(1)*T^5+coeff.rho(2)*T^4+coeff.rho(3)*T^3+coeff.rho(4)*T^2+coeff.rho(5)*T+coeff.rho(6); % Define function of rho
        syms eta(T); eta(T)= coeff.eta(1)*T.^5+coeff.eta(2)*T.^4+coeff.eta(3)*T.^3+coeff.eta(4)*T.^2+coeff.eta(5)*T+coeff.eta(6); % Define function of eta
        T_data=arr_T_lit(ind_rnge(1):ind_rnge(2));
        figure();
        plot(T_data,eta(arr_T_lit(ind_rnge(1):ind_rnge(2)))./rho(arr_T_lit(ind_rnge(1):ind_rnge(2))),'Displayname','Fit')
        ylabel('Nu [m^2/s]')
        xlabel('T [�C]')        
        title('Nu')
        hold on
        scatter(tbl_litprops.T,tbl_litprops.eta./tbl_litprops.rho,'Displayname','Lit. Data')
        scatter(T_in,eta_Tin/rho_Tin,'o','filled','r','Displayname','Calc. Value')
        legend('-DynamicLegend')
    end
end

end