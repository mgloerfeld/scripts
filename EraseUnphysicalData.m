function [arr_cleandata]=EraseUnphysicalData(arr_rawdata)
% ERASEUNPHYSICALDATA erase data from spreading data that shows unphysical changes,
% i.e. obvious measurement errors


arr_diffdata=diff(arr_rawdata); % Calculate differential of all data
[~,ind_max]=max(arr_rawdata);
var_difthr=abs(mean(arr_diffdata(1:ind_max),'omitnan')); % Use first measurements as measure for highest slope (Drop impact)

arr_cleandata=arr_rawdata; % Initialize clean data array
for ii=ind_max:length(arr_cleandata) % Start at second entry
    
    ind_lstvlid=find(~isnan(arr_cleandata(1:ii-1)),1,'last'); % Search vor last entry which is not NaN
    var_curdiff=abs(arr_cleandata(ii)-arr_cleandata(ind_lstvlid)); % Calculate slope to current point
    if var_curdiff>var_difthr    % Check if slope is too big

        arr_cleandata(ii)=NaN; % Replace with NaN
        
        if ii-ind_lstvlid>=10 % Avoid consecutive deletion of all following data (reinsert last 10 is all were deleted)
            arr_cleandata(ii-9:ii)=arr_rawdata(ii-9:ii);
        end
    end

end
%% ValidationPlot
% figure();
% scatter(1:length(arr_rawdata),arr_cleandata)
% 
% hold on
% scatter(1:length(arr_rawdata),arr_rawdata,'.');
% scatter(ind_max,arr_rawdata(ind_max),'*')
% hold off

end