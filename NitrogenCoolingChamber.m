% Calculate the mass flux of nitrogen to cool a climate chamber

%% Default Values
%  Starting Air Properties
Air_T=20; % Starting Temperature in [�C]
Air_Tset=-20; % Set-Point Air Temperature
Air_cp=(air_props(Air_T,'cp')+air_props(Air_Tset,'cp'))/2; % Heat capacity in [J/(kgK)] at Air_T
Air_rho=(air_props(Air_T,'rho')+air_props(Air_Tset,'rho'))/2; %Density in [kg/m^3] at Air_T


% Nitrogen Starting Properties
N_boilT=-196; % Boiling Temperature of Nitrogen
N_enthv=199.4e3; % Enthalpy of vaporisation at -196�C
N_cp_av=(1.122+1.042)*10^3/2; % Average heat capacity for temperatures -196�C and -20�C [J/(kgK)]
N_rho_g_av=(4.501+1.3319)/2; % Average density for gaseous nitrogen at temperatures -196�C and -20�C [kg/m^3)]
N_rho_fl=807.01; % Density for fluid nitrogen at temperatures -196�C [kg/m^3)]

% Inner Chamber Details
ch_w=0.9; % Width of Chamber [m]
ch_l=1.54; % Lenght of Chamber [m]
ch_h=2.4; % Height of Chamber [m]
ch_vol=ch_w*ch_l*ch_h; % Chamber Vol [m^3]

%% Heat Flux
Q_set=2000; % Heat flux brought into chamber with wind channel running [W]
Q_loss=300; % Heat flux through walls at Air_Tset [W]
N_m_ini= Q_set/(N_cp_av*(Air_Tset-N_boilT)+N_enthv); % Mass flux required to balance Q_set
% Qair_ini= Air_rho*Air_cp*ch_vol*(Air_T-Air_Tset); % Required removed Heat to cool air to Air_Tsoll


N_m=[N_m_ini:5e-5:99*N_m_ini]';
t_req=NaN(length(N_m),1); %Preallocate cool down times
for jj=1%:length(N_m)
t=[1:3600]';
delta_t=t(2)-t(1);
T=NaN(length(t),1); % Preallocate Temperatures
T(1)=Air_T; % Initial temperature in chamber
for ii=1:length(t)-1
    T(ii+1)=-((N_m(jj)*(N_cp_av*(T(ii)-N_boilT)+N_enthv)-Q_loss)/(air_props(T(ii),'rho')*ch_vol*air_props(T(ii),'cp')))*delta_t+T(ii);
    if T(ii+1)<Air_Tset
        t_req(jj)=t(ii); % Save time required for cool down to Air_Tset
        break
    end
end
end
%% Plots
% Temperature propagation
figure();
plot(t,T);
% Cool down times for several mass flow
figure();
plot(N_m,t_req/60);
ylabel('t in min')
xlabel('m_N in kg/s')
grid on
% Cool down times for several volume flow
figure();
plot(N_m*N_rho_fl,t_req/60);
ylabel('t in min')
xlabel('V in m^3/s')
grid on
    
    
    
    
    