% Select mass percentage of Gylcerin and plot corresponding Oh,We and Re


arr_T=[20 20]; % Drop temperature in �C
d_wg=3e-3; % Drop Diameter in m
arr_v_wg=[4 10 15]; % Impact Velocity in m/s

for kk=1:length(arr_v_wg)
    T=arr_T(1);
    v_wg=arr_v_wg(kk);
Gly_perc=(0:5:100); % Glycerin Percentages in %

Re_Gly=NaN(length(Gly_perc),1);
We_Gly=NaN(length(Gly_perc),1);
Oh_Gly=NaN(length(Gly_perc),1);



for ii=1: length(Gly_perc)
    
    str_gly=['Gly',num2str(Gly_perc(ii))];
    [rho_wg,eta_wg,sigma_wg]=function_liquid_properties(str_gly,T);
    
    Re_Gly(ii)=v_wg*d_wg*rho_wg/eta_wg;
    We_Gly(ii)=rho_wg*v_wg^2*d_wg/sigma_wg;
    Oh_Gly(ii)=eta_wg/sqrt(d_wg*rho_wg*sigma_wg);
    
end

fig=figure(1);

%% Plot Reynolds Number
sfig1=subplot(2,2,1);
hold on
plot(Gly_perc,Re_Gly)
hold off
title('Reynolds Number')
xlabel('Mass Fraction Glycerol [%]')
ylabel('Reynolds Number')


%% Plot Weber Number
sfig2=subplot(2,2,2);
hold on
plot(Gly_perc,We_Gly)
hold off
title('Weber Number')
xlabel('Mass Fraction Glycerol [%]')
ylabel('Weber Number')


%% Plot Ohnesorg Number
sfig3=subplot(2,2,3);

hold on; 
plot(Gly_perc,Oh_Gly)

Oh_crit=0.0044;
plot([0 100],[Oh_crit Oh_crit])
hold off

title('Ohnesorg Number')
xlabel('Mass Fraction Glycerol [%]')
ylabel('Ohnesorg Number')


%% Plot in Regime Card We over Re
sfig4=subplot(2,2,4);

T_w=-12;
Oh_water=water_props(T_w,'eta')/sqrt(2e-3*water_props(T_w,'rho')*water_props(T_w,'sigma')); % Calculate Ohnesorgnumber of supercooled water

We_crit=Oh_crit^2*Re_Gly.^2; % Calculate Corona Splash threshold
We_w=Oh_water^2*Re_Gly.^2; % Calculaute Line for supercooled water

hold on;
scatter(Re_Gly,We_Gly,[],Gly_perc,'filled')
scatter(Re_Gly(Gly_perc==40),We_Gly(Gly_perc==40),'d')

plot(Re_Gly,We_crit,'--');
plot(Re_Gly,We_w,'.');


% plot(values_x,values_y,'.-')

text(Re_Gly(end-1),We_Gly(end-1)-500,['V=',num2str(v_wg),'m/s'])

hold off


cb=colorbar;
cb.Label.String='Glycerin Percentage in %';

set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')


title('Regime Card')
xlabel('Re')
ylabel('We')


sfig4.XLim=[100 17000];
sfig6YLim=[200 10000];

end
