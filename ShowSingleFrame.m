% function ShowSingleFrame(Location,framenr)

Location='F:\Experiments\ResidualMass_Experiments\Experiments\20201111\minus5d\001_-5_DC_0ms_h\';


ImageFiles=dir([Location '**\*images*']);
load([ImageFiles.folder,'\',ImageFiles.name]);

DataFiles=dir([Location '**\*Data.mat']);
load([DataFiles.folder,'\',DataFiles.name]);


framenr=find(stc_VID.Info.Impact,1);
%% Plot


figure();
imshow(images(:,:,framenr))
hold on
scatter(stc_VID.ImageData(framenr).SurfaceTouchPts(:,1),stc_VID.ImageData(framenr).SurfaceTouchPts(:,2))
hold off


% end