% Compute dew point over ice
function T=DewPointIce(phi,T1)

myfun = @(T2,phi) VapourPressureIce(T2)-phi*VapourPressureIce(T1);  % parameterized function
fun = @(T2) myfun(T2,phi);    % function of T2 alone
T = fzero(fun,-5);

end