% Plot Temperature and Humidty Data from wet bulb temperature
% Location='C:\Users\Measure\Desktop\Temperaturmessung\Kalibration_WetBulbTemp\';


Location='C:\Users\Measure\Desktop\Temperaturmessung\Tiefk�hlzellenmessung\';
keyword='zellenmessung';
Files=dir([Location,'*',keyword,'*']);

%% Preallocation
% cl_time(ii)
% cl_droptemp(ii)
% cl_airtemp(ii)
% cl_ardtemp(ii)
% cl_ardhumid(ii)

%% Get data from Wet bulb temperature measurement
% for ii=length(Files)    
% Data=readdroptemp([Location,Files(ii).name],5);
% 
% cl_time{ii}=Data{1};
% cl_droptemp{ii}=Data{2};
% cl_airtemp{ii}=Data{3};
% cl_ardtemp{ii}=Data{4};
% cl_ardhumid{ii}=Data{5};
% 
% end
%% Get data from Freezing chamber temperature measurement
for ii=length(Files)-1
Data=readdroptemp([Location,Files(ii).name],2);

cl_time{ii}=Data{1};
cl_droptemp{ii}=Data{2};

end


%% Plot Drop Temperature
fig1=figure(1);
hold on
for ii=1:length(cl_time)

plot(cl_time{ii}/60,cl_droptemp{ii},'Displayname','Drop Temperature')
xlabel('Time /min')
ylabel('Temperature /�C')
title('Sollwert -20�C,Verbraucher 1.4kW')

end
grid on 
hold off

print('C:\Users\Measure\Desktop\Tiefk�hlzelle\SW_-20_VB_1400W.png','-dpng')
%% Plot Air Temperature
% % fig2=figure(2);
% hold on
% for ii=1:length(cl_time)
% 
% plot(cl_time{ii}/60,cl_airtemp{ii},'Displayname','Air Temperature')
% xlabel('Time /min')
% end
% hold off
% 
% legend('-DynamicLegend')

 %% Plot Humidity
% fig3=figure(3);
% hold on
% for ii=1:length(cl_time)
% 
% plot(cl_time{ii}/60,cl_ardhumid{ii})
% xlabel('Time /min')
% end
% hold off

%% Plot Difference of Air and Drop Temperature
% fig4=figure(4);
% hold on
% for ii=1:length(cl_time)
% 
% plot(cl_time{ii}/60,cl_airtemp{ii}-cl_droptemp{ii})
% xlabel('Time /min')
% end
% hold off