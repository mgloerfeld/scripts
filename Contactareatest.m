% Calculate contact area of impacting sphere

t_=0:0.0001:2;
r_1=sqrt(2.*t_);
r_2old=sqrt(2.*t_-t_.^2);
r_2=sqrt(2.*t_-t_.^2);

ind_uni=find(r_2==1);
r_2(ind_uni:end)=r_2(ind_uni:end)+cumsum(r_2(ind_uni:end))./r_2(ind_uni:end);

figure();
hold on 
semilogx(t_,r_1)
semilogx(t_,r_2)
semilogx(t_,r_2old)

% semilogx(t_,r_1./r_2)
hold off


