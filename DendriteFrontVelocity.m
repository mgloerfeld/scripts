function [U_f]=DendriteFrontVelocity(Delta_T)
%% FUNCTION THAT GIVES DENDRITE FRONT VELOCITY BASED ON DATA FROM SCHREMB(2016)

% Input: Delta_t - Supercooling temperature
% Output U_f - Dendrite cloud front velocity


Data=load('DendriteFrontVelocityData');
Data=Data.Data;

modelfun = @(b,x)(b(1).*(x.^(b(2))));
beta0=[1 1];
pwlwcoeff=nlinfit(Data.DeltaT,Data.U_f,modelfun,beta0);

fitcoeff=polyfit(Data.DeltaT,Data.U_f,3);


% fitcoeff=[-3.62537079187723e-05,0.00144661559176047,-0.00666049085452133,0.00571873647922988]; % Fit from Schremb(2016) Data
U_f=round(polyval(fitcoeff,abs(Delta_T)),4);



% Validation Plot
% figure();
% scatter(Data.DeltaT,Data.U_f)
% hold on
% scatter(abs(Delta_T),U_f,'ro','filled')
% plot(0:20,polyval(fitcoeff,0:20))
% plot(0:20,modelfun(pwlwcoeff,0:20),'--')
% hold off


end