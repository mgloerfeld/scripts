Location='F:\Experiments\SupercooledDropImpact_DropAccelerator\exp_11_17_low_vel_bonus\'; % Video Folder Location (Adjust for different video file location)

%% Get all Data found in Location
DataFiles = dir([Location '**\*Data_test2.mat']);
FileNames = {DataFiles.name};
FileFolder={DataFiles.folder};
numdata=length(DataFiles); % Get number of DataFiles (One data set per video)
if numdata==0 % Display Message if no Data has been found
    disp('No Files Found! Check Data File Location')
    return
end

for VidNr=1:length(FileFolder)
    VideoName=strtok(FileNames{VidNr},'.'); %Video Name
    FileName=[FileFolder{VidNr},'\', FileNames{VidNr}]; % Concatenate Filename
    % Load stc.VID (Data file)
    if isfile([FileFolder{VidNr},'\',VideoName,'.mat'])
        load([FileFolder{VidNr},'\',VideoName,'.mat'])
    end
    NewVideoName=strsplit(VideoName,'_test');
    NewVideoName=NewVideoName{1};
    save([FileFolder{VidNr},'\',NewVideoName,'.mat'],'stc_VID')
end