
ind_dropimp=find(stc_VID.Info.Impact,1);
for kk=1:length(images(1,1,:))
    
figure(1);
imshow(images(:,:,kk));
hold on;
if ~isempty(stc_VID.ImageData(kk).SurfaceSpreadL)
scatter(arr_imppt(1)-stc_VID.ImageData(kk).SurfaceSpreadL,arr_imppt(2),'*','r');
scatter(arr_imppt(1)+stc_VID.ImageData(kk).SurfaceSpreadR,arr_imppt(2),'*','b');
end
% scatter(arr_imppt(1)+(stc_VID.Data.UpwindSpreadingLength(kk-ind_dropimp+1)/stc_VID.Info.PixelSize),arr_imppt(2),'o','b');
% scatter(arr_imppt(1)-(stc_VID.Data.DownwindSpreadingLength(kk-ind_dropimp+1)/stc_VID.Info.PixelSize),arr_imppt(2),'o','r');

hold off

end