% Calculate Heat Flux in Flow Trough Radiator

%% Air Properties
air_T_in=-19; % Inflow temperature [�C]
air_T_out=-20; % Desired Outflow temperature [�C]
air_T_av=(air_T_out+air_T_in)/2; % Average temperature [�C]
air_Pr= air_props(air_T_av,'Pr'); % Prandtl number at average temperature
air_nu= air_props(air_T_av,'nu'); % Kinematic viscosity at av. temperature [m/s^2]
air_lambda= air_props(air_T_av,'lambda'); % Heat conduction coefficient at av. temperature [W/(mK)]
air_vel= 40; % Inflow velocity [m/s]

%% Radiator geometry
%%%% Gap Geometry %%%%
gap_s=0.002; % Width of gaps in radiatior [m]
gap_dhyd=2*gap_s; % Hydraulic diameter of radiator
gap_l=0.03; % Length of radiator gap [m]
gap_dpth=0.005; % Depth of gap orthogonal to flow direction [m]
gap_A=gap_dpth*gap_l;
%%%% Radiator Geometry %%%
rad_w=0.2; % Width of whole radiator
rad_l=0.25; % Lenght of whole radiator
rad_Ngap=(rad_w/gap_dpth)*(rad_l/gap_s); % Number of gaps in radiator


%% Single Gap
Re_air= gap_dhyd*air_vel/air_nu; % Reynolds number in radiator gap
xeta_p=(1.8*log10(Re_air)-1.5)^-2; % Pressure loss coefficient; taken from fully turbulent flow in a pipe
Nu=((xeta_p/8)*Re_air*air_Pr/(1+12.7*sqrt(xeta_p/8)*(air_Pr^(2/3)-1)))*(1+((gap_dhyd/gap_l)^(2/3))); % Nusselt number for fully turbulent flow in a pipe

rad_alpha=Nu*air_lambda/(gap_dhyd); % Heat conduction coefficient for single gap
Q_gap=rad_alpha*gap_A*(air_T_in-air_T_out); % Heat flux for single gap

Q_tot=Q_gap*rad_Ngap;

