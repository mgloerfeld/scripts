vol_flw= 13.42; % Volume flow in m^3/h
enth_fl= 152.5e3; % Enthalpie of liquid R449A [J/kg]
enth_g= 379.41e3; % Enthalpie of gaseous R449A [J/kg]

%Cooling power due to vaporisation
cool_pw=(vol_flw/3600)*(enth_g-enth_fl);

%Cooling power due to temperature difference