clear all;
close all;
clc;

input='C:\Users\ui16ryno\Desktop\Promotion\Media\Tropfenschuss\See through shot-Succesfull/2bar';
input = uigetdir(input);
filename='test';

dd = dir([input '\*.tif']);
fileNames = {dd.name};
ref=(imread(char(fullfile(input,fileNames(1)))));
     ref=double(ref)/4096;

  %   ref=imadjust(ref,[0,0.15],[0,1]);
imshow(ref)
[ref,rect]=imcrop(ref);
mov=im2uint8(zeros(size(ref,1),size(ref,2),size(fileNames,2)));
for i=1:size(fileNames,2)
     frame = imread(char((fullfile(input,fileNames(i)))));
     frame=double(frame)*255/4096;
%    frame=imadjust(frame,[0,0.5],[0,1]);
     frame = imcrop(frame, rect);
     mov(:,:,i) = frame;
     disp(['progress: ', num2str(i/size(fileNames,2))]);
end
imshow(mov(:,:,2))
%% Speichere Das Video
writerObj = VideoWriter(fullfile(input,[filename]),'Grayscale AVI');
open(writerObj);
writeVideo(writerObj,mov);
close(writerObj);

close all;