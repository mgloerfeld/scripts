% Caluculate thickness of ice layer propagation during fall

thickness_approx=NaN(numel(arr_avdiam),1);
thtorad=NaN(numel(arr_avdiam),1);
icemasstodendmass=NaN(numel(arr_avdiam),1);
dropmasstodendmass=NaN(numel(arr_avdiam),1);


for ii=1:numel(arr_avdiam)

    L=333.1e3; % Latent heat of water at 0�C in J/kg
    rho_ice=900; % Density of ice at 0�C
    cp_ice=2000; % Heat capacity of ice at 0�C (VDI 10th Edition)
    d_drop=arr_avdiam(ii); % Diameter of drop in m
    r_drop=d_drop/2; % Radius of drop
    u_0=arr_dropspeed(ii);
    h_fall=0.7;
    t_fall=h_fall/u_0;
    T_SC=arr_droptemp(ii);

    % Calculate ice fraction in drop after dendritic freezing
    Ice_frac=water_props(T_SC,'cp').*water_props(T_SC,'rho').*abs(T_SC)./(rho_ice*L+abs(T_SC).*(water_props(0,'cp')*water_props(0,'rho')-rho_ice*cp_ice)); % Ice Fraction of drop after dendritic freezing


    % Get amount of heat transferred in worst case (Delta_T does not change)
    [var_deltaT,arr_Qdot]=DropTemperature(arr_avdiam(ii),arr_dropspeed(ii),arr_AirVel(ii),arr_droptemp(ii),arr_airtemp(ii));

    Q_tot=arr_Qdot(1)*t_fall;

    % Calculate mass of ice developing
    m_ice=Q_tot/L;
    % Calculate Volume of ice
    Vol_ice=m_ice/rho_ice; 
    % Calculate thickness of ice shell assuming hollow sphere
    syms th
    thickness_ice=vpasolve(Vol_ice==4/3*pi*(r_drop^3-(r_drop-th)^3),th,[0 1]);
    thickness_ice=double(thickness_ice);
    % Approximate for thin shells
    thickness_approx(ii)=Vol_ice/(4*pi*r_drop^2);


    %% Calculate dendrite tip radius (MST critival wave length = radius)
    T_SC=arr_droptemp(ii);
    rho_wt=water_props(T_SC,'rho');
    a=water_props(T_SC,'thdiff'); % Thermal diffusivity (m^2/s)
    sigma=water_props(T_SC,'sigma'); %  Surface Tension (N/m)
    v_0=7e-2; % Dendrite growth velocity (m/s) (for T_SC=-10�C)
    T_m=273.15; % Melting temperature water in K
    c_v= water_props(0,'cp')*rho_wt; % Value is similar to value of cp for water at 0�C 

    lambda=sqrt(2*a*T_m*sigma*c_v/(v_0*(L*rho_ice)^2));

    %% Relations

    thtorad(ii)=thickness_approx(ii)/lambda;

    icemasstodendmass(ii)=Vol_ice/(Ice_frac*4/3*pi*(d_drop/2)^3);

    dropmasstodendmass(ii)=Vol_ice/(4/3*pi*(d_drop/2)^3);

end