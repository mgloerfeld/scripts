d=0.00476; % Diameter of sphere
V=(4/3)*pi*(d/2)^3; % Calculate Volume of full sphere
rho_st=7800; % Density of steel sphere

m_sph=V*rho_st; % Mass of sphere

d_trsph=0.00426; % Height of truncatedsphere

h=d-d_trsph; % Height difference to intact sphere
V_trsph=V-(pi/3)*h^2*(3*(d/2)-h); % Volume of truncated sphere

m_trsph=V_trsph*rho_st; % Mass of truncated sphere

V_trsph/V;