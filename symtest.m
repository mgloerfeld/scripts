% Find symmetry axis 

arr_boundpol=NaN(length(arr_boundaries),2);
arr_boundrot=NaN(length(arr_boundaries),2);

arr_centr=mean(arr_boundaries);
arr_bound0=arr_boundaries-arr_centr;
[arr_boundpol(:,1),arr_boundpol(:,2)]=cart2pol(arr_bound0(:,1),arr_bound0(:,2));
arr_boundpol_sort=sortrows(arr_boundpol,1);
ind_sym=find(cumtrapz(arr_boundpol_sort(:,1),arr_boundpol_sort(:,2))>=trapz(arr_boundpol_sort(:,1),arr_boundpol_sort(:,2))/2,1);

var_symang=arr_boundpol_sort(ind_sym);

arr_boundpolrot=arr_boundpol;
arr_boundpolrot(:,1)=arr_boundpol(:,1)-(var_symang+pi/2);
[arr_boundrot(:,1),arr_boundrot(:,2)]=pol2cart(arr_boundpolrot(:,1),arr_boundpolrot(:,2));
arr_boundrot=arr_boundrot+arr_centr;


coeff_sim(1)=tan(var_symang);
coeff_sim(2)=arr_centr(2)-coeff_sim(1)*arr_centr(1);
X=(0:length(img_droponly));
arr_line=polyval(coeff_sim,X);


imshow(img_droponly)
hold on; 
scatter(arr_boundaries(:,1),arr_boundaries(:,2))
scatter(arr_boundrot(:,1),arr_boundrot(:,2));
plot(X,arr_line)
hold off