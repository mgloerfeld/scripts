resolution=3000;
image=zeros(resolution,resolution);
origin=[resolution/2;resolution/2];
radius=(1:10:100);

RoundnessRP=NaN(1,length(radius));
RoundnessBW=NaN(1,length(radius));
for kk=1:length(radius)
    image=zeros(resolution,resolution);
    
    
    
for ii=1:resolution
    for jj=1:resolution       
        if sqrt((ii-origin(1))^2+(jj-origin(2))^2)<=radius(kk)
            image(ii,jj)=true;
        end
    end
end

stc_image=regionprops(image,'Area','Perimeter');
RoundnessRP(kk)=2*sqrt(stc_image.Area*pi)/stc_image.Perimeter;

cl_boundary=bwboundaries(image);
arr_bound_curr=cat(2,cl_boundary{1,1});
arr_bound_curr=arr_bound_curr(:,end:-1:1); % Rearrange coordinates to first column: x, second column: y
arr_bwperimeter=arclength(arr_bound_curr(:,1),arr_bound_curr(:,2));
RoundnessBW(kk)=2*sqrt(stc_image.Area*pi)/arr_bwperimeter;
% imshow(image)
end
%%
figure();
hold on
plot(radius,RoundnessRP,'g')
plot(radius,RoundnessBW,'r')
hold off
